/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <stdint.h>
#include <iostream>

namespace cabad
{

// To be overwritten by concrete TimingContext
class TimingContext
{
    public:

        TimingContext()
        {}

        // returns timestamp in nanosecond precision
        virtual int64_t getTimeStamp() const = 0;

        // We assume that different events which can occur are represented as numbered
        virtual unsigned int getEventNumber() const = 0;

        // Allows to use a foreign identifier for context information, not bound to the timestamp
        virtual int64_t getID() const
        {
            return 0;
        }

        virtual ~TimingContext()
        { }

        friend std::ostream& operator<<(std::ostream& os, const TimingContext& context)
        {
            os << "TimingContext - Timestamp: " << context.getTimeStamp() << " EventNumber: " << context.getEventNumber() << std::endl;
            return os;
        }
};

} // close namespace
