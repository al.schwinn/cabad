/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/SampleMetaData.h>
#include <cabad/CircularBufferBase.h>
#include <cabad/CircularWindowIterator.h>
#include <cabad/Definitions.h>
#include <cabad/DataReadyManager.h>
#include <cabad/DataReadyHandler.h>
#include <cabad/Util.h>

#include <map>
#include <deque>

namespace cabad
{

class ContextTracker;
class DeviceDataBufferBase;

/* Manages all Iterators and Meta-Data of a single CircularBufferBase */ 
class CircularBufferManagerBase
{

public:

    virtual ~CircularBufferManagerBase();

    double getSampleToSampleDistance()
    {
        return sample_to_sample_distance_;
    }

    int64_t getSampleToSampleDistanceNano()
    {
        return sample_to_sample_distance_nano_;
    }

    CircularBufferBase* getCircularBuffer()
    {
        return circularBuffer_;
    }

    std::string getSignalName()
    {
        return signal_name_;
    }

    std::size_t getPreTriggerSamples()
    {
        return pre_trigger_samples_;
    }

    std::size_t getPostTriggerSamples()
    {
        return post_trigger_samples_;
    }

    void setTriggerSamples(std::size_t pre_trigger_samples, std::size_t post_trigger_samples)
    {
        pre_trigger_samples_ = pre_trigger_samples;
        post_trigger_samples_ = post_trigger_samples;
    }

    void setTriggerMatchingTolerance(int64_t triggerMatchingTolerance_ns)
    {
        triggerMatchingTolerance_ns_ = triggerMatchingTolerance_ns;
    }

    float getSampleRate()
    {
        return sample_rate_hz_;
    }

    void pushBackDataFinished();

    // Retunrs a pointer to the window if it was sucesfully moved, NULL otherwise
    // notification_context will be filled within the method
    // Virtual in order to ease unit-testing
    // last_sample pointer to last sample which is to notify
    // last_sample_stamp: The new window only should have samples with stamps <= last_sample_stamp
    // n_samples_max maximum number of samples to notify (if there are more samples until the previous windows, they are dropped)
    virtual CircularWindowIterator*
    updateStreamingWindowIter(MaxClientUpdateFrequencyType update_frequency_hz,
                              int64_t                      last_sample_stamp,
                              std::size_t                  n_samples_max);

    // Retunrs true if window was sucessfully moved
    // notification_context will be filled within the method
    // @last_sequence_marker_stamp: Only move the full_Sequence window till here (in order to sync buffer movement between different buffers)
    // Virtual in order to ease unit-testing
    virtual CircularWindowIterator*
    updateFullSequenceWindowIter(int64_t last_sequence_marker_stamp);

    // Retunrs true if window was sucessfully moved
    // notification_context will be filled within the method
    // Virtual in order to ease unit-testing
    virtual CircularWindowIterator*
    updateTriggerWindowIter(int64_t     trigger_stamp,
                            EventNumber eventNumber);

    CircularWindowIterator* getStreamingDataWindow(MaxClientUpdateFrequencyType update_frequency_hz);

    CircularWindowIterator* getFullSequenceDataWindow(std::shared_ptr<const TimingContext> timingContext);

    CircularWindowIterator* getCurrentFullSequenceDataWindow();

    CircularWindowIterator* getTriggerDataWindow(std::string triggerEventName);

    CircularWindowIterator* getTriggerDataWindow(unsigned int triggerEventID);

    double getMatchedTriggersPercentage();

    // CTor for Unit Testing
    CircularBufferManagerBase(std::string signal_name);

    LogLevel getLogLevelStreaming(MaxClientUpdateFrequencyType maxClientUpdateFrequency)
    {
        if(log_levels_per_type_[ClientNotificationType::STREAMING] < log_levels_per_maxClientUpdateFrequency_[maxClientUpdateFrequency])
            return log_levels_per_type_[ClientNotificationType::STREAMING];
        else
            return log_levels_per_maxClientUpdateFrequency_[maxClientUpdateFrequency];
    }

    LogLevel getLogLevelTriggered()
    {
        return log_levels_per_type_[ClientNotificationType::TRIGGERED];
    }

    LogLevel getLogLevelFullSequence()
    {
        return log_levels_per_type_[ClientNotificationType::FULL_SEQUENCE];
    }

    void setLogLevelTriggered(LogLevel log_level)
    {
        log_levels_per_type_[ClientNotificationType::TRIGGERED] = log_level;
    }

    void setLogLevelFullSequence(LogLevel log_level)
    {
        log_levels_per_type_[ClientNotificationType::FULL_SEQUENCE] = log_level;
    }

    void setLogLevelStreaming(LogLevel log_level, MaxClientUpdateFrequencyType maxClientUpdateFrequency)
    {
        log_levels_per_type_[ClientNotificationType::STREAMING] = log_level;
        log_levels_per_maxClientUpdateFrequency_[maxClientUpdateFrequency] = log_level;
    }

    void log_error_if(ClientNotificationType type, const std::string& message);
    void log_trace_if(ClientNotificationType type, const std::string& message);
    void log_trace_if(ClientNotificationType type, MaxClientUpdateFrequencyType maxClientUpdateFrequency, const std::string& message);
    bool is_log_enabled_for(ClientNotificationType type, LogLevel log_level);
    bool is_log_enabled_for(ClientNotificationType type, MaxClientUpdateFrequencyType maxClientUpdateFrequency, LogLevel log_level);

protected:

    /*
     * CircularBufferManagerBase:
     * @signal_name: Name of the signal, stored in the buffer
     * @sample_rate_hz: Sample rate of the signal. Use -1 if the signal is not sampled with constant rate
     * @maxClientUpdateFrequencies: Frequencies used to notify subscribed clients
     *                              The BufferManager will try to notify in all passed frequencies,
     *                              though dependant on the incoming sample-rate, the actualupdate frequency can be lower
     * @contextTracker: Each BufferManager requires a context tracker to lookup and allign TimingEvents to the data
     * @deviceDataBuffer: Required to manage reference Triggers and Event-Signatures which might differ per device
     * @circularBuffer: The raw buffer which willhold the data
     * @metadataBuffer: A buffer which will hold metadata (timestamps, event/trigger-info) for concrete entries on the @circularbuffer
     * @clientNotificationType: Masks the supported modes in which clients should be notified
     * @dataPushMode: The way data will be pushed into the buffer. (single samples / arrays / continous / disjunct )
     * @dataReadyManager: Required in order to create/find appropriate DataReadyHandlers for this BufferManager
     * @notification_group: Should the notification of clients be synchronized with other BufferManagers ? Than use the same notification_group for them.
     *                      Otherwise pass some unique key (e.g. the signal_name)
     *
     * Constructor
     */
    CircularBufferManagerBase(std::string signal_name,
                              float sample_rate_hz,
                              std::vector<MaxClientUpdateFrequencyType> maxClientUpdateFrequencies,
                              ContextTracker* contextTracker,
                              DeviceDataBufferBase* deviceDataBuffer,
                              CircularBufferBase* circularBuffer,
                              MetaDataBuffer* metadataBuffer,
                              ClientNotificationType clientNotificationType,
                              DataPushMode dataPushMode,
                              DataReadyManager* dataReadyManager,
                              const std::string& notification_group);



    // Very basic function to add any kind of metadata
    int addMetaData(std::shared_ptr<SampleMetadata>& meta);

    std::shared_ptr<SampleMetadata> addMeta(int64_t  meta_timestamp,
                                            uint64_t related_sample_absolute_offset,
                                            uint32_t status,
                                            bool     isTriggerMeta);

    // Add metadata which is correlated to a WR-Stamp
    // The related sample is just stamped with some WR Timestamp, not related to any WR-Event
    std::shared_ptr<SampleMetadata> addWrStampMetaData(int64_t  wr_stamp,
                                                       uint64_t related_sample_absolute_offset,
                                                       uint32_t status);

    // Add metadata which is correlated to WR-Event and its Hardware Trigger
    // In this case, the related WRContext will be searched and added to the related sample.
    // The old, unprecise trigger stamp will be replaced by the according WR-STamp
    // The delay between trigger stmap and WR stamp will be stored, and added to all other stamped non-trigger samples to correctly allign them in the data stream
    // The matching tolerance to match a trigger stamp with a WRStamp can be controlled via "setTriggerMatchingTolerance"
    std::shared_ptr<SampleMetadata> addTriggerMetaData(int64_t  hardware_trigger_stamp,
                                                       uint64_t related_sample_absolute_offset,
                                                       uint32_t status);

    std::shared_ptr<SampleMetadata> addTriggerMetaData(std::shared_ptr<const TimingContext> context,
                                                       uint64_t related_sample_absolute_offset,
                                                       int64_t related_sample_stamp,
                                                       uint32_t status);

    int findSampleWindowMarkers (const std::shared_ptr<const CircularBufferBase::iterator>& firstSample,
                                 const std::shared_ptr<const CircularBufferBase::iterator>& lastSample,
                                 int64_t& firstSampleStamp,
                                 int64_t& firstContextSearchStamp,
                                 int64_t& lastContextSearchStamp,
                                 int64_t& first_sample_abs_offset);

    // Adds all MultiplexingContexts to the data stream which are found between the time of "firstSample" and the time of "lastSample"
    // This method requires to have a least one call of "addWrStampMetaData", so that a timebase can be build and the contexts can be alligned to the correct samples
    int addMultiplexingContextDataForRange(const std::shared_ptr<const CircularBufferBase::iterator>& firstSample,
                                           const std::shared_ptr<const CircularBufferBase::iterator>& lastSample);

    // After a data push, this method checks if all windows are still valid
    void validateWindows();

    // Has to be initialized in concrete childclass
    CircularBufferBase* circularBuffer_;

    // Each metaData element points to a specific sample, same like tags in gnuradio
    MetaDataBuffer* metadata_buffer_;

    // Distance between two samples in seconds / nanoseconds
    double sample_to_sample_distance_;
    double sample_to_sample_distance_nano_; // as well double required .. can be less than 1ns for samp_rate > 1GS/S

    ContextTracker* contextTracker_;

    // Name of this Signal
    std::string signal_name_;

    // Pointer to the related DeviceDataBuffer
    DeviceDataBufferBase* deviceDataBuffer_;

    // All streaming Data Windows which can be read out by properties (stored per update frequency)
    std::map<MaxClientUpdateFrequencyType, CircularWindowIterator*> readDataWindowColStreaming_;

    std::map<MaxClientUpdateFrequencyType, DataReadyHandlerStreaming*> dataReadyHandlerColStreaming_;
    std::map<EventNumber, DataReadyHandlerTriggered*> dataReadyHandlerColTriggered_;
    DataReadyHandlerTriggered* dataReadyHandlerFullSeq_;

    // The FULL_SEQUENCE window which can be read out by properties
    // More than one window is required if there is more than one full sequences in one FESA Sink update (e.g. 1Hz Sink)
    std::vector<CircularWindowIterator*> readDataWindowColFullSequence_;
    std::size_t numberOfFullSequenceWindows_;
    std::vector<CircularWindowIterator*>::iterator currentFullSequenceWindow_;

    // Latest meta which was added to this circular buffer
    std::shared_ptr<SampleMetadata> most_recent_meta_;

    // Latest meta which was added to this circular buffer
    std::shared_ptr<SampleMetadata> most_recent_timing_event_meta_;

    // MetaData of the most recent seq start and end events
    std::shared_ptr<SampleMetadata> most_recent_sequence_marker_;

    // We already updated the sfull-sequence window till here
    std::shared_ptr<SampleMetadata> last_processed_sequence_marker_;

    // MetaData of the most recent trigger data we received
    std::shared_ptr<SampleMetadata> most_recent_trigger_meta_;

    // MetaDataIter until the method addMultiplexingContextDataForRange already added multiplexing context info
    std::shared_ptr<SampleMetadata> mux_contexts_info_added_till_here_;

    int64_t last_context_search_end_stamp_;

    // All Triggered Data Windows, one per eventNumber
    std::map<EventNumber, CircularWindowIterator*> readDataWindowColTriggered_;

    // We expect that output_package_size of the sink = #presamples + #postsamples for each trigger-event
    std::size_t pre_trigger_samples_;
    std::size_t post_trigger_samples_;

    // Bit enum to remember which notification types this buffer supports
    ClientNotificationType clientNotificationType_;

    bool new_sequence_marker_detected_;
    bool new_trigger_detected_;

    DataPushMode dataPushMode_;

    // The maximum tolerance which will be accepted to match incoming trigger stamps with their according WRContext Timestamps
    int64_t triggerMatchingTolerance_ns_;

    float sample_rate_hz_;

    // For diagnostics
    std::map<ClientNotificationType, LogLevel>       log_levels_per_type_;
    std::map<MaxClientUpdateFrequencyType, LogLevel> log_levels_per_maxClientUpdateFrequency_;

    // Will have a 1 for each matched trigger, a 0 for a missmatch
    average_filter<double> n_matched_triggers_;

    // offset between the utc stamp on incoming data and the real WR time (positive offset --> matched WR stamp is older than stamp on sample)
    // Once a trigger stamp was matched, that delay will be used to correctly allgn all non-trigger stamps
    average_filter<int64_t> alligment_offset_data_timestamp_WRStamp_;
    std::shared_ptr<const TimingContext> last_matching_trigger_context_;
};

} /* namespace cabad */
