/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/Definitions.h>

#include <string>
#include <functional>
#include <deque>

namespace cabad
{

// per default, no logging is done, user of this library might overwrite with "setLogFunction"
typedef std::function<void(LogLevel severity, const std::string& file, int line, const std::string& message, const std::string& topic)> LogFunction;
extern LogFunction globalLogFunction;

// In order to allow unit-testing with fake timestamps
typedef std::function<uint64_t()> GetTimestampFunction;
extern GetTimestampFunction globalGetTimestampFunction;

#define LOG_ERROR(message)   globalLogFunction(LogLevel::ERROR,   __FILE__, __LINE__, message, "" )
#define LOG_WARNING(message) globalLogFunction(LogLevel::WARNING, __FILE__, __LINE__, message, "" )
#define LOG_INFO(message)    globalLogFunction(LogLevel::INFO,    __FILE__, __LINE__, message, "" )
#define LOG_TRACE(message)   globalLogFunction(LogLevel::TRACE,   __FILE__, __LINE__, message, "" )
#define LOG_DEBUG(message)   globalLogFunction(LogLevel::DEBUG,   __FILE__, __LINE__, message, "" )

#define LOG_ERROR_FOR_TOPIC(message,topic)   globalLogFunction(LogLevel::ERROR,   __FILE__, __LINE__, message, topic )
#define LOG_WARNING_FOR_TOPIC(message,topic) globalLogFunction(LogLevel::WARNING, __FILE__, __LINE__, message, topic )
#define LOG_INFO_FOR_TOPIC(message,topic)    globalLogFunction(LogLevel::INFO,    __FILE__, __LINE__, message, topic )
#define LOG_TRACE_FOR_TOPIC(message,topic)   globalLogFunction(LogLevel::TRACE,   __FILE__, __LINE__, message, topic )
#define LOG_DEBUG_FOR_TOPIC(message,topic)   globalLogFunction(LogLevel::DEBUG,   __FILE__, __LINE__, message, topic )

std::string getVersion();

uint64_t get_timestamp_nano_utc();

void setLogFunction(LogFunction function);

void setTimeStampFunction(GetTimestampFunction function);


/* Calculates the average value of a maximal number of added items */
template<typename T>
class average_filter
{
private:
  std::deque<T> queue_;
  std::size_t last_n_values_to_consider_;

public:
  average_filter(std::size_t last_n_values_to_consider = 1):
      last_n_values_to_consider_(last_n_values_to_consider)
  {
      if(last_n_values_to_consider == 0) // for sanity reasons
          last_n_values_to_consider_ = 1;
  }

  void push(T val)
  {
      queue_.push_back(val);
      if(queue_.size() > last_n_values_to_consider_)
          queue_.pop_front();
  }

  T get_average() const
  {
      if (queue_.size() == 0)
          return 0;

      T sum = 0;
      for(const auto& element : queue_)
          sum += element;

      return sum/queue_.size();
  }

  size_t size() const
  {
    return queue_.size();
  }
};

} // end namespace
