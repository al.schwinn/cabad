/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <cabad/SampleMetaData.h>

#include <cabad/DeviceDataBufferBase.h>

namespace cabad
{

    // This constructor can be used when the meta item belongs to a WREvent
    SampleMetadata::SampleMetadata(std::shared_ptr<CircularBufferBase::iterator> related_sample,
                                   uint64_t                                        related_sample_absolute_offset,
                                   const std::shared_ptr<const TimingContext>&                  timingContext,
                                   uint64_t                                        related_sample_stamp,
                                   uint32_t                                        status,
                                   uint32_t                                        user_status):
                                                              related_sample_(related_sample),
                                                              related_sample_stamp_(related_sample_stamp),
                                                              related_sample_absolute_offset_(related_sample_absolute_offset),
                                                              timingContext_(timingContext),
                                                              user_delay_(0),
                                                              actual_delay_(0)

    {
        status_ = status;
        user_status_ = user_status;
    }

    // This constructor can be used when the meta item does not belong to a WREvent (idependant WR stamp from wr timing receiver)
    SampleMetadata::SampleMetadata(std::shared_ptr<CircularBufferBase::iterator> related_sample,
                                                          uint64_t related_sample_absolute_offset,
                                                          uint64_t related_sample_stamp,
                                                          uint32_t status,
                                                          uint32_t user_status) :
                                                              related_sample_(related_sample),
                                                              related_sample_stamp_(related_sample_stamp),
                                                              related_sample_absolute_offset_(related_sample_absolute_offset),
                                                              user_delay_(0),
                                                              actual_delay_(0)
    {
        status_ = status;
        user_status_ = user_status;
    }

    SampleMetadata& SampleMetadata::operator= (const SampleMetadata& other)
    {
        related_sample_ = other.related_sample_;
        related_sample_stamp_ = other.related_sample_stamp_;
        related_sample_absolute_offset_ = other.related_sample_absolute_offset_;
        timingContext_  = other.timingContext_;
        status_ = other.status_;
        user_status_ = other.user_status_;
        user_delay_ = other.user_delay_;
        actual_delay_ = other.actual_delay_;

        next_ = other.next_;
        prev_ = other.prev_;
        return *this;
    }

    std::ostream& operator<<(std::ostream& os, const SampleMetadata& meta)
    {
        if(!meta.getRelatedSample())
            os << "This meta-item has no related sample";
        else
            os << "Related Sample index: " << meta.getRelatedSample()->index() << " - Timestamp: " << meta.getRelatedSampleStamp();
        if(meta.hasTimingContext())
            os << " - EventNumber: " << meta.getTimingContext()->getEventNumber() << " EventStamp: " << meta.getTimingContextStamp();
        os << std::endl;
        return os;
    }

// ############## MetaDataBuffer ##############

    MetaDataBuffer::MetaDataBuffer(std::size_t           max_size,
                                   DeviceDataBufferBase* device_data_buffer) :
            max_size_(max_size),
            device_data_buffer_(device_data_buffer)
    {
        assert(max_size > 0);
    }

    MetaDataBuffer::~MetaDataBuffer()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if(buffer_.empty())
            return;
        for(auto& iter = buffer_.front(); iter; iter = iter->next_)
        {
            iter->next_ = std::shared_ptr<SampleMetadata>(nullptr);
            iter->prev_ = std::shared_ptr<SampleMetadata>(nullptr);
        }

        // clear by swapping with an empty queue
        std::deque<std::shared_ptr<SampleMetadata> > empty;
        std::swap( buffer_, empty );
    }

    void MetaDataBuffer::drop_oldest_element()
    {
        if(buffer_.empty())
            return;

        buffer_.pop_back();

        if(!buffer_.empty())
            buffer_.back()->prev_ = std::shared_ptr<SampleMetadata>(nullptr);
    }

    int MetaDataBuffer::push(const std::shared_ptr<SampleMetadata> metadata)
    {
        if(!metadata)
           throw RUNTIME_ERROR("Attempt to push nothing");
        std::lock_guard<std::mutex> lock(mutex_);

        if(buffer_.size() == 0)
        {
            buffer_.push_front(metadata);
            return 0;
        }

        if(metadata->hasTimingContext() && buffer_.front()->hasTimingContext())
        {
            // Only stricly increasing timestamps are permitted
            if(metadata->getTimingContext()->getTimeStamp() <= buffer_.front()->getTimingContext()->getTimeStamp())
                return -1; //TODO: Log Warn / Error ?
        }

        buffer_.front()->next_ = metadata;
        metadata->prev_ = buffer_.front();
        metadata->next_ = std::shared_ptr<SampleMetadata>(nullptr);
        buffer_.push_front(metadata);

        if(buffer_.size() > max_size_)
          drop_oldest_element();
        return 0;
    }

    std::shared_ptr<SampleMetadata> MetaDataBuffer::getNextTriggerMeta(const std::shared_ptr<const SampleMetadata>& current_trigger_meta)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if(buffer_.empty())
            throw RUNTIME_ERROR("logical error.");
        std::shared_ptr<SampleMetadata> start;

        if(current_trigger_meta == buffer_.front())
            return std::shared_ptr<SampleMetadata>(nullptr);

        // If no current trigger meta is specified, start search by oldest known meta
        if(current_trigger_meta && current_trigger_meta->next_)
            start = current_trigger_meta->next_;
        else
            start = buffer_.back();

        for(auto& iter = start; iter; iter = iter->next_)
        {
            if (iter->hasTimingContext())
             {
                unsigned int eventNumber = iter->getTimingContext()->getEventNumber();
                if (device_data_buffer_->isTriggerEvent(eventNumber))
                    return iter;
             }
        }

        //nothing found
        return std::shared_ptr<SampleMetadata>(nullptr);
    }

    std::shared_ptr<SampleMetadata> MetaDataBuffer::findTriggerMeta(int64_t trigger_stamp)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if(buffer_.empty())
            return std::shared_ptr<SampleMetadata>(nullptr);

        for(const auto& iter : buffer_)
        {
            if (iter->hasTimingContext())
             {
                unsigned int eventNumber = iter->getTimingContext()->getEventNumber();
                if (device_data_buffer_->isTriggerEvent(eventNumber) && iter->getTimingContextStamp() == trigger_stamp)
                    return iter;
             }
        }

        //nothing found
        return std::shared_ptr<SampleMetadata>(nullptr);
    }

    // searches for the oldest ref trigger in the given window
    // WIll continue to serach outside the window, if no ref trigger was found in the window
    std::shared_ptr<const SampleMetadata> MetaDataBuffer::findRefMeta(const std::shared_ptr<const SampleMetadata>& window_start,
                                                                         const std::shared_ptr<const SampleMetadata>& window_end)
    {
        if(!window_start || !window_end)
            throw RUNTIME_ERROR("Invalid argument passed");
        std::lock_guard<std::mutex> lock(mutex_);

        if(buffer_.empty())
           throw RUNTIME_ERROR("The buffer is empty");

        std::shared_ptr<const SampleMetadata> match;
        bool start_passed = false;

        for(auto iter = window_end; iter; iter = iter->prev_)
        {
            if (device_data_buffer_->isRefMetaTrigger(iter))
            {
                match = iter;
                // If we searched the whole window, we will continue to search older TimingContexts
                // And return the first refMeta we can find
                if (start_passed)
                    return match;
            }

            // If we find a ref-trigger inside the window, we use that ref-trigger
            if(iter == window_start && match)
                return match;
            if(iter == window_start)
                start_passed = true;
        }

        //nothing found
        return std::shared_ptr<SampleMetadata>(nullptr);
    }

    std::shared_ptr<SampleMetadata> MetaDataBuffer::getNextSequenceMarker(const std::shared_ptr<const SampleMetadata>& last_sequence_marker)
    {
        std::lock_guard<std::mutex> lock(mutex_);

        if(buffer_.empty())
           throw RUNTIME_ERROR("The buffer is empty");

        std::shared_ptr<SampleMetadata> iter;
        if(last_sequence_marker)
        {
           // cycled out
            if( buffer_.back()->related_sample_stamp_ <= last_sequence_marker->related_sample_stamp_ && last_sequence_marker->next_)
            {
                iter = last_sequence_marker->next_;
            }
        }

        if(!iter)
        {
            iter = buffer_.back();
        }

        for(; iter; iter = iter->next_)
        {
            if (iter->hasTimingContext())
             {
                // We want either a new meta, or  not the old one again
                if(last_sequence_marker && iter->getTimingContextStamp() == last_sequence_marker->getTimingContextStamp())
                    continue;

                if(iter->getTimingContext()->getEventNumber() == EVENT_NO_SEQ_START || iter->getTimingContext()->getEventNumber() == EVENT_NO_GAP_START)
                    return iter;
             }
        }

        // Not found.
        return std::shared_ptr<SampleMetadata>(nullptr);;
    }

    // get the oldest metadata which is available
    std::shared_ptr<SampleMetadata> MetaDataBuffer::back()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if(buffer_.empty())
           throw RUNTIME_ERROR("The buffer is empty");
        return buffer_.back();
    }

    // get the most recent metadata which is available
    std::shared_ptr<SampleMetadata> MetaDataBuffer::front()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if(buffer_.empty())
           throw RUNTIME_ERROR("The buffer is empty");

        return buffer_.front();
    }

bool MetaDataBuffer::isCycledOut(const std::shared_ptr<const SampleMetadata>& metadata)
{
    if(buffer_.empty())
        return true;
    if(!metadata->next_ && !metadata->prev_ && buffer_.size() != 1)
        return true;

    std::lock_guard<std::mutex> lock(mutex_);

    if( buffer_.back()->related_sample_stamp_ > metadata->related_sample_stamp_)
        return true;

    return false;
}

std::ostream& operator<<(std::ostream& os, const MetaDataBuffer& buffer)
{
    os << "### MetaDataBuffer ### - max_size: " << buffer.max_size_ << std::endl;
    for(auto& iter : buffer.buffer_)
        os << *iter;
    os << "####################################" << std::endl;
    return os;
}

} /* namespace cabad */
