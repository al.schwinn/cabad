/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <cabad/DataReadyManager.h>
#include <cabad/CircularBufferManagerBase.h>
#include <iostream>
namespace cabad
{

DataReadyManager::DataReadyManager(AllDataReadyFunction cb_allDataReady,
                                   std::size_t dataReadyQueueSize):
                                   cb_allDataReady_(cb_allDataReady),
                                   dataReadyQueue_(dataReadyQueueSize)
{
}

DataReadyManager::~DataReadyManager()
{
    for (auto& handler :data_ready_handlers_streaming_)
    {
        delete handler;
    }
    for (auto& handler :data_ready_handlers_triggered_)
    {
        delete handler;
    }
}

DataReadyHandlerStreaming* DataReadyManager::getStreamingDataReadyHandler(const std::string& notification_group,
                                                                          ClientNotificationType clientNotificationType,
                                                                          MaxClientUpdateFrequencyType maxClientUpdateFrequency,
                                                                          float sample_rate_hz,
                                                                          CircularBufferManagerBase* circularBufferManager)
{
    for(auto& data_ready_handler : data_ready_handlers_streaming_)
    {
        if(data_ready_handler->getClientNotificationType() == clientNotificationType &&
           data_ready_handler->getMaxClientUpdateFrequency() == maxClientUpdateFrequency &&
           data_ready_handler->getSampleRateHz() == sample_rate_hz &&
           data_ready_handler->getNotificationGroup() == notification_group)
        {
            data_ready_handler->connect(circularBufferManager);
            return data_ready_handler;
        }
    }

    // Not found ? Create a new one
    DataReadyHandlerStreaming* handler = new DataReadyHandlerStreaming(notification_group,
                                                                       clientNotificationType,
                                                                       maxClientUpdateFrequency,
                                                                       sample_rate_hz,
                                                                       &dataReadyQueue_,
                                                                       cb_allDataReady_);
    handler->connect(circularBufferManager);
    data_ready_handlers_streaming_.push_back(handler);
    return handler;
}

DataReadyHandlerTriggered* DataReadyManager::getTriggeredDataReadyHandler(const std::string& notification_group,
                                                                          ClientNotificationType clientNotificationType,
                                                                          EventNumber event_number,
                                                                          float sample_rate_hz,
                                                                          CircularBufferManagerBase* circularBufferManager)
{
    for(auto& data_ready_handler : data_ready_handlers_triggered_)
    {
        if(data_ready_handler->getClientNotificationType() == clientNotificationType &&
           data_ready_handler->getEventNumber() == event_number &&
           data_ready_handler->getSampleRateHz() == sample_rate_hz &&
           data_ready_handler->getNotificationGroup() == notification_group)
        {
            data_ready_handler->connect(circularBufferManager);
            return data_ready_handler;
        }
    }

    // Not found ? Create a new one
    DataReadyHandlerTriggered* handler = new DataReadyHandlerTriggered(notification_group,
                                                                       clientNotificationType,
                                                                       event_number,
                                                                       sample_rate_hz,
                                                                       &dataReadyQueue_,
                                                                       cb_allDataReady_);
    handler->connect(circularBufferManager);
    data_ready_handlers_triggered_.push_back(handler);
    return handler;
}

bool DataReadyManager::getDataByID(uint32_t id, ClientNotificationData& data)
{
    return dataReadyQueue_.requestData(id, data);
}

bool DataReadyManager::doTheseCircularBuffersShareTheSameDataReadyHandler(std::vector<std::string> circularBufferNames,
                                                                          ClientNotificationType clientNotificationType,
                                                                          MaxClientUpdateFrequencyType maxClientUpdateFrequency)
{
    if(clientNotificationType & ClientNotificationType::STREAMING)
        return doTheseCircularBuffersShareTheSameStreamingDataReadyHandler(circularBufferNames, clientNotificationType, maxClientUpdateFrequency);
    else
        return doTheseCircularBuffersShareTheSameTriggeredDataReadyHandler(circularBufferNames, clientNotificationType);
}

bool DataReadyManager::doTheseCircularBuffersShareTheSameStreamingDataReadyHandler(std::vector<std::string> circularBufferNames,
                                                                          ClientNotificationType clientNotificationType,
                                                                          MaxClientUpdateFrequencyType maxClientUpdateFrequency)
{
    // No buffers do not share anything
    if(circularBufferNames.empty())
        return false;

    if(circularBufferNames.size() == 1)
        return true;

    for(auto& data_ready_handler : data_ready_handlers_streaming_)
    {
        // As sonn as we have a match, we just check if this data ready handler as well holds the remaining circular buffers
        if( data_ready_handler->hasCircularBuffer(*(circularBufferNames.begin()), clientNotificationType, maxClientUpdateFrequency))
        {
            for(auto& circularBufferName :circularBufferNames)
            {
                if (data_ready_handler->hasCircularBuffer(circularBufferName, clientNotificationType, maxClientUpdateFrequency) == false)
                    return false;
            }
            return true;
        }
    }
    return false;
}

bool DataReadyManager::doTheseCircularBuffersShareTheSameTriggeredDataReadyHandler(std::vector<std::string> circularBufferNames,
                                                                          ClientNotificationType clientNotificationType)
{
    // No buffers do not share anything
    if(circularBufferNames.empty())
        return false;

    if(circularBufferNames.size() == 1)
        return true;

    for(auto& data_ready_handler : data_ready_handlers_triggered_)
    {
        // As sonn as we have a match, we just check if this data ready handler as well holds the remaining circular buffers
        if( data_ready_handler->hasCircularBuffer(*(circularBufferNames.begin()), clientNotificationType))
        {
            for(auto& circularBufferName :circularBufferNames)
            {
                if (data_ready_handler->hasCircularBuffer(circularBufferName, clientNotificationType) == false)
                    return false;
            }
            return true;
        }
    }
    return false;
}


} /* namespace cabad */
