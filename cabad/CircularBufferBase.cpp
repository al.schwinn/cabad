/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <cabad/CircularBufferBase.h>
#include <cabad/Definitions.h>
#include <cabad/Util.h>
#include <iostream>

namespace cabad
{
    CircularBufferBase::CircularBufferBase(std::size_t size) :
        size_(size), n_data_pushed_total_(0)
    {

    }

    CircularBufferBase::~CircularBufferBase()
    {
        write_iterator_ = std::shared_ptr<CircularBufferBase::iterator>(nullptr);
    }

    std::shared_ptr<CircularBufferBase::iterator> CircularBufferBase::getReadIterator(int32_t index)
    {
        while(index < 0)
            index += size_;
        if(index != 0 )
           index = index % size_;
        std::shared_ptr<CircularBufferBase::iterator> iter(new_iterator(index));
        return iter;
    }

    std::shared_ptr<CircularBufferBase::iterator> CircularBufferBase::getLatest()
    {
       return getReadIterator(int32_t(write_iterator_->index()) - 1);
    }

    std::shared_ptr<CircularBufferBase::iterator> CircularBufferBase::getCopyOfWriteIterator()
    {
        return getReadIterator(write_iterator_->index());
    }

    std::shared_ptr<CircularBufferBase::iterator> CircularBufferBase::getCopyOfPrevWriteIterator()
    {
        return getReadIterator(previous_write_iterator_->index());
    }

    std::shared_ptr<CircularBufferBase::iterator> CircularBufferBase::next(const CircularBufferBase::iterator& iter)
    {
        return getReadIterator(iter.index() + 1);
    }

    std::shared_ptr<CircularBufferBase::iterator> CircularBufferBase::prev(const CircularBufferBase::iterator& iter)
    {
        return getReadIterator(iter.index() - 1);
    }

    bool CircularBufferBase::was_overwritten(const CircularBufferBase::iterator& iter) const
    {
    	// uninitialized iterators cannot be overwritten
    	if (iter.index() == size_)
    		return false;

    	if (previous_write_iterator_)
    	  return iter.isBetween(*previous_write_iterator_, *write_iterator_);

    	// If the write iterator was not moved yet, no overwrite can happen
    	return false;
    }

    int32_t CircularBufferBase::valid_distance_between(const CircularBufferBase::iterator& iter1, const CircularBufferBase::iterator& iter2) const
    {
        std::size_t distance = iter1.distance_to(iter2);
        std::size_t distance_to_write_iter = this->distance_to_write_iter(iter1);
        if(distance == distance_to_write_iter)
        {
            LOG_ERROR("There is no valid distance between this iterator and the write iterator.");
            return distance;
        }
        else if(distance > distance_to_write_iter)
        {
            return -1 * iter2.distance_to (iter1);
        }
        else
        {
            return distance;
        }
    }

// ######################### iteratror stuff ################################

    CircularBufferBase::iterator::iterator (): CircularBufferBase::iterator::iterator(0, 0)
    {
    }

    CircularBufferBase::iterator::iterator (std::size_t circularSize, std::size_t index):
        circularSize_(circularSize)
    {
        // Explicitly allow this case to support buffer.end(), which is at index "circularSize_"
        if(index == circularSize_ )
            index_ = circularSize_;
        else
        {
            if( index == 0 )
                index_ = 0;
            else
                index_ = index % circularSize_;
        }
    }

    bool CircularBufferBase::iterator::operator==(const CircularBufferBase::iterator& rhs) const
    {
        return index_ == rhs.index_ && equalsOther(rhs);
    }

    bool CircularBufferBase::iterator::operator!=(const CircularBufferBase::iterator& rhs) const
    {
        return !(*this == rhs);
    }

    CircularBufferBase::iterator& CircularBufferBase::iterator::operator+= (std::size_t length)
    {
        index_ = ( length + index_ ) % circularSize_;
        setDataPointerToIndex();
        return *this;
    }

    CircularBufferBase::iterator& CircularBufferBase::iterator::operator-= (std::size_t length)
    {
        length = length % circularSize_;
        if( length > index_ )
            index_ = circularSize_ - ( length - index_);
        else
            index_ -= length;
        setDataPointerToIndex();
        return *this;
    }

    CircularBufferBase::iterator& CircularBufferBase::iterator::operator= (const CircularBufferBase::iterator& other)
    {
        if (this != &other)
        {
            copyDataPointerFrom(other);
            circularSize_ = other.circularSize_;
            index_ = other.index_;
            setDataPointerToIndex();
        }
        return *this;
    }

    CircularBufferBase::iterator& CircularBufferBase::iterator::operator++ ()
    {
        index_ = (index_ + 1) % circularSize_;
        setDataPointerToIndex();
        return *this;
    }

    CircularBufferBase::iterator& CircularBufferBase::iterator::operator-- ()
    {
        if( index_ == 0 )
        {
            index_ = circularSize_-1;
            setDataPointerToIndex();
            return *this;
        }
        index_ = (index_ - 1) % circularSize_;
        setDataPointerToIndex();
        return *this;
    }

    void CircularBufferBase::iterator::setToEnd()
    {
        index_ = circularSize_;
        setDataPointerToIndex();
    }

    std::size_t CircularBufferBase::iterator::index() const
    {
        return index_;
    }

    std::size_t CircularBufferBase::iterator::prev_index() const
    {
        if(index_ == 0)
            return circularSize_ - 1;
        return index_ - 1;
    }

    void CircularBufferBase::iterator::reset_to(int32_t index)
    {
        while(index < 0)
            index += circularSize_;
        if( index == 0 )
            index_ = 0;
        else
            index_ = index % circularSize_;
        setDataPointerToIndex();
    }

    std::size_t CircularBufferBase::iterator::distanceTillEnd() const
    {
        return circularSize_ - index_ - 1;
    }

    std::size_t CircularBufferBase::iterator::getCircularSize() const
    {
        return circularSize_;
    }

    bool CircularBufferBase::iterator::isBetween(CircularBufferBase::iterator& start, CircularBufferBase::iterator& end) const
    {
        if( start.circularSize_ != circularSize_ || end.circularSize_ != circularSize_)
            return false;

        if( start.index() < end.index() )
        {
            if(index_ >=  start.index() && index_ <= end.index())
                return true;
        }
        else if( start.index() == end.index()) // all elements are in between
        {
                return true;
        }
        else // rollover
        {
            if(index_ <=  end.index() || index_ >= start.index())
                return true;
        }
        return false;
    }

    std::size_t CircularBufferBase::iterator::distance_to(const CircularBufferBase::iterator& other) const
    {
        if ( other.index_ >= this->index_ )
            return other.index_ - this->index_;
        else // rollover
            return this->distanceTillEnd() + other.index_ + 1;
    }

    std::ostream& operator<<(std::ostream& os, const CircularBufferBase::iterator& iter)
    {
        os << "Iterator at index: " << iter.index_ << std::endl;
        return os;
    }

    std::ostream& operator<<(std::ostream& os, const CircularBufferBase& buffer)
    {
        os << "WriteIterator at index: " << buffer.write_iterator_->index() << std::endl;
        return os;
    }
} /* namespace cabad */

