/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/Definitions.h>
#include <cabad/TimingContext.h>

#include <vector>
#include <string>
#include <deque>
#include <mutex>
#include <memory>

namespace cabad
{

class ClientNotificationData
{

public:

    ClientNotificationData(ClientNotificationType notificationType,
                           std::vector<std::string> sinkNames,
                           std::shared_ptr<const TimingContext> context,
                           MaxClientUpdateFrequencyType updateFrequency):
        notificationType_(notificationType),
        context_(context),
        sinkNames_(sinkNames),
        updateFrequency_(updateFrequency)
    {
    }

    ClientNotificationData()
    {
    }

    uint32_t id_;
    ClientNotificationType notificationType_;
    std::shared_ptr<const TimingContext> context_;
    std::vector<std::string> sinkNames_;
    MaxClientUpdateFrequencyType updateFrequency_; // only relevant for streaming mode
};

// In FESA clients are updated by sending a message through a "Notification Queue".
// Since it is not possible (yet) to transfere objects, We sent just an id_ through this FESA queue and later gather the real data from this queue by using the id
class DataReadyQueue
{
public:
    DataReadyQueue(std::size_t max_size);

    uint32_t push(std::vector<std::string>&                    sinkNames,
                  std::shared_ptr<const TimingContext>               context,
                  ClientNotificationType                       notificationType,
                  MaxClientUpdateFrequencyType                 maxClientUpdateFrequency);

    // returns true if found, false otherwise
    bool requestData(uint32_t id, ClientNotificationData& data);

private:
    std::mutex mutex_;
    std::deque< ClientNotificationData> queue_;

    std::size_t max_size_;
    uint32_t idCount_;
};

} /* namespace */
