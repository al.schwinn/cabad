/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <cabad/CircularWindowIterator.h>
#include <cabad/Util.h>

#include <cmath>
#include <sstream>

#define _1HZ_IN_NANOSECONDS  1000000000
#define _10HZ_IN_NANOSECONDS  100000000
#define _25HZ_IN_NANOSECONDS   40000000

namespace cabad
{

int CircularWindowIterator::startup_finished_count_ = 0;

CircularWindowIterator::CircularWindowIterator(std::string name,
                                               CircularBufferBase* circularBuffer,
                                               MetaDataBuffer* metadataBuffer,
                                               bool considerForStartupFinishedCount,
                                               DataPushMode dataPushMode,
                                               double sample_to_sample_distance):
                                                    data_start_(circularBuffer->pEnd()),
                                                    data_end_(circularBuffer->pEnd()),
                                                    circularBuffer_(circularBuffer),
                                                    metadataBuffer_(metadataBuffer),
                                                    startup_finished_(false),
                                                    dataPushMode_(dataPushMode),
                                                    disabled_(false),
                                                    name_(name),
                                                    sample_to_sample_distance_(sample_to_sample_distance),
                                                    stamp_last_sample_prev_(0),
													valid_(true)

{
    if(considerForStartupFinishedCount)
        startup_finished_count_ ++;
    else
        startup_finished_ = true;

    sample_to_sample_distance_nano_ = sample_to_sample_distance_ * 1000000000.;
}

CircularWindowIterator::CircularWindowIterator(MetaDataBuffer* metadataBuffer) : metadataBuffer_(metadataBuffer)
{

}

CircularWindowIterator& CircularWindowIterator::operator= (const CircularWindowIterator& other)
{
    if (this != &other)
    {
        circularBuffer_ = other.circularBuffer_;
        metadataBuffer_ = other.metadataBuffer_;
        meta_start_ = other.meta_start_;
        meta_end_ = other.meta_end_;
        ref_meta_ = other.ref_meta_;
        startup_finished_ = other.startup_finished_;
        dataPushMode_ = other.dataPushMode_;
        disabled_ = other.disabled_;
        name_ = other.name_;
        sample_to_sample_distance_ = other.sample_to_sample_distance_;
        sample_to_sample_distance_nano_ = other.sample_to_sample_distance_nano_;
        stamp_last_sample_prev_ = other.stamp_last_sample_prev_;

        data_start_ = circularBuffer_->pEnd();
        data_end_ = circularBuffer_->pEnd();
        data_start_->reset_to(other.data_start_->index());
        data_end_->reset_to(other.data_end_->index());
        valid_ = other.valid_;
    }
    return *this;
}

CircularWindowIterator::~CircularWindowIterator()
{

}

std::size_t CircularWindowIterator::windowSize()
{
    // distance 5 to 9  is 4 --> 5 samples
    return data_start_->distance_to(*data_end_) + 1;
}

void CircularWindowIterator::reset()
{
    meta_start_ = std::shared_ptr<SampleMetadata>(nullptr);
    meta_end_ = std::shared_ptr<SampleMetadata>(nullptr);
    ref_meta_ = std::shared_ptr<SampleMetadata>(nullptr);
    data_start_ = circularBuffer_->pEnd();
    data_end_ = circularBuffer_->pEnd();
}

std::string CircularWindowIterator::getName()
{
    return name_;
}

void CircularWindowIterator::startupFinished()
{
    if(!startup_finished_)
    {
        //std::cout << "CircularWindowIterator::startupFinished 1" << std::endl;
        startup_finished_count_ --;
        if(startup_finished_count_ == 0)
        {
            std::cout << "All Sequence Window Iterators of all Channels are online now" << std::endl;
            LOG_TRACE("All Sequence Window Iterators of all Channels are online now" );
        }
        startup_finished_ = true;
    }
}

void CircularWindowIterator::moveDataWindowTo(std::size_t n_samples, std::shared_ptr<CircularBufferBase::iterator> data_end, bool debug)
{
    if(n_samples == 0)
        return;

    int32_t data_start_index = data_end->index() - (n_samples - 1);
    if(data_start_index < 0)
        data_start_index += circularBuffer_->size();

    std::shared_ptr<CircularBufferBase::iterator> data_start = circularBuffer_->getReadIterator(data_start_index);
    std::shared_ptr<CircularBufferBase::iterator> old_start = circularBuffer_->getReadIterator(data_start_->index());
    std::shared_ptr<CircularBufferBase::iterator> old_end = circularBuffer_->getReadIterator(data_end_->index());

//    std::cout << "new start:" << data_start->index() << std::endl;
//    std::cout << "old start:" << data_start_->index() << std::endl;
//    std::cout << "new end  :" << data_end->index() << std::endl;

    *data_start_ = *data_start;
    *data_end_ = *data_end;

    // initially assume that our new window is valid
    valid_ = true;

    // Now make sure the write iterator is not inside the new window
    if(isInWindow(circularBuffer_->getCopyOfWriteIterator()))
    {
    	valid_ = false;
        std::ostringstream stream;
        if(old_start)
            stream << "old_start : " <<  old_start->index() << std::endl;
        if(old_end)
            stream << "old_end   : " << old_end->index() << std::endl;
        stream << "new_start : " << data_start->index() << std::endl;
        stream << "new_end   : " << data_end->index() << std::endl;
        stream << "write iter: " << circularBuffer_->getCopyOfWriteIterator()->index() << std::endl;
        LOG_ERROR("CircularWindowIterator::moveDataWindowTo - Fail, Window moved into write iterator. - n_samples: " + std::to_string(n_samples) + " " + stream.str());
    }
}

bool CircularWindowIterator::isInWindow(const std::shared_ptr<const CircularBufferBase::iterator>& sample) const
{
    if(!data_start_ || !data_end_ || !sample)
        return false;

    if(*data_start_ == *data_end_)
    	return *sample == *data_start_;

    return sample->isBetween(*data_start_, *data_end_ );
}

bool CircularWindowIterator::isInWindow(const std::shared_ptr<const TimingContext> timingContext) const
{
    for(const auto& iter : window_metadata_)
    {
        if(iter->hasTimingContext())
        {
            if( iter->getTimingContextStamp() == timingContext->getTimeStamp())
                return true;
        }
    }
    return false;
}

int64_t CircularWindowIterator::distanceRefMetaToWindow()
{
    if( !ref_meta_)
        throw RUNTIME_ERROR("The CircularWindowIterator'" + name_ + "' does not have a ref_meta (yet)");
    if( isInWindow(ref_meta_->getRelatedSample()))
    {
        return data_start_->distance_to(*(ref_meta_->getRelatedSample())) * -1; // negative distance
    }
    else
    {
        return ref_meta_->getRelatedSample()->distance_to(*data_start_);
    }
}

std::vector<float> CircularWindowIterator::getTimeSinceRefMetaForEachSample (uint64_t foreign_ref_meta_stamp)
{
    std::size_t window_size = windowSize();
    std::vector<float> timesSinceRefMeta(window_size);
    fillTimeSinceRefMetaForEachSample(&(timesSinceRefMeta[0]), window_size, foreign_ref_meta_stamp);
    return timesSinceRefMeta;
}

void CircularWindowIterator::fillTimeSinceRefMetaForEachSample(float*      timesSinceRefMeta,
                                                               std::size_t timesSinceRefMeta_size,
                                                               uint64_t    foreign_ref_meta_stamp)
{
    uint64_t ref_meta_stamp = foreign_ref_meta_stamp;
    if( ref_meta_stamp == 0)
    {
        if(!ref_meta_)
            throw RUNTIME_ERROR("The CircularWindowIterator'" + name_ + "' does not have a ref_meta (yet)");
        ref_meta_stamp = ref_meta_->getTimingContext()->getTimeStamp();
    }

    if(windowSize() > timesSinceRefMeta_size)
        throw RUNTIME_ERROR("The passed C-Array is to short");

    //printMeta();
    if(dataPushMode_ == DataPushMode::DISJUNCT_SINGLE_SAMPLES)
    {
        std::shared_ptr<CircularBufferBase::iterator> sample_iter = circularBuffer_->getReadIterator(data_start_->index());
        std::size_t sample_count = 0;
        //std::cout << "windowSize(): " <<  windowSize()<< std::endl;
        while (true)
        {
            if (window_metadata_.empty())
                throw RUNTIME_ERROR("No meta-window defined");
           //std::cout << "sample_count: " <<  sample_count<< std::endl;

            for(const auto& meta_iter : window_metadata_)
            {
                //std::cout << "meta_iter->getRelatedSample()->index()      : " << meta_iter->getRelatedSample()->index() << std::endl;
                //std::cout << "meta_count: " << std::endl;

                //std::cout << "sample_iter->index(): " << sample_iter->index() << std::endl;

                // For single sample mode, some samples will have two meta items. A raw one, and an item which holds a WRContext.
                // Ignore one of both in order to prevent duplicate, additional stamps
                if(meta_iter->getRelatedSample()->index() == sample_iter->index())
                {
                    timesSinceRefMeta[sample_count] = (float(int64_t(meta_iter->getRelatedSampleStamp() - ref_meta_stamp))/ 1000000000.);
                    break;
                }
            }

            if( sample_iter->index() == data_end_->index() )
                break;
            ++(*sample_iter);
            ++sample_count;
        }
    }
    else
    {
        if( foreign_ref_meta_stamp != 0 )
            throw RUNTIME_ERROR("Support for foreign ref_meta_stamp so far only for DataPushMode::DISJUNCT_SINGLE_SAMPLES");

        // Will be positive when WREvent happened after the related sample
        int64_t wREventOffsetToSample_ns = int64_t(ref_meta_stamp - ref_meta_->getRelatedSampleStamp());

        double distanceRefMetaToWindow_time = double(distanceRefMetaToWindow()) * sample_to_sample_distance_ - double(wREventOffsetToSample_ns) / 1000000000.;

        double stamp = distanceRefMetaToWindow_time;
        for (std::size_t i=0;i< windowSize();i++ )
        {
            timesSinceRefMeta[i] = (float(stamp));
            stamp += sample_to_sample_distance_;
        }

        //std::cout << "first sample index : "  << data_start_->index() << " stamp: " << ref_meta_->getTimingContext()->getTimeStamp() + int64_t(timesSinceRefMeta[0] * 1000000000) << std::endl;
        //std::cout << "last sample index  : "  << data_end_->index() << " stamp: " << ref_meta_->getTimingContext()->getTimeStamp() + int64_t(timesSinceRefMeta[windowSize()-1] * 1000000000) << std::endl;
    }
}

std::vector< std::shared_ptr<SampleMetadata> > CircularWindowIterator::getMetaDataCol()
{
    return window_metadata_;
}

uint32_t CircularWindowIterator::getWindowStatus()
{
    uint32_t sum_status = 0;
    for(const auto& meta_iter : window_metadata_)
      sum_status |= meta_iter->getStatus();

    return sum_status;
}

int CircularWindowIterator::timeSinceRefMetaSanityCheck(std::vector<float>& timeSinceRefMetaForEachSample,
                                                        uint64_t            foreign_ref_meta_stamp,
                                                        bool                enableLogging)
{
    if (timeSinceRefMetaForEachSample.empty())
        return 0;

    uint64_t ref_meta_stamp = foreign_ref_meta_stamp;
    if(ref_meta_stamp == 0)
    {
        if (!ref_meta_)
            return 0;
        if (ref_meta_->hasTimingContext() == false) // So far we only support WR Timing
            return 0;

        ref_meta_stamp = ref_meta_->getTimingContext()->getTimeStamp();
    }

    // check if timeSinceRefMeta is strictly increasing
    float last = std::nan("");
    for (auto& iter: timeSinceRefMetaForEachSample)
    {
        if (last != std::nan(""))
        {
            if (iter < last)
            {
                if (enableLogging)
                {
                    std::string message = "getTimeSinceRefMetaForEachSample - sanity check failed. timeSinceRefMeta have wrong order";
                    message += "old stamp: " + std::to_string (last) + " ";
                    message += "new stamp: " + std::to_string (iter);
                    LOG_WARNING (message);
                }
                return 1;
            }
            else if (iter == last)
            {
                if (enableLogging)
                {
                    std::string message = "getTimeSinceRefMetaForEachSample - sanity check failed. Same timeSinceRefMeta used twice";
                    message += "stamp: " + std::to_string (iter);
                    LOG_WARNING (message);
                }
                return 2;
            }
        }
        last = iter;
    }

    // check that timeSinceRefMeta from the last update does not overlap with the current update

    int64_t stamp_first_sample = ref_meta_stamp + (timeSinceRefMetaForEachSample.front() * 1000000000 );
    int64_t stamp_last_sample = ref_meta_stamp + (timeSinceRefMetaForEachSample.back() * 1000000000 );
    //std::cout << "stamp_last_sample_prev_: " << stamp_last_sample_prev_ << std::endl;
    //std::cout << "stamp_first_sample     : " << stamp_first_sample << std::endl;
    if (stamp_first_sample < stamp_last_sample_prev_)
    {
        if (enableLogging)
        {
            std::string message = "getTimeSinceRefMetaForEachSample - sanity check failed. timeSinceRefMeta across two updates have wrong order: ";
            message += "Previous last sample stamp: " + std::to_string (stamp_last_sample_prev_) + " ";
            message += "Next first sample stamp: " + std::to_string (stamp_first_sample);
            LOG_WARNING (message);
        }
        stamp_last_sample_prev_ = 0;
        return 3;
    }
    else if (stamp_first_sample == stamp_last_sample_prev_)
    {
        if (enableLogging)
        {
            std::string message = "getTimeSinceRefMetaForEachSample - sanity check failed. Same timeSinceRefMeta used twice across two updates";
            message += "stamp: " + std::to_string (stamp_last_sample_prev_);
            LOG_WARNING (message);
        }
        stamp_last_sample_prev_ = 0;
        return 4;
    }
    stamp_last_sample_prev_ = stamp_last_sample;
    return 0;
}

void CircularWindowIterator::updateMetaWindowAccordingToSampleWindow(bool update_ref_meta)
{
    bool window_found = false;

    window_metadata_.clear();

    if(!metadataBuffer_->empty())
    {
        // Add all metaitems which are located in this sample-window
        metadataBuffer_->for_each_meta_data([&] (const std::shared_ptr<SampleMetadata>& iter)
        {
            if(!iter->getRelatedSample())
            {
                LOG_ERROR ("Found a MetaData without a related sample. This should not happen.");
                return false;
            }

            if(iter->getRelatedSample() && isInWindow(iter->getRelatedSample()))
            {
                window_metadata_.push_back(iter);
                window_found = true;
            }
            else
            {
                // Once we left the window, we dont need to further search
                if(window_found)
                  return false; // this will break the for_each;
            }
            return true;
        });
    }

    // Update meta_start_ and meta_end_ accordingly
    if(window_metadata_.empty())
    {
        if(update_ref_meta)
        {
            if(meta_end_)
            {
                // Use previous end to search for the next ref-trigger, if no new meta was found
                ref_meta_ = metadataBuffer_->findRefMeta(meta_end_,meta_end_);
            }
			// Otherwise the previous ref-trigger will just be reused/kept
        }
        meta_start_ = std::shared_ptr<SampleMetadata>(nullptr);
        meta_end_ = std::shared_ptr<SampleMetadata>(nullptr);
    }
    else
    {
        meta_start_ = window_metadata_.back();
        meta_end_ = window_metadata_.front();
        if(update_ref_meta)
            ref_meta_ = metadataBuffer_->findRefMeta(meta_start_,meta_end_);
    }
}

int64_t CircularWindowIterator::getTimeStampOfLastSample()
{
    if(dataPushMode_ == DataPushMode::DISJUNCT_SINGLE_SAMPLES)
    {
        if (!meta_end_)
            return 0;
        return meta_end_->getRelatedSampleStamp();
    }
    else
    {
        if(!ref_meta_)
            return 0;

        int64_t ref_meta_stamp = ref_meta_->getTimingContext()->getTimeStamp();

        // Will be positive when WREvent happened after the related sample
        int64_t wREventOffsetToSample_ns = int64_t(ref_meta_stamp - ref_meta_->getRelatedSampleStamp());

        int64_t distanceRefMetaToWindow_ns = distanceRefMetaToWindow() * sample_to_sample_distance_nano_- wREventOffsetToSample_ns;
        return ref_meta_stamp + (distanceRefMetaToWindow_ns +  (windowSize() - 1) * sample_to_sample_distance_nano_);
    }
}

std::ostream& operator<<(std::ostream& os, const CircularWindowIterator& window)
{
    os << "WIN - Window Info for   Window : " << window.name_ << std::endl;
    os << "WIN - Write iterator      index: " << window.circularBuffer_->getCopyOfWriteIterator()->index() << std::endl;
    os << "WIN - Write iterator prev index: " << window.circularBuffer_->getCopyOfPrevWriteIterator()->index() << std::endl;
    os << "WIN - Window read iterator range(index): [" << window.data_start_->index() << "," << window.data_end_->index() << "]" << std::endl;

    if(window.window_metadata_.empty())
    {
        os << "WIN - No metadata found for that window" << std::endl;
        return os;
    }

    for(const auto& iter : window.window_metadata_)
    {
        if (!iter)
        {
            os << "WIN - Error, failed to traverse till meta_end !" << std::endl;
            if(window.meta_end_)
                os << "WIN - Meta End SampStamp: " << window.meta_end_->getRelatedSampleStamp() << " sampleIndex: " << window.meta_end_->getRelatedSample()->index() << std::endl;
            break;
        }
        os << "WIN - " << *iter;
    }

    if(window.ref_meta_)
    	os << "WIN - ref trigger: " << *(window.ref_meta_);

    return os;
}

void CircularWindowIterator::validate()
{
	// If some previous check failed, we keep that state
	if (!valid_)
		return;

	// Check if the last push operation went through our window (Size of a push is limited by the size of the buffer
	if(circularBuffer_->was_overwritten (*data_start_) || circularBuffer_->was_overwritten(*data_end_))
    {
		valid_ = false;
        std::ostringstream stream;
        stream << *this;
        LOG_ERROR("CircularWindowIterator::validate - Write iterator wrote into existing window. - " + stream.str());
    }
}

} /* namespace cabad */

