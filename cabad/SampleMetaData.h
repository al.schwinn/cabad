/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/CircularBuffer.h>
#include <cabad/TimingContext.h>

#include <deque>
#include <memory>

namespace cabad
{

namespace AcquisitionStatus
{
enum AcquisitionStatus
{
       // A Trigger meta could not be matched to its related timing context within the given tolerance window
       TRIGGER_CONTEXT_MATCHING_NO_MATCH          = 0x01,

       // Multiple timing context matches were found for a single trigger event
       TRIGGER_CONTEXT_MATCHING_MULTIPLE_MATCHES  = 0x02,
};
}

class SampleMetadata
{
public:



    // This constructor can be used when the meta item belongs to a WREvent
    SampleMetadata(std::shared_ptr<CircularBufferBase::iterator>  related_sample,
                   uint64_t                                       related_sample_absolute_offset,
                   const std::shared_ptr<const TimingContext>&    timingContext,
                   uint64_t                                       related_sample_stamp,
                   uint32_t                                       status = 0,
                   uint32_t                                       user_status = 0);

    // This constructor can be used when the meta item does not belong to a WREvent (idependant WR stamp from wr timing receiver)
    SampleMetadata(std::shared_ptr<CircularBufferBase::iterator> related_sample,
                   uint64_t                                      related_sample_absolute_offset,
                   uint64_t                                      related_sample_stamp,
                   uint32_t                                      status = 0,
                   uint32_t                                      user_status = 0);

    //Constructors needed for unit-testing
    SampleMetadata(const std::shared_ptr<const TimingContext>& timingContext):
            related_sample_stamp_(timingContext->getTimeStamp()),
            timingContext_(timingContext)
    {

    }

    SampleMetadata(std::shared_ptr<CircularBufferBase::iterator>& related_sample,
                   const std::shared_ptr<const TimingContext>& timingContext):
            related_sample_(related_sample),
            related_sample_stamp_(timingContext->getTimeStamp()),
            timingContext_(timingContext)
    {

    }

    SampleMetadata(std::shared_ptr<CircularBufferBase::iterator>& related_sample, uint64_t related_sample_stamp = 0):
            related_sample_(related_sample),
            related_sample_stamp_(related_sample_stamp)
    {

    }

    SampleMetadata(uint64_t related_sample_stamp = 0):
            related_sample_stamp_(related_sample_stamp),
            related_sample_absolute_offset_(0),
            status_(0),
            user_delay_(0),
            actual_delay_(0)
    {

    }

    ~SampleMetadata()
    {

    }

    SampleMetadata& operator= (const SampleMetadata& other);

    bool hasTimingContext() const
    {
        if(timingContext_)
            return true;
        return false;
    }

    std::shared_ptr<const TimingContext> getTimingContext() const
    {
        if(!timingContext_)
            throw RUNTIME_ERROR("This SampleMetadata has no Timing context");
        return timingContext_;
    }

    int64_t getTimingContextStamp() const
    {
        return timingContext_->getTimeStamp();
    }

    // Concrete stamp of the related sample
    int64_t getRelatedSampleStamp() const
    {
        return related_sample_stamp_;
    }

    uint64_t getRelatedSampleAbsoluteOffset() const
    {
        return related_sample_absolute_offset_;
    }

    std::shared_ptr<CircularBufferBase::iterator> getRelatedSample() const
    {
        return related_sample_;
    }

    uint32_t getStatus() const
    {
        return status_;
    }

    void setStatus(uint32_t status)
    {
        status_ = status;
    }

    std::shared_ptr<SampleMetadata> prev() const
    {
        return prev_;
    }
    std::shared_ptr<SampleMetadata> next() const
    {
        return next_;
    }

    // Attach any relevant printable info to the stream
    friend std::ostream& operator<<(std::ostream& os, const SampleMetadata& meta);

private:

    std::shared_ptr<CircularBufferBase::iterator> related_sample_;
    int64_t                                       related_sample_stamp_;
    uint64_t                                      related_sample_absolute_offset_;    // (number of items read from input stream in total)

    // Timing context which is very close to the related sample
    std::shared_ptr<const TimingContext> timingContext_;

	// Internal Status - bit enum, see "AcquisitionStatus"
    uint32_t status_;

    // next list element, which is newer than this element
    std::shared_ptr<SampleMetadata> next_;

    // previous list element, which is older than this element
    std::shared_ptr<SampleMetadata> prev_;

public:
    //TODO Split into hierachie of classes to make it customizable:
    // - base class only has "related sample" and "related_sample_absolute_offset_"
    // - "SampleStampData",  "SampleWRContextData", "SampleCurstomData"
    // "SampleStampData" and "SampleWRContextData" created by this lib, where "SampleCurstomData" optionally can be created by the user of the lib.
    uint32_t user_status_;           // acquisition status of Digitizers
    double user_delay_;         // comes from Digitizers ... realy needed ?
    double actual_delay_;       // comes from Digitizers ... realy needed ?

    friend class MetaDataBuffer;
};

class DeviceDataBufferBase;

// Linked list of SampleMetadata
class MetaDataBuffer
{

public:

    MetaDataBuffer(std::size_t max_size, DeviceDataBufferBase* device_data_buffer);

    ~MetaDataBuffer();

    /*
     * push:
     * @metadata : New metadata to be pushed into the buffer
     *
     * The new metadata will be pushed into the buffer, if it does not exist yet (checked by comparing TimingContext)
     * The new metadata will be inserted at the proper location, so that all metadata in the buffer will always be sorted chronologically
     *
     * Return value: 0 on sucess, -1 if a metadata with the same TImingContext already exists
     */
    int push(const std::shared_ptr<SampleMetadata> metadata);

    //empty if nothing is found
    std::shared_ptr<SampleMetadata> getNextTriggerMeta(const std::shared_ptr<const SampleMetadata>& current_trigger_meta);

    //empty if nothing is found
    std::shared_ptr<SampleMetadata> findTriggerMeta(int64_t trigger_stamp);

    // searches for the oldest ref trigger in the given window
    // Will continue to serach outside the window, if no ref trigger was found in the window
    // empty, if not found
    std::shared_ptr<const SampleMetadata> findRefMeta(const std::shared_ptr<const SampleMetadata>& window_start,
                                                         const std::shared_ptr<const SampleMetadata>& window_end);

    std::shared_ptr<SampleMetadata> getNextSequenceMarker(const std::shared_ptr<const SampleMetadata>& last_sequence_marker);

    // get the oldest metadata which is available
    std::shared_ptr<SampleMetadata> back();

    // get the most recent metadata which is available
    std::shared_ptr<SampleMetadata> front();

    // True if the timestamp of the passed metadata is older than the oldest element in the buffer
    bool isCycledOut(const std::shared_ptr<const SampleMetadata>& metadata);

    // Attach any relevant printable info to the stream
    friend std::ostream& operator<<(std::ostream& os, const MetaDataBuffer& buffer);

    bool empty()
    {
        return buffer_.empty();
    }

    template <class Callable>
    void for_each_meta_data(Callable callable)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if(buffer_.empty())
            return;

        for(std::deque<std::shared_ptr<SampleMetadata> >::iterator iter = buffer_.begin(); iter != buffer_.end(); iter++)
        {
            if (callable(*iter) == false)
                return;
        }
    }

private:

    // drops the oldest element of the buffer
    void drop_oldest_element();

    std::mutex mutex_;
    std::size_t max_size_;
    std::deque<std::shared_ptr<SampleMetadata> > buffer_;
    DeviceDataBufferBase* device_data_buffer_;
};


} /* namespace cabad */
