/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/Definitions.h>
#include <cabad/CircularBufferBase.h>

#include <vector>
#include <functional>
#include <mutex>
#include <iostream>
#include <memory>
#include <map>

namespace cabad
{

// Forward decleration
class CircularBufferManagerBase;

typedef std::function<void(uint32_t data_id)> AllDataReadyFunction;

class DataReadyQueue;

/* class to move data windows for a specific collection of CircularBuffers and to signal
 * that new data is ready to be picked up for a specific collection of CircularBuffers
 * The 'AllDataReadyFunction' will  only be called if all buffers of the Handler signal to be ready
 * Further information on which circular buffers are ready, etc. will be pushed to a queue */
class DataReadyHandler
{
public:

    DataReadyHandler(const std::string& notification_group,
                     ClientNotificationType clientNotificationType,
                     float sample_rate_hz,
                     DataReadyQueue* dataReadyQueue,
                     AllDataReadyFunction cb_allDataReady);

    virtual ~DataReadyHandler();

    virtual void connect(CircularBufferManagerBase* circularBufferManager) = 0;

    ClientNotificationType getClientNotificationType() const
    {
        return clientNotificationType_;
    }

    float getSampleRateHz() const
    {
        return sample_rate_hz_;
    }

    const std::string& getNotificationGroup() const
    {
        return notification_group_;
    }

protected:

    // Only circular buffers sharing the same notification group are allowed to share the same DataReadyHandler
    std::string notification_group_;

     // This DataReadyHandler only handles notifications for circular buffers which use the specified clientNotificationType
     ClientNotificationType clientNotificationType_;

     // DataReadyQueue used by all DataReadyHandler to push Info when all connected circular buffers signaled data ready
     DataReadyQueue* dataReadyQueue_;

     // Callback which will be executed when all connected circular signaled data ready for a specific context
     AllDataReadyFunction cbAllDataReady_;

     // mutex in order to coordinate window movement across different circular buffers
     std::mutex move_data_window_mutex_;

     // Only relevant for modes CONTINOUS_SINGLE_SAMPLES and CONTINOUS_MULTI_SAMPLES
     float sample_rate_hz_;

     std::vector<std::string> connected_circular_buffer_manager_names_;
};

class DataReadyHandlerStreaming : public DataReadyHandler
{
public:

    DataReadyHandlerStreaming(const std::string& notification_group,
                     ClientNotificationType clientNotificationType,
                     MaxClientUpdateFrequencyType maxClientupdateFrequency,
                     float sample_rate_hz,
                     DataReadyQueue* dataReadyQueue,
                     AllDataReadyFunction cb_allDataReady);

    virtual ~DataReadyHandlerStreaming();

    void connect(CircularBufferManagerBase* circularBufferManager) override;

    void requestMoveWindow (int64_t                    last_sample_stamp,
                                     std::size_t                n_new_samples,
                                     CircularBufferManagerBase* manager);

    bool hasCircularBuffer(std::string circularBufferName,
                           ClientNotificationType clientNotificationType,
                           MaxClientUpdateFrequencyType maxClientUpdateFrequency);

    MaxClientUpdateFrequencyType getMaxClientUpdateFrequency()
    {
        return maxClientUpdateFrequency_;
    }

    // Attach any relevant printable info to the stream
    friend std::ostream& operator<<(std::ostream& os, const DataReadyHandlerStreaming& handler);

private:

    bool requestMoveStreamingWindow(int64_t last_sample_stamp, std::size_t n_new_samples);

    bool moveWindows(int64_t sync_move_stamp, std::size_t n_samples);

    struct AvailableData
    {
        int64_t       last_sample_stamp_;
        std::size_t   n_new_samples_;
    };

    // This DataReadyHandler only handles notifications for circular buffers which use the specified sample rate and update Frequency
    MaxClientUpdateFrequencyType maxClientUpdateFrequency_;

    // stamp when the last streaming update happened
    uint64_t last_streaming_update_;

    // minimum delay between two client updates
    uint64_t min_client_update_delay_ns_;

    // List of circular buffers which connected to this DataReadyHandler
    // And their relevant Data
    std::map<CircularBufferManagerBase*, AvailableData> connected_circular_buffer_managers_;
};

class DataReadyHandlerTriggered : public DataReadyHandler
{

public:

    DataReadyHandlerTriggered(const std::string& notification_group,
                              ClientNotificationType clientNotificationType,
                              EventNumber event_number,
                              float sample_rate_hz,
                              DataReadyQueue* dataReadyQueue,
                              AllDataReadyFunction cb_allDataReady);

    virtual ~DataReadyHandlerTriggered();

    void connect(CircularBufferManagerBase* circularBufferManager) override;

    void requestMoveWindow(int64_t trigger_stamp,
                                    CircularBufferManagerBase* manager);

    bool hasCircularBuffer(std::string circularBufferName,
                           ClientNotificationType clientNotificationType);

    EventNumber getEventNumber()
    {
        return event_number_;
    }

    // Attach any relevant printable info to the stream
    friend std::ostream& operator<<(std::ostream& os, const DataReadyHandlerTriggered& handler);

private:

    // will move all window up to the window of 'trigger stamp'
    void moveWindows(int64_t trigger_stamp);

    struct AvailableData
    {
        int64_t last_trigger_stamp_;
    };

    // This DataReadyHandler only handles notifications for the specified event_number
    EventNumber event_number_;

    // List of circular buffers which connected to this DataReadyHandler
    // And their relevant Data
    std::map<CircularBufferManagerBase*, AvailableData> connected_circular_buffer_managers_;
};

} /* namespace cabad */
