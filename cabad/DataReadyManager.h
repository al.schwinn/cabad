/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/Definitions.h>

#include <cabad/DataReadyHandler.h>
#include <cabad/DataReadyQueue.h>

#include <vector>

// Forward decleration
class CircularBufferManagerBase;

namespace cabad
{

/* class to manage different data ready handlers in order to synchronize and bundle data-ready updates of different circular buffers */
/* All circular buffer which share the same ClientNotificationType, clientUpdateFrequency and the same sample rate will be updated at once */
class DataReadyManager
{
public:

    DataReadyManager(AllDataReadyFunction cb_allDataReady,
                    std::size_t dataReadyQueueSize);

    ~DataReadyManager();

    DataReadyHandlerStreaming* getStreamingDataReadyHandler(const std::string& notification_group,
                                                            ClientNotificationType clientNotificationType,
                                                            MaxClientUpdateFrequencyType maxClientUpdateFrequency,
                                                            float sample_rate_hz,
                                                            CircularBufferManagerBase* circularBufferManager);

    DataReadyHandlerTriggered* getTriggeredDataReadyHandler(const std::string& notification_group,
                                                            ClientNotificationType clientNotificationType,
                                                            EventNumber event_number,
                                                            float sample_rate_hz,
                                                            CircularBufferManagerBase* circularBufferManager);

    // returns true if data found, false otherwise
    bool getDataByID(uint32_t id, ClientNotificationData& data);

    bool doTheseCircularBuffersShareTheSameDataReadyHandler(std::vector<std::string> circularBufferNames,
                                                            ClientNotificationType clientNotificationType,
                                                            MaxClientUpdateFrequencyType maxClientUpdateFrequency);

    bool doTheseCircularBuffersShareTheSameStreamingDataReadyHandler(std::vector<std::string> circularBufferNames,
                                                            ClientNotificationType clientNotificationType,
                                                            MaxClientUpdateFrequencyType maxClientUpdateFrequency);
    bool doTheseCircularBuffersShareTheSameTriggeredDataReadyHandler(std::vector<std::string> circularBufferNames,
                                                            ClientNotificationType clientNotificationType);

private:

    std::vector<DataReadyHandlerStreaming*> data_ready_handlers_streaming_;

    std::vector<DataReadyHandlerTriggered*> data_ready_handlers_triggered_;

    AllDataReadyFunction cb_allDataReady_;

    DataReadyQueue dataReadyQueue_;
};

} /* namespace */
