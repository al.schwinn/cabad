/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <cabad/DataReadyQueue.h>

namespace cabad
{

DataReadyQueue::DataReadyQueue(std::size_t max_size): max_size_(max_size)
{


}

uint32_t DataReadyQueue::push(std::vector<std::string>&      sinkNames,
                              std::shared_ptr<const TimingContext> context,
                              ClientNotificationType         notificationType,
                              MaxClientUpdateFrequencyType   maxClientUpdateFrequency)
{
    //std::cout << "DataReadyQueue::push start" << std::endl;
    ClientNotificationData data(notificationType, sinkNames, context, maxClientUpdateFrequency);// update frequency is irrelevant for triggered mode
    std::lock_guard<std::mutex> lock(mutex_);
    data.id_ = ++idCount_;
    queue_.push_back (data);
    if(queue_.size() > max_size_)
        queue_.pop_front();
    return data.id_;
}

bool DataReadyQueue::requestData(uint32_t id, ClientNotificationData& data )
{
    std::lock_guard<std::mutex> lock(mutex_);
    for(std::deque< ClientNotificationData>::iterator iter = queue_.begin(); iter != queue_.end(); iter ++)
    {
        if(iter->id_ == id)
        {
            data.context_ = iter->context_;
            data.sinkNames_ = iter->sinkNames_;
            data.notificationType_ = iter->notificationType_;
            data.updateFrequency_ = iter->updateFrequency_;
            data.id_ = iter->id_;
            return true;
        }
    }
    return false;
}


} /* namespace */
