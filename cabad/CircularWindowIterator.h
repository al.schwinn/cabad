/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/CircularBufferBase.h>
#include <cabad/SampleMetaData.h>
#include <cabad/Definitions.h>

#include <mutex>
#include <vector>

namespace cabad
{

/* Keeps track on a concrete data window and alll containing metadata in that window */
class CircularWindowIterator
{

public:

    CircularWindowIterator(std::string name,
              CircularBufferBase* circularBuffer,
              MetaDataBuffer* metadataBuffer,
              bool considerForStartupFinishedCount,
              DataPushMode dataPushMode,
              double sample_to_sample_distance);

    // TODO: Drop this Ctor as soon as the Fesa-Class does not require it any more !
    CircularWindowIterator()
    {
    }

    CircularWindowIterator(MetaDataBuffer* metadataBuffer);

    CircularWindowIterator& operator= (const CircularWindowIterator& other);

    virtual ~CircularWindowIterator();

    std::size_t windowSize();

    void reset();

    std::string getName();

    std::shared_ptr<CircularBufferBase::iterator> data_start_;
    std::shared_ptr<CircularBufferBase::iterator> data_end_;

    // All meta tags in the related data-window
    std::shared_ptr<SampleMetadata> meta_start_;
    std::shared_ptr<SampleMetadata> meta_end_;

    bool isStartupFinished();

    void startupFinished();

    // Moves the datawindow up to the specified data_end
    // The new window will have the length n_samles
    void moveDataWindowTo(std::size_t n_samples, std::shared_ptr<CircularBufferBase::iterator> data_end, bool debug = false);

    bool disabled()
    {
        return disabled_;
    }

    void setRefMeta(const std::shared_ptr<const SampleMetadata>& refMeta)
    {
        ref_meta_ = refMeta;
    }

    std::shared_ptr<const SampleMetadata> getRefMeta()
    {
        return ref_meta_;
    }

    void resetRefMeta()
    {
        ref_meta_ = std::shared_ptr<SampleMetadata>(nullptr);
    }

    // distance between ref trigger and first sample of the window
    // Positive if ref trigger happened before window
    // Is 0 if ref trigger is on first sample
    // can be negative, if ref-trigger is inside window
    int64_t distanceRefMetaToWindow();

    bool isInWindow(const std::shared_ptr<const  CircularBufferBase::iterator>& sample) const;

    // Check if a meta item with the given timing context is in this window
    bool isInWindow(const std::shared_ptr<const TimingContext> timingContext) const;

    // Time in seconds from each sample to the ref triigger of this window (can be negative)
    // optionally add foreign ref trigger stamp (e.g. if there is no timing)
    // Note: Use "fillTimeSinceRefMetaForEachSample" for better performance if you work with C-Arrays
    std::vector<float> getTimeSinceRefMetaForEachSample(uint64_t foreign_ref_meta_stamp = 0);

     // Time in seconds from each sample to the ref triigger of this window (can be negative)
     // optionally add foreign ref trigger stamp (e.g. if there is no timing)
    void fillTimeSinceRefMetaForEachSample(float*      timesSinceRefMeta,
                                           std::size_t timesSinceRefMeta_size,
                                           uint64_t    foreign_ref_meta_stamp = 0);

    int64_t getTimeStampOfLastSample();

    std::vector< std::shared_ptr<SampleMetadata> > getMetaDataCol();

    uint32_t getWindowStatus();

    // Check if timeSinceRefMeta on each sample have increasing order, as well across multiple updates
    // returns 0 if check passed, an error code otherwise (check the code for the meaning)
    int timeSinceRefMetaSanityCheck(std::vector<float>& timeSinceRefMetaForEachSample,
                                    uint64_t            foreign_ref_meta_stamp = 0,
                                    bool                enableLogging = false);

    // Find and set the metadata (and optionally reference trigger), according to the current samples
    void updateMetaWindowAccordingToSampleWindow(bool update_ref_meta);

    // TRUE If that window was not used so far
    bool not_used_yet()
    {
    	return *data_start_ == *(circularBuffer_->pEnd()) || *data_end_ == *(circularBuffer_->pEnd());
    }

    bool valid()
    {
        return valid_;
    }

    // Check if the latest push operation invalidated this window
    // (To be executed only after each push operation, later executions might give a false positive)
    void validate();

    // Attach conent of this window to a stream
    friend std::ostream& operator<<(std::ostream& os, const CircularWindowIterator& window);

private:

    // All meta tags in the related data-window
    // if there are no new meta tags in this window, both will point to the last valid metadata
    std::vector< std::shared_ptr<SampleMetadata> > window_metadata_;

    // The CircularBuffer on which this window iter operates
    CircularBufferBase* circularBuffer_;

    // The metadataBuffer on which this window iter operates
    MetaDataBuffer* metadataBuffer_;

    // Reference meta which will be used to calculate relative sample times (can be a trigger meta or a timestamp meta)
    std::shared_ptr<const SampleMetadata> ref_meta_;

    bool startup_finished_;

    static int startup_finished_count_;

    DataPushMode dataPushMode_;

    // This window is not used
    bool disabled_;

    // E.g. "signalName::triggered" for triggered window iterators ... useful for debugging
    std::string name_;

    // Distance between two samples in seconds
    double sample_to_sample_distance_;

    // Distance between two samples in nanoseconds
    double sample_to_sample_distance_nano_;

    // stamp of the last sample on the latest window update
    // only used for timeSinceRefMetaSanityCheck
    int64_t stamp_last_sample_prev_;

    // If the write iterator did not overwrite any sample of this window iterator, it is considered to be valid
    bool valid_;
};

} /* namespace cabad */
