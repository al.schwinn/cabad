/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <cabad/DataReadyHandler.h>
#include <cabad/DataReadyQueue.h>
#include <cabad/Util.h>
#include <cabad/CircularBufferManagerBase.h>

#include <iostream>
#include <algorithm>
#include <sstream>

namespace cabad
{

DataReadyHandler::DataReadyHandler(const std::string& notification_group,
                                   ClientNotificationType clientNotificationType,
                                   float sample_rate_hz,
                                   DataReadyQueue* dataReadyQueue,
                                   AllDataReadyFunction cb_allDataReady):
                                        notification_group_(notification_group),
                                        clientNotificationType_(clientNotificationType),
                                        dataReadyQueue_(dataReadyQueue),
                                        cbAllDataReady_(cb_allDataReady),
                                        sample_rate_hz_(sample_rate_hz)
{

}

DataReadyHandler::~DataReadyHandler()
{
    connected_circular_buffer_manager_names_.clear();

}

// ########################################################################
// ############ DataReadyHandlerStreaming #################################
// ########################################################################

DataReadyHandlerStreaming::DataReadyHandlerStreaming(const std::string& notification_group,
                                                     ClientNotificationType clientNotificationType,
                                                     MaxClientUpdateFrequencyType maxClientupdateFrequency,
                                                     float sample_rate_hz,
                                                     DataReadyQueue* dataReadyQueue,
                                                     AllDataReadyFunction cb_allDataReady):
                                                        DataReadyHandler(notification_group,
                                                                         clientNotificationType,
                                                                         sample_rate_hz,
                                                                         dataReadyQueue,
                                                                         cb_allDataReady),
                                                        maxClientUpdateFrequency_(maxClientupdateFrequency)

{
    // Mainly required for unit-testing to initialize to now instead of '0'
    // In order to make sure that the first update only happens after min_client_update_delay_ns_, not emmediatly
    last_streaming_update_ = get_timestamp_nano_utc();

    min_client_update_delay_ns_ = 1000000000./maxClientUpdateFrequency_;
}

DataReadyHandlerStreaming::~DataReadyHandlerStreaming()
{
    connected_circular_buffer_managers_.clear();
}

void DataReadyHandlerStreaming::connect(CircularBufferManagerBase* circularBufferManager)
{
    auto iter = connected_circular_buffer_managers_.find(circularBufferManager);
    if(iter != connected_circular_buffer_managers_.end())
        throw RUNTIME_ERROR("Circular Buffer of Signal '" + circularBufferManager->getSignalName() + "' is already connected to this DataReadyHandler.");

    connected_circular_buffer_managers_[circularBufferManager] = AvailableData {0, 0};
    connected_circular_buffer_manager_names_.push_back(circularBufferManager->getSignalName());
}

void DataReadyHandlerStreaming::requestMoveWindow (int64_t                    last_sample_stamp,
                                                   std::size_t                n_new_samples,
                                                   CircularBufferManagerBase* manager)
{
    // Nothing to do
    if(n_new_samples == 0)
        return;

    // update what we know about this buffer
    connected_circular_buffer_managers_[manager].last_sample_stamp_ = last_sample_stamp;
    connected_circular_buffer_managers_[manager].n_new_samples_ = n_new_samples;

    uint64_t now = get_timestamp_nano_utc();

    // Check if it is already time to do a client update
    if( now >= ( last_streaming_update_ + min_client_update_delay_ns_) )
    {
        if(requestMoveStreamingWindow(last_sample_stamp, n_new_samples))
        {
            // "Move Window" was triggered, so we update the stamp
            last_streaming_update_ = now;
        }
    }
}

bool DataReadyHandlerStreaming::requestMoveStreamingWindow(int64_t     last_sample_stamp,
                                         std::size_t n_new_samples)
{
    //std::cout << " requestMoveWindow n_new_samples:" << n_new_samples << " - sync_move_timestamp: " << sync_move_timestamp << " manager: "<< manager->getSignalName() << std::endl;

    std::lock_guard<std::mutex> lock(move_data_window_mutex_);

    std::size_t n_samples = n_new_samples;
    std::size_t matching_buffers = 0;

    for(const auto&  manager : connected_circular_buffer_managers_)
    {
        if(manager.second.last_sample_stamp_ >= last_sample_stamp && manager.second.n_new_samples_ >= n_new_samples)
        {
            matching_buffers++;

            // Use minimum number of available samples to make sure all windows to move will have the same number of samples
            n_samples = std::min(n_samples, manager.second.n_new_samples_);
        }
    }

    // When he have a move request of each connected buffer, we can move them all
    if(matching_buffers == connected_circular_buffer_managers_.size())
    {
        moveWindows(last_sample_stamp, n_samples);
        return true;
    }

    return false;
}

bool DataReadyHandlerStreaming::hasCircularBuffer(std::string circularBufferName,
                                         ClientNotificationType clientNotificationType,
                                         MaxClientUpdateFrequencyType maxClientUpdateFrequency)
{
    if(clientNotificationType != clientNotificationType_)
        return false;

    if(maxClientUpdateFrequency != maxClientUpdateFrequency_)
        return false;

    for(const auto&  manager : connected_circular_buffer_managers_)
    {
        if(manager.first->getSignalName() == circularBufferName)
        {
            return true;
        }
    }

    return false;
}

bool DataReadyHandlerStreaming::moveWindows(int64_t last_sample_stamp, std::size_t n_new_samples)
{
    std::vector<CircularWindowIterator*> windows;
    for(auto& manager : connected_circular_buffer_managers_)
    {
        // Update the internal info regarding available samples on that buffer
        manager.second.n_new_samples_ -= n_new_samples;

        if(clientNotificationType_ & ClientNotificationType::STREAMING && n_new_samples > 0)
        {
            CircularWindowIterator* window = manager.first->updateStreamingWindowIter(maxClientUpdateFrequency_,
                                                                                      last_sample_stamp,
                                                                                      n_new_samples);
            if(window != NULL)
            {
                windows.push_back(window);
                if(manager.first->is_log_enabled_for(ClientNotificationType::STREAMING, maxClientUpdateFrequency_, LogLevel::TRACE))
                {
                    std::ostringstream stream;
                    stream << *window;
                    manager.first->log_trace_if (ClientNotificationType::STREAMING, maxClientUpdateFrequency_, "Found a new window:\n " + stream.str());
                }
            }
            else
            {
                std::ostringstream message;
                message << "Failed to move streaming window for signal " <<  manager.first->getSignalName();
                message << *this;
                LOG_ERROR(message.str());
            }
        }
    }

    if(windows.size() == connected_circular_buffer_managers_.size())
    {

        int64_t ref_meta_stamp = 0;
        bool ref_metas_do_match = true;
        std::shared_ptr<const TimingContext> timing_context;

        // Set refMeta for all windows
        for(auto window: windows)
        {
            window->updateMetaWindowAccordingToSampleWindow(true);
            if(window->getRefMeta() && window->getRefMeta()->hasTimingContext())
            {
                timing_context =  window->getRefMeta()->getTimingContext();
                if(ref_meta_stamp == 0)
                    ref_meta_stamp = timing_context->getTimeStamp();
                if(ref_meta_stamp != timing_context->getTimeStamp())
                    ref_metas_do_match = false;
            }
        }

        // Default to "no ref trigger" if ref triggers differ between windows
        // TODO: Attempt to use an older ref-trigger
        if(!ref_metas_do_match)
        {
            for(auto window: windows)
                window->resetRefMeta();
            timing_context = std::shared_ptr<const TimingContext>(nullptr);
        }

        //std::cout << "moved_all_windows !!" << std::endl;
        //TODO: Conditional logging required for performance reasons (or just use syslog /zlog )
        //LOG_TRACE("All windows moved. Number of windows moved: " + std::to_string(connected_circular_buffer_managers_.size()));
//            if(clientNotificationType_ & ClientNotificationType::STREAMING && sample_rate_hz_ == 10000. && maxClientUpdateFrequency_ == 1)
//                printf("DataReadyHandler::requestMoveWindow notify data ready - count: %i \n", connected_circular_buffer_managers_.size());

        // Add a new notification payload to the queue
        uint32_t id = dataReadyQueue_->push(connected_circular_buffer_manager_names_,
                                            timing_context,
                                            clientNotificationType_,
                                            maxClientUpdateFrequency_);

        // Notify clients that there is new data on the queue
        cbAllDataReady_(id);

        return true;
    }

    return false;
}

std::ostream& operator<<(std::ostream& os, const DataReadyHandlerStreaming& handler)
{
    os << "#################### DataReadyHandler ####################" << std::endl;
    os << "ClientNotificationType: STREAMING maxClientUpdateFrequency_: " << handler.maxClientUpdateFrequency_ << " sample_rate_hz_: " << handler.sample_rate_hz_ <<  " notification group: " << handler.notification_group_ <<std::endl;
    os << "last_streaming_update_: " << handler.last_streaming_update_ << " - Connected circular buffer managers: ";
    for(auto manager: handler.connected_circular_buffer_managers_)
    {
        os << manager.first->getSignalName() << " - last_sample_stamp_: " << manager.second.last_sample_stamp_ << " n_pending_samples: " << manager.second.n_new_samples_ << std::endl;
    }
    os << "##########################################################" << std::endl;
    return os;
}

// ########################################################################
// ############ DataReadyHandlerTriggered #################################
// ########################################################################

DataReadyHandlerTriggered::DataReadyHandlerTriggered(const std::string& notification_group,
                                                     ClientNotificationType clientNotificationType,
                                                     EventNumber event_number,
                                                     float sample_rate_hz,
                                                     DataReadyQueue* dataReadyQueue,
                                                     AllDataReadyFunction cb_allDataReady):
                                                        DataReadyHandler(notification_group,
                                                                         clientNotificationType,
                                                                         sample_rate_hz,
                                                                         dataReadyQueue,
                                                                         cb_allDataReady),
                                                        event_number_(event_number)

{

}

DataReadyHandlerTriggered::~DataReadyHandlerTriggered()
{
    connected_circular_buffer_managers_.clear();
}

void DataReadyHandlerTriggered::connect(CircularBufferManagerBase* manager)
{
    auto iter = connected_circular_buffer_managers_.find(manager);
    if(iter != connected_circular_buffer_managers_.end())
        throw RUNTIME_ERROR("Circular Buffer of Signal '" + manager->getSignalName() + "' is already connected to this DataReadyHandler.");

    connected_circular_buffer_managers_[manager] = AvailableData {0};
    connected_circular_buffer_manager_names_.push_back(manager->getSignalName());
}

void DataReadyHandlerTriggered::requestMoveWindow(int64_t trigger_stamp,
                                                  CircularBufferManagerBase* manager)
{
    if(manager->is_log_enabled_for(clientNotificationType_, LogLevel::TRACE))
        manager->log_trace_if (clientNotificationType_, "requesting window for trigger_stamp: " + std::to_string(trigger_stamp));

    connected_circular_buffer_managers_[manager].last_trigger_stamp_ = trigger_stamp;

    std::lock_guard<std::mutex> lock(move_data_window_mutex_);

    std::size_t matching_buffers = 0;

    for(const auto&  manager : connected_circular_buffer_managers_)
    {
        // Comparison with '>=' instead of '==' in case one buffer already received the trigger twice
        if(manager.second.last_trigger_stamp_ >= trigger_stamp)
        {
            matching_buffers++;
            // When he have a move request of each connected buffer, we can move them all
            if(matching_buffers == connected_circular_buffer_managers_.size())
            {
                moveWindows(trigger_stamp);
                return;
            }
        }
    }
}

void DataReadyHandlerTriggered::moveWindows(int64_t trigger_stamp)
{
    // It might happen that one data-push added several trigger-events. So we loop until all of them got processed
    while (true)
    {
        std::vector<CircularWindowIterator*> windows;
        for(auto& manager : connected_circular_buffer_managers_)
        {
            if(clientNotificationType_ & ClientNotificationType::FULL_SEQUENCE)
            {
                CircularWindowIterator* window = manager.first->updateFullSequenceWindowIter(trigger_stamp);
                if(window != NULL)
                {
                    windows.push_back(window);
                    if(manager.first->is_log_enabled_for(clientNotificationType_, LogLevel::TRACE))
                    {
                        std::ostringstream stream;
                        stream << *window;
                        manager.first->log_trace_if (clientNotificationType_, "Found a new window:\n " + stream.str());
                    }
                }
            }
            else // TRIGGERED
            {
                CircularWindowIterator* window = manager.first->updateTriggerWindowIter(trigger_stamp, event_number_);
                if(window != NULL)
                {
                    windows.push_back(window);
                    if(manager.first->is_log_enabled_for(clientNotificationType_, LogLevel::TRACE))
                    {
                        std::ostringstream stream;
                        stream << *window;
                        manager.first->log_trace_if (clientNotificationType_, "Found a new window:\n " + stream.str());
                    }
                }
            }
        }

        // If no window needs to be moved, we are finished
        if(windows.size() == 0)
            return;

        if(windows.size() < connected_circular_buffer_managers_.size())
        {
            std::ostringstream message;
            message << "Errror, failed to move some channel windows while others were moved: " << std::endl;
            message << *this;
            LOG_ERROR(message.str());
            return; // no notification for corrupt windows
        }

        int64_t ref_meta_stamp = 0;
        bool triggers_do_match = true;
        std::shared_ptr<const TimingContext> ref_meta_context;

        // compare refMeta of all windows
        for(auto window: windows)
        {
            if(window->getRefMeta() && window->getRefMeta()->hasTimingContext())
            {
                ref_meta_context =  window->getRefMeta()->getTimingContext();
                if(ref_meta_stamp == 0)
                    ref_meta_stamp = ref_meta_context->getTimeStamp();
                if(ref_meta_stamp != ref_meta_context->getTimeStamp())
                    triggers_do_match = false;
            }
        }

        std::shared_ptr<const TimingContext> seq_start_context;
        if(clientNotificationType_ & ClientNotificationType::FULL_SEQUENCE)
        {
            int64_t seq_start_stamp = 0;

            // set/compare timing_context of all windows
            for(auto window: windows)
            {
                // note that the event-stamps will almost never be exactly on a sample. Only in that case, it may happen that we count samples twice (which anyhow is not a problem)
                // Use meta_start_ (aka the START_SEQ trigger) as notification context
                if(window->meta_start_)
                {
                    if(!(window->valid()))
                    {
                        std::ostringstream message;
                        message << "Error: SEQ_START got invalid meanwhile. Possibly the length of the seq-window exceeds the available sample buffer length. Window:" << std::endl;
                        message << *this;

                        std::cout << "meta window:" << window << std::endl;
                        LOG_ERROR(message.str());
                        //return; // No notification if window is invalid
                    }

                    seq_start_context = window->meta_start_->getTimingContext();
                }
                else
                {
                    std::ostringstream message;
                    message << "Logical Error: No start_sequence context found for FULL_SEQ window: " << std::endl;
                    message << *this;
                    LOG_ERROR(message.str());
                    triggers_do_match = false;
                    break;
                }

                // This possibly will overwrite the 'seq_start_context' on th first sample (e.g. in DISJUNCT_SINGLE_SAMPLES)
                //  So it's only done after we got seq_start_context
                window->updateMetaWindowAccordingToSampleWindow(true);

                if(seq_start_stamp == 0)
                    seq_start_stamp = seq_start_context->getTimeStamp();
                if(seq_start_stamp != seq_start_context->getTimeStamp())
                {
                    std::ostringstream message;
                    message << "Errror, Mismatch between seq-start events of FULL_SEQ windows: " << std::endl;
                    message << *this;
                    LOG_ERROR(message.str());
                    triggers_do_match = false;
                    break;
                }
            }
        }

        // Use seq_start context as notification context for FULL_SEQ windows
        std::shared_ptr<const TimingContext> timing_context;
        if(clientNotificationType_ & ClientNotificationType::FULL_SEQUENCE)
            timing_context = seq_start_context;
        else // TRIGGERED
            timing_context = ref_meta_context;

        // Default to "no ref trigger"/ empty notification context if ref triggers differ between windows
        if(!triggers_do_match)
        {
            for(auto window: windows)
                window->resetRefMeta();
            timing_context = std::shared_ptr<const TimingContext>(nullptr);
        }

        //std::cout << "moved_all_windows !!" << std::endl;
        //TODO: Conditional logging required for performance reasons (or just use syslog /zlog )
        //LOG_TRACE("All windows moved. Number of windows moved: " + std::to_string(connected_circular_buffer_managers_.size()));

        // Add a new notification payload to the queue
        uint32_t id = dataReadyQueue_->push(connected_circular_buffer_manager_names_,
                                            timing_context,
                                            clientNotificationType_,
                                            0);

        // Notify clients that there is new data on the queue
        cbAllDataReady_(id);

        return;
    }
}

bool DataReadyHandlerTriggered::hasCircularBuffer(std::string circularBufferName,
                       ClientNotificationType clientNotificationType)
{
    if(clientNotificationType != clientNotificationType_)
        return false;

    for(const auto&  manager : connected_circular_buffer_managers_)
    {
        if(manager.first->getSignalName() == circularBufferName)
        {
            return true;
        }
    }

    return false;
}

std::ostream& operator<<(std::ostream& os, const DataReadyHandlerTriggered& handler)
{
    os << "#################### DataReadyHandler ####################" << std::endl;
    if(handler.clientNotificationType_ & ClientNotificationType::FULL_SEQUENCE)
        os << " ClientNotificationType : FULL_SEQUENCE";
    else
        os << " ClientNotificationType : TRIGGERED";
    os << " event_number_: " << handler.event_number_ << " sample rate: " << handler.sample_rate_hz_  <<  " notification group: " << handler.notification_group_ <<  std::endl;
    os << "Connected circular buffer managers: " << std::endl;
    for(auto manager: handler.connected_circular_buffer_managers_)
    {
        os << manager.first->getSignalName() << " - last_trigger_stamp_: " << manager.second.last_trigger_stamp_ << std::endl;
    }
    os << "##########################################################" << std::endl;
    return os;
}
} /* namespace cabad */
