/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <cabad/DeviceDataBufferBase.h>
#include <cabad/Definitions.h>
#include <cabad/ContextTracker.h>

#include <cassert>
#include <sstream>

namespace cabad
{

std::string DeviceDataBufferBase::extractEventName(const std::string& fesaConcreteEventName) const
{
    std::size_t length = 0;
    std::size_t end = fesaConcreteEventName.find("#");
    if( end == std::string::npos || end == 0 )
    {
        throw RUNTIME_ERROR("Unknown event format for event: " + fesaConcreteEventName);
    }

    // SchedulingUnitName:: prefix only comes from FESA, so we cut it out optionally
    std::size_t begin = fesaConcreteEventName.find("::");
    if( begin == std::string::npos)
    {
        begin = 0;
        length = end - begin;
        return fesaConcreteEventName.substr (begin, length);
    }
    else
    {
        length = end - begin - 2;
        return fesaConcreteEventName.substr (begin + 2, length);
    }
}

unsigned int DeviceDataBufferBase::extractEventID(const std::string& fesaConcreteEventName) const
{
    std::size_t separator = fesaConcreteEventName.find("#");
    if( separator == std::string::npos || separator == 0 )
    {
        throw RUNTIME_ERROR("Unknown event format for event: " + fesaConcreteEventName);
    }

    std::string eventIDString = fesaConcreteEventName.substr(separator + 1, std::string::npos  );
    std::istringstream iss(eventIDString);
    uint32_t eventID;
    iss >> eventID;
    return eventID;
}

void DeviceDataBufferBase::validateRefMetaTriggerEvent(std::string fesaConcreteEventName) const
{
    EventNumber eventID = extractEventID(fesaConcreteEventName);
    std::string eventName = extractEventName(fesaConcreteEventName);
    if(triggerEvents_.find(eventID) == triggerEvents_.end())
        throw RUNTIME_ERROR("The event '" + fesaConcreteEventName + "' cannot be found in the hw-event-list of this device");
    if(triggerEvents_.at(eventID).name != eventName)
        throw RUNTIME_ERROR("The event '" + fesaConcreteEventName + "' cannot be found in the hw-event-list of this device. (The event ID was found)");
}

DeviceDataBufferBase::DeviceDataBufferBase(const std::vector<std::string>& fesaConcreteEventNames, const std::string& fesaDeviceName) :
        fesaDeviceName_(fesaDeviceName)
{
    for (const auto &fesaConcreteEventName : fesaConcreteEventNames)
    {
        EventNumber eventID = extractEventID(fesaConcreteEventName);
        triggerEvents_[eventID] = {extractEventName(fesaConcreteEventName), eventID, true };
        //triggerEvents_[eventID] = extractEventName(fesaConcreteEventName);
    }
}

void DeviceDataBufferBase::setRefMetaTriggerEvents(const std::string& RefMetaTriggerEvent,
                                                   const std::string& RefMetaTriggerEvent_fallback1,
                                                   const std::string& RefMetaTriggerEvent_fallback2)
{
    lastRefMetaTrigger_ = std::shared_ptr<SampleMetadata>(nullptr);
    if(!RefMetaTriggerEvent.empty())
    {
        validateRefMetaTriggerEvent(RefMetaTriggerEvent);
        RefMetaTriggerEventID_ = extractEventID(RefMetaTriggerEvent);
    }
    if(!RefMetaTriggerEvent_fallback1.empty())
    {
        validateRefMetaTriggerEvent(RefMetaTriggerEvent_fallback1);
        RefMetaTriggerEventID_fallback1_ = extractEventID(RefMetaTriggerEvent_fallback1);
    }
    if(!RefMetaTriggerEvent_fallback2.empty())
    {
        validateRefMetaTriggerEvent(RefMetaTriggerEvent_fallback2);
        RefMetaTriggerEventID_fallback2_ = extractEventID(RefMetaTriggerEvent_fallback2);
    }
}

std::string DeviceDataBufferBase::eventID2eventName(EventNumber eventID) const
{
    auto mapIter = triggerEvents_.find(eventID);
    if (mapIter == triggerEvents_.end())
        throw RUNTIME_ERROR("Unknown triggerNumber: " + std::to_string(eventID));
    return mapIter->second.name;
}

unsigned int DeviceDataBufferBase::eventName2eventID(const std::string& eventName) const
{
    for ( auto triggerEvent : triggerEvents_)
    {
        if( triggerEvent.second.name == eventName)
            return triggerEvent.first;
    }
    throw RUNTIME_ERROR("Unknown triggerEventName: " + eventName);
}

bool DeviceDataBufferBase::isTriggerEvent(const std::string& eventName) const
{
    for ( auto triggerEvent : triggerEvents_)
    {
        if( triggerEvent.second.name == eventName)
            return true;
    }
    return false;
}

bool DeviceDataBufferBase::isTriggerEvent(EventNumber eventID) const
{
    auto mapIter = triggerEvents_.find(eventID);
    if (mapIter == triggerEvents_.end())
        return false;
    return true;
}

bool DeviceDataBufferBase::isTriggerEventEnabled(EventNumber eventID) const
{
    auto mapIter = triggerEvents_.find(eventID);
    if (mapIter == triggerEvents_.end())
        throw RUNTIME_ERROR("Unknown triggerEventID: " + std::to_string(eventID));
    return mapIter->second.enabled;
}

void DeviceDataBufferBase::setTriggerEventEnableState(EventNumber eventID, bool enable_state)
{
    auto mapIter = triggerEvents_.find(eventID);
    if (mapIter == triggerEvents_.end())
        throw RUNTIME_ERROR("Unknown triggerEventID: " + std::to_string(eventID));
    mapIter->second.enabled = enable_state;
}

bool DeviceDataBufferBase::isSequenceStartEvent(EventNumber eventID) const
{
    if( eventID == EVENT_NO_SEQ_START )
        return true;
    return false;
}

bool DeviceDataBufferBase::isSequenceEndEvent(EventNumber eventID) const
{
    // GSI/FAIR timing system does not play a SEQ_END events so far
    // So we use GAP_START as sequence end
    if( eventID == EVENT_NO_GAP_START )
        return true;
    return false;
}

bool DeviceDataBufferBase::isRefMetaTriggerEvent(EventNumber eventID) const
{
    // We dont have any reference meta? Aceppt all suppored ID's
    if(!lastRefMetaTrigger_)
    {
        if( eventID == RefMetaTriggerEventID_ || eventID == RefMetaTriggerEventID_fallback1_ || eventID == RefMetaTriggerEventID_fallback2_ )
            return true;
        return false;
    }

    // Only use Fallback events if the correct event was never seen
    if(eventID == RefMetaTriggerEventID_)
        return true;
    else if(eventID == RefMetaTriggerEventID_fallback1_)
    {
        if(lastRefMetaTrigger_->getTimingContext()->getEventNumber() == RefMetaTriggerEventID_fallback1_) // only fallback1 so far
            return true;
        if(lastRefMetaTrigger_->getTimingContext()->getEventNumber() == RefMetaTriggerEventID_fallback2_) // only fallback2 so far
            return true;
    }
    else if(eventID == RefMetaTriggerEventID_fallback2_)
    {
        if(lastRefMetaTrigger_->getTimingContext()->getEventNumber() == RefMetaTriggerEventID_fallback2_) // only fallback2 so far
            return true;
    }
    return false;
}

bool DeviceDataBufferBase::isRefMetaTrigger(const std::shared_ptr<const SampleMetadata> meta) const
{
    // Not supposed to happen during runtime (only relevant for unit-tests)
    assert(meta->getRelatedSample() && "Logical error: Metadata has no related sample");

    if(!meta->hasTimingContext())
        return false;

    return isRefMetaTriggerEvent(meta->getTimingContext()->getEventNumber());
}


void  DeviceDataBufferBase::reportEventMeta(std::shared_ptr<SampleMetadata>& meta)
{
    // Now update it, if required
    if(isRefMetaTrigger(meta))
        lastRefMetaTrigger_ = meta;
}

DeviceDataBufferBase::TriggerEventsByEventIDType DeviceDataBufferBase::getTriggerEvents() const
{
    return triggerEvents_;
}

} // close namespace
