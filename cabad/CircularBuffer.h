/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/CircularBufferBase.h>
#include <cabad/Definitions.h>

#include <cstring>
#include <iostream>
#include <mutex>
#include <cassert>

namespace cabad
{

/* class to describe a circular buffer for a single type */
template <typename T>
class CircularBuffer : public CircularBufferBase
{
public:

    CircularBuffer(std::size_t size) : CircularBufferBase(size)
    {
        data_ = new T[size_];

        begin_ = concrete_iterator(data_,size_,0);
        end_ = concrete_iterator(data_,size_,size_);

        // Cannot be done in CTor of CircularBufferBase, because new_iterator is pure virtual
        write_iterator_.reset(new_iterator(0));
        previous_write_iterator_.reset(new_iterator(0));
    }

    ~CircularBuffer()
    {
        delete[] data_;
    }

    class concrete_iterator : public CircularBufferBase::iterator
    {
        public:
            concrete_iterator() : CircularBufferBase::iterator(0,0), ptr_(NULL), data_(NULL){}
            concrete_iterator(T* data, std::size_t size,std::size_t index = 0):
            	CircularBufferBase::iterator(size,index),
				ptr_(data), data_(data)
            {
                setDataPointerToIndex();
            }
            T& operator*() { return *ptr_; }
            T* operator->() {return ptr_; }

            bool equalsOther(const CircularBufferBase::iterator& other) const override
            {
                // TODO: Check for cast errors
                const concrete_iterator* otheriter = dynamic_cast<const concrete_iterator*>(&other);
                return (this->ptr_ == otheriter->ptr_);
            }

            void setDataPointerToIndex() override
            {
                if (index_ >= circularSize_)
                    ptr_ = NULL;
                else
                    ptr_ = data_ + index_;
            }

            concrete_iterator& operator= (const concrete_iterator& other)
            {
                ptr_ = other.ptr_;
                data_ = other.data_;
                CircularBufferBase::iterator::operator=(other);
                return *this;
            }

            void copyDataPointerFrom (const CircularBufferBase::iterator& other) override
            {
                // TODO: Check for cast errors
                const concrete_iterator* otherIter = dynamic_cast<const concrete_iterator*>(&other);
                ptr_ = otherIter->ptr_;
                data_ = otherIter->data_;
            }

            T* ptr_;
            T* data_;
    };

    T& operator[](std::size_t index)
    {
        assert(index < size_);
        return data_[index];
    }

    const T& operator[](std::size_t index) const
    {
        assert(index < size_);
        return data_[index];
    }

    CircularBufferBase::iterator* new_iterator(std::size_t index) override
    {
      return new concrete_iterator(data_,size_, index);
    }

    const concrete_iterator& begin() const
    {
        return begin_;
    }

    const concrete_iterator& end() const
    {
        return end_;
    }

    void push(const T* data, std::size_t data_size)
    {
        if(data_size > size_)
            throw RUNTIME_ERROR("Error: Cannot push chunks which exceed the size of the circular buffer.");

        std::lock_guard<std::mutex> lock(write_iterator_mutex_);

        // Copy the data twice if end of the rolling buffer is reached
        std::size_t remainingBufferSize =  write_iterator_->distanceTillEnd() + 1; // +1 to including write_iterator_ itself
        if( remainingBufferSize < data_size ) // copy twice
        {
            std::size_t rolloverSize = data_size - remainingBufferSize;
            memcpy(&data_[write_iterator_->index()], data, remainingBufferSize * sizeof(T));
            memcpy(&data_[0], &data[remainingBufferSize], rolloverSize * sizeof(T));
        }
        else // single copy
        {
            memcpy(&data_[write_iterator_->index()], data, data_size * sizeof(T));
        }

        *previous_write_iterator_ = *write_iterator_;
        (*write_iterator_)+= data_size;
        n_data_pushed_total_ += data_size;
    }

    void push(std::vector<T>& data)
    {
      // TODO
      std::cout << "not implemented yet" << std::endl;
      exit(1);
    }

    void push(T& data)
    {
        std::lock_guard<std::mutex> lock(write_iterator_mutex_);
        data_[write_iterator_->index()] = data;
        *previous_write_iterator_ = *write_iterator_;
        (*write_iterator_)+= 1;
        n_data_pushed_total_ += 1;
    }

    // copies data from buffer into array
    bool copyDataWindow(iterator& start, iterator& end,T* buffer, const std::size_t& buffer_size, std::size_t& data_written)
    {
        data_written = start.distance_to(end) + 1; // +1 to read the end itself as well
        if(data_written > buffer_size)
        {
            std::string message;
            message = "Error: Write aborted. data to write ("+ std::to_string(data_written) +" elements) would exceed available buffer size (" + std::to_string(buffer_size) + " elements)";
            data_written = 0;
            throw RUNTIME_ERROR(message);
        }
        if( start.index() <= end.index())
        {
            memcpy(&buffer[0], &data_[start.index()], data_written * sizeof(T));
        }
        else
        {
            std::size_t size_remaining = start.distanceTillEnd() + 1; // +1 to including data_start_ itself
            std::size_t size_rollover = data_written - size_remaining;
            memcpy(&buffer[0], &data_[start.index()], size_remaining * sizeof(T));
            memcpy(&buffer[size_remaining], &data_[0], size_rollover * sizeof(T));
        }

        // For the sake of backward compatibility
        return true;
    }

private:

    T* data_;

    concrete_iterator begin_;
    concrete_iterator end_;
};

/* class to describe a circular buffer for two types. This keeps support for menmcopy (In contrast of using a struct with the basic CircularBuffer class ) */
template <typename T1, typename T2>
class CircularBufferDual : public CircularBufferBase
{
public:

    CircularBufferDual(std::size_t size) : CircularBufferBase(size)
    {
        data1_ = new T1[size_];
        data2_ = new T2[size_];

        begin_ = concrete_iterator(data1_,data2_,size_,0);
        end_ = concrete_iterator(data1_,data2_,size_,size_);

        // Cannot be done in CTor of CircularBufferBase, because new_iterator is pure virtual
        write_iterator_.reset(new_iterator(0));
        previous_write_iterator_.reset(new_iterator(0));
    }

    ~CircularBufferDual()
    {
        delete[] data1_;
        delete[] data2_;
    }

    class concrete_iterator : public CircularBufferBase::iterator
    {
        public:
            concrete_iterator() :
                CircularBufferBase::iterator(0,0),
				ptr1_(NULL), ptr2_(NULL), data1_(NULL), data2_(NULL){}
            concrete_iterator(T1* data1, T2* data2, std::size_t size,std::size_t index = 0):
                CircularBufferBase::iterator(size,index),
				ptr1_(data1), ptr2_(data2), data1_(data1), data2_(data2)
            {
                setDataPointerToIndex();
            }

            T1& ref_type1() {return *ptr1_;}
            T2& ref_type2() {return *ptr2_;}
            T1* p_type1() {return ptr1_;}
            T2* p_type2() {return ptr2_;}

            bool equalsOther(const CircularBufferBase::iterator& other) const override
            {
                // TODO: Check for cast errors
                const concrete_iterator* otherIter = dynamic_cast<const concrete_iterator*>(&other);
                return (ptr1_ == otherIter->ptr1_ && ptr2_ == otherIter->ptr2_);
            }

            void setDataPointerToIndex() override
            {
                if (index_ >= circularSize_)
                {
                    ptr1_ = NULL;
                    ptr2_ = NULL;
                }
                else
                {
                    ptr1_ = data1_ + index_;
                    ptr2_ = data2_ + index_;
                }
            }

            concrete_iterator& operator= (const concrete_iterator& other)
            {
                ptr1_ = other.ptr1_;
                ptr2_ = other.ptr2_;
                data1_ = other.data1_;
                data2_ = other.data2_;
                CircularBufferBase::iterator::operator=(other);
                return *this;
            }

            void copyDataPointerFrom (const CircularBufferBase::iterator& other) override
            {
                // TODO: Check for cast errors
                const concrete_iterator* otherIter = dynamic_cast<const concrete_iterator*>(&other);
                ptr1_ = otherIter->ptr1_;
                ptr2_ = otherIter->ptr2_;
                data1_ = otherIter->data1_;
                data2_ = otherIter->data2_;
            }

        private:
            T1* ptr1_;
            T2* ptr2_;
            T1* data1_;
            T2* data2_;
    };

    CircularBufferBase::iterator* new_iterator(std::size_t index) override
    {
      return new concrete_iterator(data1_, data2_, size_, index);
    }

    const concrete_iterator& begin() const
    {
        return begin_;
    }
    
    const concrete_iterator& end() const
    {
        return end_;
    }

    T1& get_type1_ref(std::size_t index)
    {
        assert(index < size_);
        return data1_[index];
    }

    T2& get_type2_ref(std::size_t index)
    {
        assert(index < size_);
        return data2_[index];
    }

    void push(const T1* data1, const T2* data2, std::size_t data_size)
    {
        if(data_size > size_)
            throw RUNTIME_ERROR("Error: Cannot push chunks which exceed the size of the circular buffer.");

        std::lock_guard<std::mutex> lock(write_iterator_mutex_);

        // Copy the data twice if end of the rolling buffer is reached
        std::size_t remainingBufferSize =  write_iterator_->distanceTillEnd() + 1; // +1 to including write_iterator_ itself
        if( remainingBufferSize < data_size ) // copy twice
        {
            std::size_t rolloverSize = data_size - remainingBufferSize;
            memcpy(&data1_[write_iterator_->index()], data1, remainingBufferSize * sizeof(T1));
            memcpy(&data2_[write_iterator_->index()], data2, remainingBufferSize * sizeof(T2));

            memcpy(&data1_[0], &data1[remainingBufferSize], rolloverSize * sizeof(T1));
            memcpy(&data2_[0], &data2[remainingBufferSize], rolloverSize * sizeof(T2));
        }
        else // single copy
        {
            memcpy(&data1_[write_iterator_->index()], data1, data_size * sizeof(T1));
            memcpy(&data2_[write_iterator_->index()], data2, data_size * sizeof(T2));
        }

        *previous_write_iterator_ = *write_iterator_;
        (*write_iterator_)+= data_size;
        n_data_pushed_total_ += data_size;
    }

    void push(std::vector<T1>& data1, std::vector<T2>& data2)
    {
      // TODO
      std::cout << "not implemented yet" << std::endl;
      exit(1);
    }

    void push(T1& data1, T2& data2)
    {
        std::lock_guard<std::mutex> lock(write_iterator_mutex_);
        data1_[write_iterator_->index()] = data1;
        data2_[write_iterator_->index()] = data2;
        *previous_write_iterator_ = *write_iterator_;
        (*write_iterator_)+= 1;
        n_data_pushed_total_ += 1;
    }

    // copies data from buffer into array
    bool copyDataWindow(iterator& start, iterator& end,T1* buffer1, T2* buffer2, const std::size_t& buffer_size, std::size_t& data_written)
    {
        data_written = start.distance_to(end) + 1; // +1 to read the end itself as well
        if(data_written > buffer_size)
        {
            std::string message;
            message = "Error: Write aborted. data to write ("+ std::to_string(data_written) +" elements) would exceed available buffer size (" + std::to_string(buffer_size) + " elements)";
            data_written = 0;
            throw RUNTIME_ERROR(message);
        }
        if( start.index() <= end.index())
        {
            memcpy(&buffer1[0], &data1_[start.index()], data_written * sizeof(T1));
            memcpy(&buffer2[0], &data2_[start.index()], data_written * sizeof(T2));
        }
        else
        {
            std::size_t size_remaining = start.distanceTillEnd() + 1; // +1 to including data_start_ itself
            std::size_t size_rollover = data_written - size_remaining;
            memcpy(&buffer1[0], &data1_[start.index()], size_remaining * sizeof(T1));
            memcpy(&buffer2[0], &data2_[start.index()], size_remaining * sizeof(T2));
            memcpy(&buffer1[size_remaining], &data1_[0], size_rollover * sizeof(T1));
            memcpy(&buffer2[size_remaining], &data2_[0], size_rollover * sizeof(T2));
        }

        // For the sake of backward compatibility
        return true;
    }

private:

    T1* data1_;
    T2* data2_;

    concrete_iterator begin_;
    concrete_iterator end_;
};

} /* namespace cabad */
