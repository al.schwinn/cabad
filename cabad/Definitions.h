/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <string>
#include <exception>

namespace cabad
{

typedef uint32_t EventNumber;

#define RUNTIME_ERROR(x) std::runtime_error(__FILE__  + std::string(":") + std::to_string(__LINE__) + " Error: " + x )

#define DEBUG_PRINT_LINE_INFO std::cout << __FILE__ << " - " << __func__ << " - " << __LINE__ << std::endl;

// Nasty hack in order to pass some information to the has-data-changed method
#define NOTIFICATION_IDENTIFIER_PREFIX  "FAIR.SELECTOR.C="

#define STRING_NO_REF_META_FOUND "NO_REF_META"

#define STRING_NO_REF_TRIGGER_FOUND "NO_REF_META"

enum class DataPushMode
{
    // Each push will only push a single sample.
    // These single samples dont have a fixed distances from each other (there is no "sample rate")
    // Because of that, each sample will require to have its own meta-item and timestamp
    DISJUNCT_SINGLE_SAMPLES  = 0,

    // Each push will only push a single sample.
    // These single samples have a fixed distances from each other, so a fixed "sample rate" can be assumed
    // not required to have a stamp on each sample
    CONTINOUS_SINGLE_SAMPLES = 1,

    // Each push will push one or many samples
    // The samples inside each single push have a fixed distance from each other, the "sample rate"
    // Separate pushes dont have a fixed distance from each other.
    // Each push needs to have at least one timestamped sample, to be able to match the correct Timing event
    DISJUNCT_MULTI_SAMPLES   = 2,

    // Each push will push one or many samples
    // All samples pushed (no matter if pushed at once, or in different pushes) have a fixed distance from each other, the "sample rate"
    // Not required to have a stamp on each push
    CONTINOUS_MULTI_SAMPLES  = 3
};

// FIXME: Not good to have event numbers hardcoded
const EventNumber EVENT_NO_BEAM_BP_START   = 256;
const EventNumber EVENT_NO_SEQ_START       = 257;
const EventNumber EVENT_NO_GAP_START       = 258;
const EventNumber EVENT_NO_BEAM_INJECTION  = 283;
const EventNumber EVENT_NO_BEAM_EXTRACTION = 284;
const EventNumber EVENT_NO_COMMAND         = 255;

typedef uint64_t MaxClientUpdateFrequencyType;

enum class ClientNotificationType
{
    STREAMING     = 1 << 0,
    TRIGGERED     = 1 << 1,
    FULL_SEQUENCE = 1 << 2
};

enum class LogLevel
{
    ERROR   = 5,
    WARNING = 4,
    INFO    = 3,
    TRACE   = 2,
    DEBUG   = 1
};

/* To Print the integer value */
template <typename Enumeration>
auto as_integer(Enumeration const value)
    -> typename std::underlying_type<Enumeration>::type
{
    return static_cast<typename std::underlying_type<Enumeration>::type>(value);
}

/* To check if enum value is set */
bool operator &(ClientNotificationType lhs, ClientNotificationType rhs);

/* To create the bit-enum by gluing multiple bits together */
ClientNotificationType operator |(ClientNotificationType lhs, ClientNotificationType rhs);

} // end namespace
