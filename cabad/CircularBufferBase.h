/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <memory>
#include <vector>
#include <mutex>

namespace cabad
{

/* class to describe a circular buffer and its iterator. The data-type is defined by the concrete implementation

 * The buffer supports multiple-writer and multiple-reader threads
 * Readers dont need to mutex lock the buffer for reading. They will not block the write iterator.
 * Writers will flag registered readers as "invalid", if they write on the same slot. Like that read iterators can drop invalid data.
 * A mutex is used to snychronize multiple writers
 *
 * The requirements to design this circular buffer were:
 * - Possibility to use memcopy on the data (be as fast as possibly)
 * - Use data types which can be directly copied into a cmw-data container (no structures)
 * - Fixed buffer size (no memory allocation during runtime)
 * - read iterators should not block write iterators
 * Why would you want to use this buffer over some other public implementations (e.g. boost circular buffer) ?
 * --> There was no public implementation which fulfilled our requirements. E.g. check https://stackoverflow.com/a/890269/1599887
 * When writing this, there was no circular buffer arround, which solved the "non-blocking write iter" aspect for a fixed buffer size
 */

class CircularBufferBase
{
public:

    class iterator
    {
        public:

            // modification
            iterator& operator= (const iterator& other);
            iterator& operator+= (std::size_t length);
            iterator& operator-= (std::size_t length);
            iterator& operator++ ();
            iterator& operator-- ();
            void reset_to(int32_t index);
            void setToEnd();

            // others
            bool operator==(const iterator& rhs) const;
            bool operator!=(const iterator& rhs) const;
            std::size_t index() const;
            std::size_t prev_index() const;

            // TRUE if the iterator index is between, or directly on start or end
            bool isBetween(iterator& start, iterator& end) const;

            // The result will always be positive.
            // Number of samples to step, until the index of other is found
            std::size_t distance_to(const iterator& other) const;
            std::size_t distanceTillEnd() const;
            std::size_t getCircularSize() const;

            // debugging
            friend std::ostream& operator<<(std::ostream& os, const CircularBufferBase::iterator& iter);

          protected:

            //creation
            iterator();
            iterator(std::size_t size, std::size_t index);

            virtual bool equalsOther(const CircularBufferBase::iterator& other) const = 0;
            virtual void setDataPointerToIndex() = 0;
            virtual void copyDataPointerFrom (const CircularBufferBase::iterator& other) = 0;

            std::size_t circularSize_;
            std::size_t index_;

        private:
            iterator& operator++(int unused); // postfix not supported (prefix anyhow is faster)
            iterator& operator--(int unused); // postfix not supported (prefix anyhow is faster)
    };

    CircularBufferBase(std::size_t size);

    virtual ~CircularBufferBase();


    std::size_t size() const
    {
        return size_;
    }

    std::shared_ptr<CircularBufferBase::iterator> pBegin()
    {
        return getReadIterator(0);
    }

    std::shared_ptr<CircularBufferBase::iterator> pEnd()
    {
        std::shared_ptr<CircularBufferBase::iterator> iter = getReadIterator(size_);
        iter->setToEnd();
        return iter;
    }

    std::size_t getWriteIterIndex() const
    {
        return write_iterator_->index();
    }

    void moveIterToLatest(CircularBufferBase::iterator& iter) const
    {
        iter = *write_iterator_;
        --iter;
    }

    bool isWriteIter(const CircularBufferBase::iterator& iter) const
    {
        return *write_iterator_ == iter;
    }

    /*
     * distance_to_write_iter
     * @iter: Iterator to check the distance
     *
     * Returns the distance from the passed iterator to the write iterator.
     * E.g. if @iter is on samle 2 and write iter is on sample 5, than 3 will be returned.
     * If both iterators point to the same sample, 0 will be returned.
     */
    std::size_t distance_to_write_iter(const CircularBufferBase::iterator& iter) const
    {
        return iter.distance_to(*write_iterator_);
    }

    /*
     * was_overwritten
     * @iter: Iterator to check
     *
     * Returns True if the passed iterator is located between the old and the new write iterator, which means it was overwritten.
     */
    bool was_overwritten(const CircularBufferBase::iterator& iter) const;

    /*
     * valid_distance_between
     * @iter1: First Iterator to check
     * @iter2: Second Iterator to check
     *
     * Returns the valid distance from the first to the second iterator.
     * The distance is considered 'valid' if there is no write iterator in between.
     * Note that in contrast to "distance_to", this method will return a negative distance, if the positive distance is not valid !
     */
	int32_t valid_distance_between(const CircularBufferBase::iterator& iter1, const CircularBufferBase::iterator& iter2) const;

    /*
     * @index index on the circular bufer for which the read iterator is requested.
     *        If the index is bigger than the size of the buffer, a modula operation will be used to find a valid index
     *        As well a negative index will handled gracefully (E.g. -1 will be the last buffer element)
     *
     * Return: A shared pointer to the requested read iterator
     */
    std::shared_ptr<CircularBufferBase::iterator> getReadIterator(int32_t index = 0);

    std::shared_ptr<CircularBufferBase::iterator> getLatest();

    std::shared_ptr<CircularBufferBase::iterator> getCopyOfWriteIterator();

    std::shared_ptr<CircularBufferBase::iterator> getCopyOfPrevWriteIterator();

    // creates a new iterator, on the next slot and returns it
    std::shared_ptr<CircularBufferBase::iterator> next(const CircularBufferBase::iterator& iter);

    // creates a new iterator, on the previous slot and returns it
    std::shared_ptr<CircularBufferBase::iterator> prev(const CircularBufferBase::iterator& iter);

    uint64_t n_data_pushed_total()
    {
    	return n_data_pushed_total_;
    }

    // for debugging
    friend std::ostream& operator<<(std::ostream& os, const CircularBufferBase& buffer);

protected:

    virtual CircularBufferBase::iterator* new_iterator(std::size_t index) = 0;

    std::size_t size_;

    // Current and previous write iterator
    std::shared_ptr<iterator>write_iterator_;
    std::shared_ptr<iterator>previous_write_iterator_;

    // mutex to protect the write iterators from concurrent access
    std::mutex write_iterator_mutex_;

    // Number of data items pushed to the buffer in total
    uint64_t n_data_pushed_total_;
};

} /* namespace cabad */
