/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <gtest/gtest.h>

#include <test/Util.h>

#include <string>
#include <iostream>

namespace cabad
{

class CircularBufferBaseTestFixture : public ::testing::Test
{

public:

    // Is called once for all tests of this fixture
    static void SetUpTestCase();

};

void CircularBufferBaseTestFixture::SetUpTestCase()
{
    setLogFunction(fake_log_function);
}


} // end namespace
