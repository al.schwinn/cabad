/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <test/DeviceDataBufferBaseFixture.h>
#include <test/TimingContextMock.h>

#include <cabad/DeviceDataBufferBase.h>

namespace cabad
{

TEST_F(DeviceDataBufferBaseFixture, extractEventName)
{
    DeviceDataBufferBase buffer(fesaConcreteEventNames_, fesaDeviceName_);

    ASSERT_NO_THROW(
            std::string eventName1 = buffer.extractEventName(fesaInternalPrefix_ + inject_event_);
            std::string eventName2 = buffer.extractEventName(inject_event_);
            ASSERT_STREQ(eventName1.c_str(), eventName2.c_str());
    );

    ASSERT_NO_THROW(
            std::string eventName = buffer.extractEventName(inject_event_);
            unsigned int id = buffer.eventName2eventID(eventName);
            ASSERT_TRUE(buffer.isTriggerEventEnabled(id));
            buffer.setTriggerEventEnableState(id, false);
            ASSERT_FALSE(buffer.isTriggerEventEnabled(id));
    );

    ASSERT_THROW(
            buffer.extractEventName("missingSeparator");
            ,std::exception);
    ASSERT_THROW(
            buffer.extractEventName("#SeparatorAtTheStart");
            ,std::exception);
}

TEST_F(DeviceDataBufferBaseFixture, extractEventID)
{
    DeviceDataBufferBase buffer(fesaConcreteEventNames_, fesaDeviceName_);

    ASSERT_NO_THROW(
            unsigned int eventID1 = buffer.extractEventID(fesaInternalPrefix_ + inject_event_);
            unsigned int eventID2 = buffer.extractEventID(inject_event_);
            ASSERT_EQ(eventID1, eventID2);
    );

    ASSERT_THROW(
            buffer.extractEventID("missingSeparator");
            ,std::exception);
}

TEST_F(DeviceDataBufferBaseFixture, setRefMetaTriggerEvents)
{

    DeviceDataBufferBase buffer(fesaConcreteEventNames_, fesaDeviceName_);

    ASSERT_NO_THROW(
            buffer.setRefMetaTriggerEvents(inject_event_, seq_start_event_, command_event_);
    );
}

TEST_F(DeviceDataBufferBaseFixture, report_and_check_eventMeta)
{
    DeviceDataBufferBase meta_buffer(fesaConcreteEventNames_, fesaDeviceName_);
    meta_buffer.setRefMetaTriggerEvents(inject_event_, seq_start_event_, command_event_);
    std::size_t circular_buffer_size = 100;
    CircularBuffer<float> circular_buffer(circular_buffer_size);

    std::shared_ptr<const TimingContext> command(new TimingContextMock(1, 255));
    std::shared_ptr<const TimingContext> seq_start(new TimingContextMock(2, 257));
    std::shared_ptr<const TimingContext> inject(new TimingContextMock(3, 283));
    std::shared_ptr<const TimingContext> random(new TimingContextMock(4, 1234));

    std::shared_ptr<CircularBufferBase::iterator> samp1 = circular_buffer.getReadIterator(1);
    std::shared_ptr<CircularBufferBase::iterator> samp2 = circular_buffer.getReadIterator(2);
    std::shared_ptr<CircularBufferBase::iterator> samp3 = circular_buffer.getReadIterator(3);
    std::shared_ptr<CircularBufferBase::iterator> samp4 = circular_buffer.getReadIterator(4);

    std::shared_ptr<SampleMetadata> e_command(new SampleMetadata(samp1, command));
    std::shared_ptr<SampleMetadata> e_seq_start(new SampleMetadata(samp2 ,seq_start));
    std::shared_ptr<SampleMetadata> e_inject(new SampleMetadata(samp3, inject));
    std::shared_ptr<SampleMetadata> e_random(new SampleMetadata(samp4, random));

    std::shared_ptr<SampleMetadata> no_timing(new SampleMetadata(samp4));

    ASSERT_NO_THROW(
            meta_buffer.reportEventMeta(e_random);
            ASSERT_FALSE(meta_buffer.isRefMetaTrigger(e_random)); // not a ref-triger event

            ASSERT_TRUE(meta_buffer.isRefMetaTrigger(e_command)); // No reference trigger reported yet --> every reference trigger is used
            meta_buffer.reportEventMeta(e_command);
            ASSERT_TRUE(meta_buffer.isRefMetaTrigger(e_command)); // Reported and matching
            meta_buffer.reportEventMeta(e_seq_start);
            ASSERT_FALSE(meta_buffer.isRefMetaTrigger(e_command)); // From now on, only seq_start used as ref trigger
            ASSERT_TRUE(meta_buffer.isRefMetaTrigger(e_seq_start));
            meta_buffer.reportEventMeta(e_inject);
            ASSERT_FALSE(meta_buffer.isRefMetaTrigger(e_command)); // From now on, only inject used as ref trigger
            ASSERT_FALSE(meta_buffer.isRefMetaTrigger(e_seq_start));
            ASSERT_TRUE(meta_buffer.isRefMetaTrigger(e_inject));
    );

    // Event without timing context is not a reference trigger
    ASSERT_NO_THROW(
            ASSERT_FALSE(meta_buffer.isRefMetaTrigger(no_timing));
    );
}

} // end namespace
