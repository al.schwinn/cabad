/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <test/CircularBufferManagerMock.h>
#include <test/Util.h>

#include <cabad/DataReadyHandler.h>
#include <cabad/DataReadyQueue.h>
#include <cabad/Util.h>

#include <gtest/gtest.h>

#include <string>
#include <iostream>

namespace cabad
{

class DataReadyHandlerFixture : public ::testing::Test
{

public:

    DataReadyHandlerFixture(): queue_(10), manager1_("Sink1"), manager2_("Sink2")
    {
        dummy_notificationType_ = ClientNotificationType::FULL_SEQUENCE;
        dummy_updateFrequency_ = 10;
        dummy_sample_rate_hz = 100;
        dummy_buffer_managers_.push_back(&manager1_);
        dummy_buffer_managers_.push_back(&manager2_);
        dummy_group_ = "dummy_notification_group";
    }

    // Is called once for all tests of this fixture
    static void SetUpTestCase();

    //called on each test
    void SetUp();
    void TearDown();


    static void callbackAllDataReady (uint32_t data_id)
    {
        data_id_received_from_cb_ = data_id;
    }

    DataReadyQueue queue_;

    std::vector<CircularBufferManagerBase*> dummy_buffer_managers_;
    ClientNotificationType dummy_notificationType_;
    MaxClientUpdateFrequencyType dummy_updateFrequency_;
    float dummy_sample_rate_hz;

    static uint32_t data_id_received_from_cb_;

    CircularBufferManagerMock manager1_;
    CircularBufferManagerMock manager2_;

    std::string dummy_group_;
};

void DataReadyHandlerFixture::SetUpTestCase()
{
    setLogFunction(fake_log_function);
    setTimeStampFunction(fake_clock_get_utc_now);
    fakeClock = START_TIME;
}
void DataReadyHandlerFixture::SetUp()
{
    data_id_received_from_cb_ = 0;
}

void DataReadyHandlerFixture::TearDown()
{

}
} // end namespace
