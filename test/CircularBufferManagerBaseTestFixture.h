/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <gtest/gtest.h>

#include <string>
#include <iostream>

#include <cabad/CircularBufferManagerBase.h>

#include <test/TimingContextMock.h>
#include <test/Util.h>

#define CIRCULAR_BUFFER_TYPE CircularBufferDual<float, float>

namespace cabad
{

class CircularBufferManager: public CircularBufferManagerBase
{

public:

    struct FakeMetaInfo
    {
        // Stamp-Only meta
        FakeMetaInfo(int64_t atrigger_stamp, uint64_t aoffset, int32_t astatus)
        {
            trigger_stamp = atrigger_stamp;
            abs_offset = aoffset;
            status = astatus;
            fesaConcreteEventName = "";
        }

        FakeMetaInfo(int64_t atrigger_stamp, uint64_t aoffset, int32_t astatus, std::string afesaConcreteEventName, std::shared_ptr<const TimingContext> acontext)
        {
            trigger_stamp = atrigger_stamp;
            abs_offset = aoffset;
            status = astatus;
            fesaConcreteEventName = afesaConcreteEventName;
            context = acontext;
        }
        int64_t trigger_stamp;
        uint64_t abs_offset;
        int32_t status;
        std::string fesaConcreteEventName;
        std::shared_ptr<const TimingContext> context;
    };

    CircularBufferManager(std::string signal_name,
                           float sample_rate_hz,
                           std::vector<MaxClientUpdateFrequencyType> maxClientUpdateFrequencies,
                           ContextTracker* contextTracker,
                           DeviceDataBufferBase* deviceDataBuffer,
                           CircularBufferBase* circularBuffer,
                           MetaDataBuffer*   metadataBuffer,
                           ClientNotificationType clientNotificationType,
                           DataPushMode dataPushMode,
                           DataReadyManager* dataReadyManager):
         CircularBufferManagerBase(   signal_name,
                                      sample_rate_hz,
                                      maxClientUpdateFrequencies,
                                      contextTracker,
                                      deviceDataBuffer,
                                      circularBuffer,
                                      metadataBuffer,
                                      clientNotificationType,
                                      dataPushMode,
                                      dataReadyManager,
                                      signal_name) // notification_group --> No grouping
    {
        fakeClock = START_TIME;
    }

    ~CircularBufferManager()
    {

    }

    void push(  const float                 *data,
                std::size_t                  data_size,
                std::vector<FakeMetaInfo>    meta_info);

    void copyDataWindow(CircularWindowIterator* window_iter, float* buffer1, float* buffer2, const std::size_t& buffer_size, std::size_t& n_data_written);

//private:
    // Pointer to FESA event source
    //UpdateTriggeredClientsSrc* updateTriggeredClientsSrc_;
};

class CircularBufferManagerBaseTestFixture : public ::testing::Test
{

public:

    static void cbDataReady(uint32_t data_id)
    {
        data_ready_flagged_ = true;
    }

    CircularBufferManagerBaseTestFixture(): dataReadyManager_(cbDataReady, 100)
    {
    }

    std::shared_ptr<CircularBufferManager> buffer_manager_;
    std::shared_ptr<MetaDataBuffer> metadata_buffer_;
    std::shared_ptr<CircularBufferBase> circular_buffer_;
    std::shared_ptr<ContextTracker> context_tracker_;
    std::shared_ptr<DeviceDataBufferBase> device_data_buffer_;
    DataReadyManager dataReadyManager_;


    static bool data_ready_flagged_;

    int64_t fakeClockCarry_;
    uint64_t fakeSampleOffset_;
    uint64_t fakeSamplesPushed_;
    uint64_t fakeSamplesChecked_;

    std::vector<int64_t> fakeSampleStamps_;

    uint64_t preTriggerSamples_;
    uint64_t postTriggerSamples_;

    std::size_t nTriggersChecked_;

    uint64_t random_measurement_jitter_max_ns_;
    uint64_t random_measurement_jitter_min_ns_;

    // All fake ref trigger stamps with their names
    std::map<int64_t, std::string> fakeRefMetaCol_;

    std::shared_ptr<const TimingContext> prevSeqStartEvent_;
    std::shared_ptr<const TimingContext> currentSeqStartEvent_;

    std::vector<std::string> trigger_events_;
    std::vector<CircularBufferManager::FakeMetaInfo> meta_to_push_;
    std::vector<CircularBufferManager::FakeMetaInfo> meta_pushed_;
    std::vector<CircularBufferManager::FakeMetaInfo>::iterator latestfakeRefMeta_;

    double sampRate_; //Hz
    float nSamplesTillPush_; //number of new samples after which the data is pushed

    std::size_t nSamplesInPreviousSequence_;
    std::size_t nSamplesIncurrentSequence_;

    void createCircularBufferManager (float sampleRate,
            std::size_t size_buffer,
            std::size_t meta_buffer_size,
            std::size_t contextTrackerSize,
            ClientNotificationType clientNotificationType,
            DataPushMode dataPushMode);

    void wait(int milliseconds);
    void waitWithPush(int milliseconds);
    void injectFakeEvent(int64_t timestamp, std::string fesaConcreteEventName, uint32_t sequenceIndex, bool addToMeta=true);
    void injectFakeEventWithTriggerData(std::string fesaConcreteEventName, uint32_t sequenceIndex, int64_t preTriggerTime_ns, int64_t postTriggerTime_ns, bool addToMeta=true);

    void pushData();
    void pushSingleSampleWithMeta();
    void injectFakeSequence1_digitizer_streaming(uint32_t sequenceIndex);
    void injectFakeSequence1_digitizer_triggered(uint32_t sequenceIndex);
    void injectFakeSequence1_powersupply_streaming(uint32_t sequenceIndex);
    void injectFakeSequence1_streaming(uint32_t sequenceIndex, DataPushMode dataPushMode);
    void checkSequenceStreaming(uint32_t sequenceIndex, MaxClientUpdateFrequencyType update_frequency_hz, DataPushMode dataPushMode);
    void checkSequenceStreaming_noTiming(MaxClientUpdateFrequencyType update_frequency_hz, bool expect_ref_meta, bool timeSinceRefMetaSanityCheck);
    void checkSequenceFullSeq(uint32_t sequenceIndex, DataPushMode dataPushMode);
    void checkSequenceTriggered(uint32_t sequenceIndex);

    // Is called once for all tests of this fixture
    void SetUp() override;

    // Is called once for all tests of this fixture
    void TearDown() override;

};


} // end namespace
