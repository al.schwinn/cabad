/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <test/DataReadyHandlerFixture.h>
#include <test/TimingContextMock.h>
#include <test/Util.h>

namespace cabad
{

uint32_t DataReadyHandlerFixture::data_id_received_from_cb_ = 0;

//TEST_F(DataReadyHandlerFixture, streaming_requestMoveWindow)
//{
//    ClientNotificationData data;
//    std::size_t circular_size = 10;
//    CircularBuffer<int> buffer(circular_size);
//
//    DataReadyHandlerStreaming handler (dummy_group_,ClientNotificationType::STREAMING, dummy_updateFrequency_, dummy_sample_rate_hz, &queue_, callbackAllDataReady);
//
//    ASSERT_NO_THROW(
//    for(auto& buffer_manager : dummy_buffer_managers_)
//        handler.connect(buffer_manager);
//    );
//
//    ASSERT_NO_THROW(
//        fakeClock += 1000000000; // 1s to make sure it is time to notify
//        // data id only received after second move
//        handler.requestMoveWindow(1,1, &manager1_);
//        ASSERT_TRUE(data_id_received_from_cb_ == 0);
//        handler.requestMoveWindow(1,1, &manager2_);
//        ASSERT_TRUE(data_id_received_from_cb_ != 0);
//        ASSERT_TRUE(queue_.requestData(data_id_received_from_cb_, data));
//        ASSERT_EQ(data.notificationType_, ClientNotificationType::STREAMING);
//        ASSERT_EQ(data.updateFrequency_, dummy_updateFrequency_);
//        ASSERT_EQ(data.sinkNames_.size(), dummy_buffer_managers_.size());
//        ASSERT_EQ(data.sinkNames_[0], dummy_buffer_managers_[0]->getSignalName());
//        ASSERT_EQ(data.sinkNames_[1], dummy_buffer_managers_[1]->getSignalName());
//    );
//
//    data_id_received_from_cb_ = 0;
//
//    ASSERT_NO_THROW(
//        fakeClock += 1000000000; // 1s to make sure it is time to notify
//        handler.requestMoveWindow(2,1, &manager1_);
//        ASSERT_TRUE(data_id_received_from_cb_ == 0);
//        handler.requestMoveWindow(2,1, &manager2_);
//        ASSERT_TRUE(data_id_received_from_cb_ != 0);
//        ASSERT_TRUE(queue_.requestData(data_id_received_from_cb_, data));
//        data_id_received_from_cb_ = 0;
//    );
//
//    // Using the same manager twice should do nothing
//    ASSERT_NO_THROW(
//        fakeClock += 1000000000; // 1s to make sure it is time to notify
//        handler.requestMoveWindow(1,1, &manager1_);
//        handler.requestMoveWindow(2,1, &manager1_);
//        ASSERT_TRUE(data_id_received_from_cb_ == 0);
//    );
//
//    // Connection a sink twice should throw
//    ASSERT_THROW(
//            handler.connect(dummy_buffer_managers_[0]);
//    ,std::exception);
//}

TEST_F(DataReadyHandlerFixture, streaming_hasCircularBuffer)
{
    DataReadyHandlerStreaming handler (dummy_group_, dummy_notificationType_, dummy_updateFrequency_, dummy_sample_rate_hz, &queue_, callbackAllDataReady);

    ASSERT_NO_THROW(
    for(auto& buffer_manager : dummy_buffer_managers_)
        handler.connect(buffer_manager);

    ASSERT_TRUE(handler.hasCircularBuffer(dummy_buffer_managers_[0]->getSignalName(), dummy_notificationType_, dummy_updateFrequency_));
    ASSERT_FALSE(handler.hasCircularBuffer("unknown name", dummy_notificationType_, dummy_updateFrequency_));
    ASSERT_FALSE(handler.hasCircularBuffer(dummy_buffer_managers_[0]->getSignalName(), ClientNotificationType::STREAMING, dummy_updateFrequency_));
    ASSERT_FALSE(handler.hasCircularBuffer(dummy_buffer_managers_[0]->getSignalName(), ClientNotificationType::FULL_SEQUENCE, 100000)); // FULL_SEQ managed by DataReadyHandlerTriggered
    );
}

TEST_F(DataReadyHandlerFixture, triggered_hasCircularBuffer)
{
    uint32_t event_id = 123;
    DataReadyHandlerTriggered handlerTriggered (dummy_group_, ClientNotificationType::TRIGGERED, event_id , dummy_sample_rate_hz, &queue_, callbackAllDataReady);
    DataReadyHandlerTriggered handlerFullSEQ (dummy_group_, ClientNotificationType::FULL_SEQUENCE, event_id , dummy_sample_rate_hz, &queue_, callbackAllDataReady);

    ASSERT_NO_THROW(
        for(auto& buffer_manager : dummy_buffer_managers_)
        {
            handlerTriggered.connect(buffer_manager);
            handlerFullSEQ.connect(buffer_manager);
        }

        ASSERT_TRUE (handlerTriggered.hasCircularBuffer(dummy_buffer_managers_[0]->getSignalName(), ClientNotificationType::TRIGGERED));
        ASSERT_FALSE(handlerTriggered.hasCircularBuffer(dummy_buffer_managers_[0]->getSignalName(), ClientNotificationType::FULL_SEQUENCE));
        ASSERT_FALSE(handlerTriggered.hasCircularBuffer("unknown name", ClientNotificationType::TRIGGERED));
        ASSERT_FALSE(  handlerFullSEQ.hasCircularBuffer("unknown name", ClientNotificationType::FULL_SEQUENCE));
        ASSERT_TRUE (handlerFullSEQ.hasCircularBuffer(dummy_buffer_managers_[0]->getSignalName(), ClientNotificationType::FULL_SEQUENCE));
        ASSERT_FALSE(handlerFullSEQ.hasCircularBuffer(dummy_buffer_managers_[0]->getSignalName(), ClientNotificationType::TRIGGERED));
    );
}


} // end namespace
