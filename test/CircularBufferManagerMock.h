/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/CircularBufferManagerBase.h>
#include <cabad/DeviceDataBufferBase.h>

#include <string>

namespace cabad
{

//FIXME: USe GMOCK instead

class CircularBufferManagerMock : public CircularBufferManagerBase
{
    public:

    CircularBufferManagerMock (std::string signal_name) : CircularBufferManagerBase(signal_name)
    {

    }

    CircularWindowIterator*
    updateStreamingWindowIter(MaxClientUpdateFrequencyType update_frequency_hz,
                              int64_t                      last_sample_stamp,
                              std::size_t                  n_samples_max)
    {
        std::vector <std::string> eventNames;
        DeviceDataBufferBase* device_data = new DeviceDataBufferBase(eventNames, "deviceName");
        MetaDataBuffer* metabuffer = new MetaDataBuffer(10, device_data);
        CircularWindowIterator* window = new CircularWindowIterator(metabuffer); // TODO: Mock the window !!
        return window;
    }

    CircularWindowIterator*
    updateFullSequenceWindowIter(int64_t last_sequence_marker_stamp)
    {
        // Fake sucessfull update whenever there is a new stamp
        if(last_trigger_stamp == last_sequence_marker_stamp)
            return NULL;
        last_trigger_stamp = last_sequence_marker_stamp;
        std::vector <std::string> eventNames;
        DeviceDataBufferBase* device_data = new DeviceDataBufferBase(eventNames, "deviceName");
        MetaDataBuffer* metabuffer = new MetaDataBuffer(10, device_data);
        CircularWindowIterator* window = new CircularWindowIterator(metabuffer); // TODO: Mock the window !!
        return window;
    }

    CircularWindowIterator*
    updateTriggerWindowIter(int64_t     trigger_stamp,
                            EventNumber eventNumber)
    {
        // Fake sucessfull update whenever there is a new stamp
        if(last_trigger_stamp == trigger_stamp)
            return NULL;
        last_trigger_stamp = trigger_stamp;
        std::vector <std::string> eventNames;
        DeviceDataBufferBase* device_data = new DeviceDataBufferBase(eventNames, "deviceName");
        MetaDataBuffer* metabuffer = new MetaDataBuffer(10, device_data);
        CircularWindowIterator* window = new CircularWindowIterator(metabuffer); // TODO: Mock the window !!
        return window;
    }

private:

    int64_t last_trigger_stamp;

};


} // end namespace
