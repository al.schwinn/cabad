/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <test/CircularBufferManagerBaseTestFixture.h>

#include <cabad/CircularBuffer.h>
#include <cabad/CircularBufferManagerBase.h>
#include <cabad/DeviceDataBufferBase.h>
#include <cabad/ContextTracker.h>
#include <cabad/Util.h>

#include <string.h> //memcopy

#define samp_rate_1Hz            1
#define samp_rate_10Hz          10
#define samp_rate_100Hz        100
#define samp_rate_1kHz        1000
#define samp_rate_10kHz      10000
#define samp_rate_100kHz    100000
#define samp_rate_1MHz     1000000
#define samp_rate_10MHz   10000000
#define samp_rate_100MHz 100000000

#define FLOAT_COMPARISON_TOLERANCE 1.0e-7 // 100ns tolerance for tests. TODO: Decrease the tolerance !

namespace cabad
{

bool  CircularBufferManagerBaseTestFixture::data_ready_flagged_ = false;

void CircularBufferManager::push(  const float                  *data,
                                    std::size_t                  data_size,
                                    std::vector<FakeMetaInfo>    meta_to_push)
{
    CIRCULAR_BUFFER_TYPE *buffer = static_cast<CIRCULAR_BUFFER_TYPE*>(circularBuffer_);
    std::shared_ptr<CircularBufferBase::iterator> firstNewSample = buffer->getCopyOfWriteIterator();
    if(DEBUG_ENABLED)
      std::cout << "pushing " << data_size << " sample(s) starting by index: "<< firstNewSample->index() << std::endl;
    buffer->push(data, data, data_size);
    std::shared_ptr<CircularBufferBase::iterator> lastNewSample = buffer->getCopyOfWriteIterator();
    --(*lastNewSample);
    //std::cout << "Number of tags found: " << tags.size() << std::endl;
    for (const auto& meta : meta_to_push)
    {
        if( !meta.fesaConcreteEventName.empty() && deviceDataBuffer_->isTriggerEvent((meta.context)->getEventNumber()) )
        {
            //std::cout << "pushing meta for stamp: "<< meta.stamp << std::endl;
            addTriggerMetaData(meta.trigger_stamp, meta.abs_offset, meta.status);
        }
        else
        {
            //std::cout << "pushing meta for stamp: "<< meta.stamp << std::endl;
            addWrStampMetaData(meta.trigger_stamp, meta.abs_offset, meta.status);
        }
    }
    addMultiplexingContextDataForRange(firstNewSample, lastNewSample);
    pushBackDataFinished();
}

void CircularBufferManager::copyDataWindow(CircularWindowIterator* window_iter, float* buffer1, float* buffer2, const std::size_t& buffer_size, std::size_t& n_data_written)
{
    if(window_iter->disabled())
        return;

    CIRCULAR_BUFFER_TYPE *buffer = static_cast<CIRCULAR_BUFFER_TYPE*>(circularBuffer_);

    buffer->copyDataWindow(*(window_iter->data_start_), *(window_iter->data_end_),buffer1, buffer2, buffer_size, n_data_written);
}

void CircularBufferManagerBaseTestFixture::SetUp()
{
    if(DEBUG_ENABLED)
      std::cout << "Setup called " << std::endl;
    fakeClock = START_TIME;
    fakeClockCarry_ = 0;
    fakeSampleOffset_ = 0;
    fakeSamplesPushed_ = 0;
    fakeSamplesChecked_ = 0;
    preTriggerSamples_ = 0;
    postTriggerSamples_ = 0;
    nTriggersChecked_ = 0;
    nSamplesInPreviousSequence_ = 0;
    nSamplesIncurrentSequence_ = 0;
    random_measurement_jitter_min_ns_ = 0;
    random_measurement_jitter_max_ns_ = 100;// 100000;// TODO: Currently 100ns to at least test that it does not need to match.. should be be tested till up to 0,9ms (default tolerance 1ms for triggers)
    trigger_events_.push_back("CMD_SEQ_START#257");
    trigger_events_.push_back("CMD_BEAM_INJECTION#283");
    trigger_events_.push_back("CMD_BEAM_EXTRACTION#284");
    trigger_events_.push_back("CMD_START_ENERGY_RAMP#285");
    trigger_events_.push_back("CMD_CUSTOM_DIAG_1#286");
    trigger_events_.push_back("CMD_CUSTOM_DIAG_2#287");
    trigger_events_.push_back("CMD_CUSTOM_DIAG_3#288");
    trigger_events_.push_back("CMD_FG_START#513");
    trigger_events_.push_back("EVT_COMMAND#255");
    trigger_events_.push_back("EVT_GAP_START#258");

    setLogFunction(fake_log_function);
    setTimeStampFunction(fake_clock_get_utc_now);
}

void CircularBufferManagerBaseTestFixture::TearDown()
{
    if(DEBUG_ENABLED)
      std::cout << "TearDown called " << std::endl;
    metadata_buffer_.reset();
    circular_buffer_.reset();
    context_tracker_.reset();
    device_data_buffer_.reset();
    buffer_manager_.reset();
    trigger_events_.clear();
    meta_to_push_.clear();
    meta_pushed_.clear();
    fakeSampleStamps_.clear();
}

void CircularBufferManagerBaseTestFixture::createCircularBufferManager (
        float sampleRate,
        std::size_t size_buffer,
        std::size_t meta_buffer_size,
        std::size_t contextTrackerSize,
        ClientNotificationType clientNotificationType,
        DataPushMode dataPushMode)
{
    sampRate_ = sampleRate;
    nSamplesTillPush_ = sampleRate / 10;
    if(DEBUG_ENABLED)
    {
        std::cout << "sampRate_: " << sampRate_ << std::endl;
        std::cout << "nSamplesTillPush_: " << nSamplesTillPush_ << std::endl;
    }
    std::vector<MaxClientUpdateFrequencyType> maxClientUpdateFrequencies = {1,10,25};
    device_data_buffer_.reset(new DeviceDataBufferBase(trigger_events_, "FakeDevice"));
    metadata_buffer_.reset(new MetaDataBuffer(meta_buffer_size, &(*device_data_buffer_)));
    circular_buffer_.reset(new CIRCULAR_BUFFER_TYPE(size_buffer));
    context_tracker_.reset(new ContextTracker(contextTrackerSize));
    device_data_buffer_->setRefMetaTriggerEvents("CMD_BEAM_INJECTION#283", "CMD_SEQ_START#257", "EVT_COMMAND#255");
    buffer_manager_.reset(new CircularBufferManager("FakeSignal",
                                                    sampleRate,
                                                    maxClientUpdateFrequencies,
                                                    context_tracker_.get(),
                                                    device_data_buffer_.get(),
                                                    circular_buffer_.get(),
                                                    metadata_buffer_.get(),
                                                    clientNotificationType,
                                                    dataPushMode,
                                                    &dataReadyManager_));
}

void CircularBufferManagerBaseTestFixture::wait(int milliseconds)
{
    fakeClock += milliseconds * 1000000;
}

void CircularBufferManagerBaseTestFixture::waitWithPush(int milliseconds)
{
    int64_t oldTime = fakeClock + fakeClockCarry_;
    int64_t newTime = oldTime + milliseconds * 1000000;
    int64_t sampleTosampleDistance_ns = buffer_manager_->getSampleToSampleDistance() *  1000000000;
    for( int64_t time = oldTime; time <= newTime; time+=sampleTosampleDistance_ns)
    {
        fakeSampleStamps_.push_back(fakeClock);
        //std::cout << "fakeSampleStamps_["<<fakeSampleOffset_<<"]: " << fakeSampleStamps_[fakeSampleOffset_] << std::endl;
        fakeSampleOffset_++;
        //std::cout << "fakeSamplesStamp.size()" << fakeSampleStamps_.size() << std::endl;
        if((fakeSampleOffset_ - fakeSamplesPushed_) >= nSamplesTillPush_)
        {
            if(DEBUG_ENABLED)
                std::cout << "Pushing data. First stamp: " << fakeSampleStamps_[fakeSamplesPushed_] << " at index: "<< fakeSamplesPushed_<<  std::endl;
            pushData();
        }
        fakeClock += sampleTosampleDistance_ns;
    }
    fakeClockCarry_ = newTime - fakeClock;
}

void CircularBufferManagerBaseTestFixture::injectFakeEvent(int64_t timestamp, std::string fesaConcreteEventName, uint32_t sequenceIndex ,bool addToMeta )
{
    unsigned int eventNumber = 0;
    try
    {
        eventNumber = device_data_buffer_->eventName2eventID(device_data_buffer_->extractEventName(fesaConcreteEventName));
    }
    catch(...)
    {
        // thsi will only suceed if the event is a trigger event.
        //All other events will have event number 0
        eventNumber = 4711;
    }

    std::shared_ptr<const TimingContext> context(new TimingContextMock(timestamp, eventNumber));

    context_tracker_->addContext(context);
    if(DEBUG_ENABLED)
    {
        if(device_data_buffer_->isTriggerEvent(eventNumber))
        {
            if(device_data_buffer_->isRefMetaTriggerEvent(eventNumber))
                std::cout << "injecting ref trigger stamp: " << timestamp << " Name: " << fesaConcreteEventName << std::endl;
            else
                std::cout << "injecting     trigger stamp: " << timestamp << " Name: " << fesaConcreteEventName << std::endl;
        }
        else
                std::cout << "injecting             stamp: " << timestamp << " Name: " << fesaConcreteEventName << std::endl;
    }
    if(device_data_buffer_->isTriggerEvent(eventNumber))
    {
        if( device_data_buffer_->isRefMetaTriggerEvent(eventNumber))
            fakeRefMetaCol_[timestamp] = device_data_buffer_->eventID2eventName(eventNumber);

        if ( fesaConcreteEventName == "CMD_SEQ_START#257")
        {
            prevSeqStartEvent_ = currentSeqStartEvent_;
            currentSeqStartEvent_ = context;
            //std::cout << "currentSeqStartEvent_: " << currentSeqStartEvent_->getTimeStamp() << std::endl;
            nSamplesInPreviousSequence_ = nSamplesIncurrentSequence_ ;
            nSamplesIncurrentSequence_ = 0;
        }
        // add some random offset to trigger stamp to simulate hardware in a realistic way
        int64_t offset = random_measurement_jitter_min_ns_ + rand() % ( random_measurement_jitter_max_ns_ - random_measurement_jitter_min_ns_);
        //std::cout << "random offset: " << offset << std::endl;
        CircularBufferManager::FakeMetaInfo meta(timestamp + offset,fakeSampleOffset_, 0, fesaConcreteEventName, context);
        if(addToMeta)
            meta_to_push_.push_back(meta);
    }
}

void CircularBufferManagerBaseTestFixture::injectFakeEventWithTriggerData(std::string fesaConcreteEventName, uint32_t sequenceIndex, int64_t preTriggerTime_ns, int64_t postTriggerTime_ns, bool addToMeta )
{
    if(!device_data_buffer_->isTriggerEvent(device_data_buffer_->eventName2eventID(device_data_buffer_->extractEventName(fesaConcreteEventName))))
    {
        std::cout << "Test configuration error: '"<< fesaConcreteEventName << "' is not a trigger event. This method only should be usedwith trigger events. Use 'injectFakeEvent' for non-trigger events" << std::endl;
        exit(1);
    }

    std::size_t samples = preTriggerSamples_ + postTriggerSamples_;
    int64_t sampleTosampleDistance_ns = buffer_manager_->getSampleToSampleDistance() *  1000000000;

    for( std::size_t samp_count = 0; samp_count < samples; samp_count++)
    {
        if( samp_count == preTriggerSamples_  )//trigger tag on first post trigger sample
            injectFakeEvent(fakeClock, fesaConcreteEventName, sequenceIndex, addToMeta);

        fakeSampleStamps_.push_back(fakeClock);
        //std::cout << "fakeSampleStamps_["<<fakeSampleOffset_<<"]: " << fakeSampleStamps_[fakeSampleOffset_] <<  " timestamp: " << fakeClock << std::endl;
        fakeSampleOffset_++;
        fakeClock += sampleTosampleDistance_ns;
    }
    pushData();
}

void CircularBufferManagerBaseTestFixture::pushData()
{
    uint64_t newDataSize = fakeSampleOffset_ - fakeSamplesPushed_;
    float data[newDataSize];
    for(std::size_t i= 0;i<newDataSize;i++)
    {
        data[i]=float(fakeSamplesPushed_);
        fakeSamplesPushed_++;
    }
    //std::cout << "fakeSamplesPushed_: " << fakeSamplesPushed_ << std::endl;
    buffer_manager_->push(&data[0], newDataSize, meta_to_push_ );
    nSamplesIncurrentSequence_ += newDataSize;

    // insert all pushed data into pushed
    meta_pushed_.insert(meta_pushed_.end(), meta_to_push_.begin(), meta_to_push_.end());
    meta_to_push_.clear();
}

void CircularBufferManagerBaseTestFixture::pushSingleSampleWithMeta()
{

    uint64_t newDataSize = 1;
    float data = fakeSamplesPushed_;
    meta_to_push_.clear();
    CircularBufferManager::FakeMetaInfo meta(fakeClock,fakeSampleOffset_,0);
    fakeSampleOffset_++;
    meta_to_push_.push_back(meta);
    //std::cout << "pushSingleSampleWithStamp: " << fakeSamplesPushed_ << " to index: "  << buffer_manager_->getCircularBuffer()->getWriteIterIndex()<< std::endl;
    if(DEBUG_ENABLED)
      std::cout << "pushing single sample at    stamp: " << fakeClock << std::endl;
    buffer_manager_->push(&data, newDataSize, meta_to_push_ );
    fakeSampleStamps_.push_back(fakeClock);
    fakeSamplesPushed_++;
}

void CircularBufferManagerBaseTestFixture::injectFakeSequence1_digitizer_streaming(uint32_t sequenceIndex)
{
    if(DEBUG_ENABLED)
        std::cout << "--- starting to inject new sequence ---" << std::endl;
    int64_t fakeClockStart = fakeClock;
    uint64_t fakeSamplesPushedStart = fakeSamplesPushed_;
    waitWithPush(530);
    injectFakeEvent(fakeClock, "CMD_SEQ_START#257", sequenceIndex);
    waitWithPush(100);
    injectFakeEvent(fakeClock, "CMD_BP_START#256", sequenceIndex);
    waitWithPush(100);
    injectFakeEvent(fakeClock, "CMD_BEAM_INJECTION#283", sequenceIndex);
    waitWithPush(10);
    injectFakeEvent(fakeClock, "CMD_FG_START#513", sequenceIndex);
    waitWithPush(10);
    injectFakeEvent(fakeClock, "CMD_BP_START#256", sequenceIndex);
    waitWithPush(10);
    injectFakeEvent(fakeClock, "CMD_CUSTOM_DIAG_1#286", sequenceIndex);
    waitWithPush(1);
    injectFakeEvent(fakeClock, "CMD_START_ENERGY_RAMP#285", sequenceIndex);
    waitWithPush(39);
    injectFakeEvent(fakeClock, "CMD_CUSTOM_DIAG_2#287", sequenceIndex);
    waitWithPush(100);
    injectFakeEvent(fakeClock, "CMD_CUSTOM_DIAG_3#288", sequenceIndex);
    waitWithPush(50);
    injectFakeEvent(fakeClock, "CMD_BEAM_EXTRACTION#284", sequenceIndex);
    waitWithPush(50);
    if(DEBUG_ENABLED)
      std::cout << "--- This sequence took "<< float(fakeClock - fakeClockStart) / 1000000000 << " seconds" << " - " << fakeSamplesPushed_ - fakeSamplesPushedStart << " new samples added ---" << std::endl;
}

void CircularBufferManagerBaseTestFixture::injectFakeSequence1_digitizer_triggered(uint32_t sequenceIndex )
{
    if(DEBUG_ENABLED)
        std::cout << "--- starting to inject new sequence ---" << std::endl;
    int64_t sampleTosampleDistance_ns = buffer_manager_->getSampleToSampleDistance() *  1000000000;
    int64_t preTriggerTime_ns = preTriggerSamples_ * sampleTosampleDistance_ns;
    int64_t postTriggerTime_ns = postTriggerSamples_ * sampleTosampleDistance_ns;
//    std::cout << "preTriggerTime_ns "<< preTriggerTime_ns << std::endl;
//    std::cout << "postTriggerTime_ns "<< postTriggerTime_ns << std::endl;
    int64_t fakeClockStart = fakeClock;
    uint64_t fakeSamplesPushedStart = fakeSamplesPushed_;
    wait(530);
    injectFakeEventWithTriggerData("CMD_SEQ_START#257", sequenceIndex, preTriggerTime_ns, postTriggerTime_ns);
    wait(100);
    injectFakeEvent(fakeClock, "CMD_BP_START#256", sequenceIndex);
    wait(100);
    injectFakeEventWithTriggerData("CMD_BEAM_INJECTION#283", sequenceIndex, preTriggerTime_ns, postTriggerTime_ns);
    wait(10);
    injectFakeEventWithTriggerData("CMD_FG_START#513", sequenceIndex, preTriggerTime_ns, postTriggerTime_ns);
    wait(10);
    injectFakeEvent(fakeClock, "CMD_BP_START#256", sequenceIndex);
    wait(10);
    injectFakeEventWithTriggerData("CMD_CUSTOM_DIAG_1#286", sequenceIndex, preTriggerTime_ns, postTriggerTime_ns);
    wait(1);
    injectFakeEventWithTriggerData("CMD_START_ENERGY_RAMP#285", sequenceIndex, preTriggerTime_ns, postTriggerTime_ns);
    wait(39);
    injectFakeEventWithTriggerData("CMD_CUSTOM_DIAG_2#287", sequenceIndex, preTriggerTime_ns, postTriggerTime_ns);
    wait(100);
    injectFakeEventWithTriggerData("CMD_CUSTOM_DIAG_3#288", sequenceIndex, preTriggerTime_ns, postTriggerTime_ns);
    wait(50);
    injectFakeEventWithTriggerData("CMD_BEAM_EXTRACTION#284", sequenceIndex, preTriggerTime_ns, postTriggerTime_ns);
    wait(50);
    if(DEBUG_ENABLED)
      std::cout << "--- This sequence took "<< float(fakeClock - fakeClockStart) / 1000000000 << " seconds" << " - " << fakeSamplesPushed_ - fakeSamplesPushedStart << " new samples added ---" << std::endl;
}

void CircularBufferManagerBaseTestFixture::injectFakeSequence1_powersupply_streaming(uint32_t sequenceIndex)
{
    if(DEBUG_ENABLED)
        std::cout << "--- starting to inject new sequence ---" << std::endl;
    int64_t fakeClockStart = fakeClock;
    uint64_t fakeSamplesPushedStart = fakeSamplesPushed_;
    wait(530);
    injectFakeEvent(fakeClock, "CMD_SEQ_START#257", sequenceIndex);
    wait(1);
    pushSingleSampleWithMeta(); // TODO: In reality, there are bursts of measurements (with fixed samp rate) .. to be simulated in a more realistic way !
    wait(100);
    injectFakeEvent(fakeClock, "CMD_BP_START#256", sequenceIndex);
    wait(5);
    pushSingleSampleWithMeta();
    wait(100);
    injectFakeEvent(fakeClock, "CMD_BEAM_INJECTION#283", sequenceIndex);
    wait(2);
    pushSingleSampleWithMeta();
    wait(10);
    injectFakeEvent(fakeClock, "CMD_FG_START#513", sequenceIndex);
    wait(1);
    pushSingleSampleWithMeta();
    wait(10);
    injectFakeEvent(fakeClock, "CMD_BP_START#256", sequenceIndex);
    wait(1);
    pushSingleSampleWithMeta();
    wait(10);
    injectFakeEvent(fakeClock, "CMD_CUSTOM_DIAG_1#286", sequenceIndex);
    wait(5);
    pushSingleSampleWithMeta();
    wait(1);
    injectFakeEvent(fakeClock, "CMD_START_ENERGY_RAMP#285", sequenceIndex);
    wait(1);
    pushSingleSampleWithMeta();
    wait(20);
    injectFakeEvent(fakeClock, "CMD_CUSTOM_DIAG_2#287", sequenceIndex);
    wait(1);
    injectFakeEvent(fakeClock, "CMD_CUSTOM_DIAG_2#287", sequenceIndex);//event with same timestamp than data
    pushSingleSampleWithMeta();
    wait(100);
    injectFakeEvent(fakeClock, "CMD_CUSTOM_DIAG_3#288", sequenceIndex);
    wait(1);
    pushSingleSampleWithMeta();
    wait(50);
    pushSingleSampleWithMeta();//multiple push without event in between
    wait(50);
    injectFakeEvent(fakeClock, "CMD_BEAM_EXTRACTION#284", sequenceIndex);
    wait(1);
    pushSingleSampleWithMeta();
    if(DEBUG_ENABLED)
      std::cout << "--- This sequence took "<< float(fakeClock - fakeClockStart) / 1000000000 << " seconds" << " - " << fakeSamplesPushed_ - fakeSamplesPushedStart << " new samples added ---" << std::endl;
}

void CircularBufferManagerBaseTestFixture::injectFakeSequence1_streaming(uint32_t sequenceIndex, DataPushMode dataPushMode)
{
    if(dataPushMode == DataPushMode::DISJUNCT_SINGLE_SAMPLES)
        injectFakeSequence1_powersupply_streaming(sequenceIndex);
    else
        injectFakeSequence1_digitizer_streaming(sequenceIndex);
}

TEST_F(CircularBufferManagerBaseTestFixture, creation)
{
    createCircularBufferManager(float(10), 1000,100, 200, ClientNotificationType::STREAMING, DataPushMode::CONTINOUS_MULTI_SAMPLES);
    //ASSERT_EQ(writeIter->index(), 2);
}

TEST_F(CircularBufferManagerBaseTestFixture, push_single_values_no_meta)
{
    createCircularBufferManager(float(10), 1000,100, 200, ClientNotificationType::STREAMING, DataPushMode::DISJUNCT_SINGLE_SAMPLES);
    std::size_t data_size = 1;
    float data[data_size];
    for(std::size_t i= 0;i<data_size;i++)
        data[i]=float(i);
    std::vector<CircularBufferManager::FakeMetaInfo>    meta_to_push;
    buffer_manager_->push(&data[0], data_size, meta_to_push );
    buffer_manager_->push(&data[0], data_size, meta_to_push );
    buffer_manager_->push(&data[0], data_size, meta_to_push );
}

TEST_F(CircularBufferManagerBaseTestFixture, digitizer_push_multi_values_no_meta)
{
    createCircularBufferManager(float(10), 1000,100, 200, ClientNotificationType::STREAMING, DataPushMode::CONTINOUS_MULTI_SAMPLES);
    std::size_t data_size = 10;
    float data[data_size];
    for(std::size_t i= 0;i<data_size;i++)
        data[i]=float(i);
    std::vector<CircularBufferManager::FakeMetaInfo>    meta_to_push;
    buffer_manager_->push(&data[0], data_size, meta_to_push );
    buffer_manager_->push(&data[0], data_size, meta_to_push );
    buffer_manager_->push(&data[0], data_size, meta_to_push );
}

void CircularBufferManagerBaseTestFixture::checkSequenceStreaming(uint32_t sequenceIndex, MaxClientUpdateFrequencyType update_frequency_hz, DataPushMode dataPushMode)
{
    std::vector<float> timeSinceRefMeta;
    CircularWindowIterator* window_iterator;
    std::shared_ptr<const SampleMetadata> ref_meta;

    injectFakeSequence1_streaming(sequenceIndex, dataPushMode);

    ASSERT_TRUE (data_ready_flagged_);
    data_ready_flagged_ = false;

    window_iterator = buffer_manager_->getStreamingDataWindow(update_frequency_hz);

//    std::cout <<  "data_start_: " << window_iterator->data_start_->index() << std::endl;
//    std::cout <<  "data_end_  : " << window_iterator->data_end_->index() << std::endl;
    std::size_t result_size = 100000, n_data_written = 0;
    float result1[result_size], result2[result_size];
    buffer_manager_->copyDataWindow(window_iterator, &result1[0], &result2[0], result_size, n_data_written);

    ASSERT_TRUE (n_data_written > 0);
    ASSERT_TRUE (window_iterator->windowSize() > 0);

    ASSERT_EQ(window_iterator->getWindowStatus(), (unsigned int)0);
    ref_meta = window_iterator->getRefMeta();
    ASSERT_FALSE(!ref_meta);

    auto fakeRefMeta = fakeRefMetaCol_.find(ref_meta->getTimingContext()->getTimeStamp());
    ASSERT_TRUE(fakeRefMeta != fakeRefMetaCol_.end())<< "RefMeta not found in fakeRefMetas";
    ASSERT_STREQ(fakeRefMeta->second.c_str(), device_data_buffer_->eventID2eventName(ref_meta->getTimingContext()->getEventNumber()).c_str());
    int64_t fakeRefMetaStamp = fakeRefMeta->first;

    timeSinceRefMeta = window_iterator->getTimeSinceRefMetaForEachSample();
    ASSERT_EQ (window_iterator->timeSinceRefMetaSanityCheck(timeSinceRefMeta, 0, true),0);
    ASSERT_TRUE (timeSinceRefMeta.size() > 0);

    for (std::size_t i=0,j = fakeSamplesChecked_;i< timeSinceRefMeta.size();i++,j++ )
    {
        if(DEBUG_ENABLED)
        {
        	std::cout << *window_iterator;
            std::cout << "fakeSampleStamps_[" << j <<"]        : " << fakeSampleStamps_[j] << std::endl;
            std::cout << "fakeRefMetaStamp         : " << fakeRefMetaStamp << std::endl;
            std::cout << "measured timeSinceRefMeta["<<i<<"]      : " << timeSinceRefMeta[i] << std::endl;
            std::cout << "expected timeSinceRefMeta["<<j<<"]      : " << (fakeSampleStamps_[j] - fakeRefMetaStamp)/1000000000. << std::endl;
        }
        ASSERT_NEAR((fakeSampleStamps_[j] - fakeRefMetaStamp)/1000000000., timeSinceRefMeta[i], FLOAT_COMPARISON_TOLERANCE ) << "Wrong timeSinceRefMeta at index: " << i;
        ASSERT_EQ(result1[i], j);
    }

    fakeSamplesChecked_ += timeSinceRefMeta.size();
}

void CircularBufferManagerBaseTestFixture::checkSequenceStreaming_noTiming(MaxClientUpdateFrequencyType update_frequency_hz, bool expect_ref_meta, bool timeSinceRefMetaSanityCheck)
{
    std::vector<float> timeSinceRefMeta;
    CircularWindowIterator* window_iterator;
    std::shared_ptr<const SampleMetadata> ref_meta;

    waitWithPush(1000);

    ASSERT_TRUE (data_ready_flagged_);
    data_ready_flagged_ = false;

    window_iterator = buffer_manager_->getStreamingDataWindow(update_frequency_hz);
//    std::cout <<  "data_start_: " << window_iterator->data_start_->index() << std::endl;
//    std::cout <<  "data_end_  : " << window_iterator->data_end_->index() << std::endl;
    std::size_t result_size = 100000, n_data_written = 0;
    float result1[result_size], result2[result_size];
    buffer_manager_->copyDataWindow(window_iterator, &result1[0], &result2[0], result_size, n_data_written);
    ref_meta = window_iterator->getRefMeta();

    ASSERT_TRUE (n_data_written > 0);
    if(DEBUG_ENABLED)
        std::cout << "Pushed " << n_data_written << " new samples to buffer" << std::endl;
    ASSERT_TRUE (window_iterator->windowSize() > 0);
    ASSERT_EQ(window_iterator->getWindowStatus(), (unsigned int)0);
    if(expect_ref_meta)
    {
        ASSERT_FALSE(!ref_meta);

        auto fakeRefMeta = fakeRefMetaCol_.find(ref_meta->getTimingContext()->getTimeStamp());
        ASSERT_TRUE(fakeRefMeta != fakeRefMetaCol_.end())<< "RefMeta not found in fakeRefMetas";
        ASSERT_STREQ(fakeRefMeta->second.c_str(), device_data_buffer_->eventID2eventName(ref_meta->getTimingContext()->getEventNumber()).c_str());
        int64_t fakeRefMetaStamp = fakeRefMeta->first;

        timeSinceRefMeta = window_iterator->getTimeSinceRefMetaForEachSample();
        if(timeSinceRefMetaSanityCheck)
        {
            ASSERT_EQ (window_iterator->timeSinceRefMetaSanityCheck(timeSinceRefMeta, 0, true),0);
        }

        ASSERT_TRUE (timeSinceRefMeta.size() > 0);

        for (std::size_t i=0,j = fakeSamplesChecked_;i< timeSinceRefMeta.size();i++,j++ )
        {
            if(DEBUG_ENABLED)
            {
                std::cout << "fakeSampleStamps_[j]        : " << fakeSampleStamps_[j] << std::endl;
                std::cout << "fakeRefMetaStamp         : " << fakeRefMetaStamp << std::endl;
                std::cout << "measured timeSinceRefMeta["<<i<<"]      : " << timeSinceRefMeta[i] << std::endl;
                std::cout << "expected timeSinceRefMeta["<<j<<"]      : " << (fakeSampleStamps_[j] - fakeRefMetaStamp)/1000000000. << std::endl;
            }
            // TODO: ASSERT_NEAR((fakeSampleStamps_[j] - fakeRefMetaStamp)/1000000000., timeSinceRefMeta[i], FLOAT_COMPARISON_TOLERANCE ) << "Wrong timeSinceRefMeta at index: " << i;
            ASSERT_EQ(result1[i], j);
        }
    }
    else
    {
        //std::cout << "ref_meta->getTimingContext()->getTimeStamp()        : " << ref_meta->getTimingContext() ==  << std::endl;
        ASSERT_TRUE(!ref_meta);
        for (std::size_t i=0,j = fakeSamplesChecked_;i< timeSinceRefMeta.size();i++,j++ )
        {
            ASSERT_EQ(fakeSampleStamps_[j], 0);
            ASSERT_EQ(result1[i], j);
        }
    }
    fakeSamplesChecked_ += timeSinceRefMeta.size();
}

void CircularBufferManagerBaseTestFixture::checkSequenceFullSeq(uint32_t sequenceIndex, DataPushMode dataPushMode)
{
    std::vector<float> timeSinceRefMeta;
    CircularWindowIterator* window_iterator;
    std::shared_ptr<const SampleMetadata> ref_meta;

    uint64_t fakeSamplesPushedBefore = fakeSamplesPushed_;
    injectFakeSequence1_streaming(sequenceIndex, dataPushMode);

    ASSERT_TRUE (data_ready_flagged_);
    data_ready_flagged_ = false;

    window_iterator = buffer_manager_->getFullSequenceDataWindow (prevSeqStartEvent_);
//    std::cout <<  "data_start_: " << window_iterator->data_start_->index() << std::endl;
//    std::cout <<  "data_end_  : " << window_iterator->data_end_->index() << std::endl;
    std::size_t result_size = 100000, n_data_written = 0;
    float result1[result_size], result2[result_size];
    buffer_manager_->copyDataWindow(window_iterator, &result1[0], &result2[0], result_size, n_data_written);
    if(DEBUG_ENABLED)
        std::cout << "** FULL_SEQ Window Size: " << result_size << std::endl;

    //std::cout << "** nSamplesIncurrentSequence_: " << nSamplesIncurrentSequence_ << std::endl;
    //std::cout << "** nSamplesInPreviousSequence_: " << nSamplesInPreviousSequence_ << std::endl;
    //std::cout << "** fakeSamplesPushed_: " << fakeSamplesPushed_ << std::endl;
    //std::cout << "** fakeSamplesPushedBefore: " << fakeSamplesPushedBefore << std::endl;

    std::size_t expectedNUmberOfPushesInSequence = fakeSamplesPushed_ - fakeSamplesPushedBefore ;
    ASSERT_EQ(expectedNUmberOfPushesInSequence, n_data_written);
    ASSERT_EQ(expectedNUmberOfPushesInSequence, window_iterator->windowSize());
    ASSERT_EQ((unsigned int)0, window_iterator->getWindowStatus());
    ref_meta = window_iterator->getRefMeta();
    ASSERT_FALSE(!ref_meta);

    // Get the ref-trigger which occured before the last ref-trigger
    auto previousfakeRefMeta = fakeRefMetaCol_.end();
    previousfakeRefMeta--; // now we have the last list element
    previousfakeRefMeta--; // now we have the second last list element

    ASSERT_STREQ(previousfakeRefMeta->second.c_str(), device_data_buffer_->eventID2eventName(ref_meta->getTimingContext()->getEventNumber()).c_str());
    ASSERT_EQ(previousfakeRefMeta->first, ref_meta->getTimingContext()->getTimeStamp())<< "TestError: RefMeta Skipped";

    timeSinceRefMeta = window_iterator->getTimeSinceRefMetaForEachSample();
    ASSERT_EQ (window_iterator->timeSinceRefMetaSanityCheck(timeSinceRefMeta, 0, true),0);
    ASSERT_EQ(timeSinceRefMeta.size(), expectedNUmberOfPushesInSequence);
}

void CircularBufferManagerBaseTestFixture::checkSequenceTriggered(uint32_t sequenceIndex)
{
    int64_t fakeSamplesChecked = fakeSamplesPushed_;
    std::vector<float> timeSinceRefMeta;
    CircularWindowIterator* window_iterator;
    std::shared_ptr<const SampleMetadata> ref_meta;

    //uint64_t fakeSamplesPushedBefore = fakeSamplesPushed_;
    injectFakeSequence1_digitizer_triggered(sequenceIndex);

    ASSERT_TRUE (data_ready_flagged_);
    data_ready_flagged_ = false;

    for( ;nTriggersChecked_ !=  meta_pushed_.size(); nTriggersChecked_++ )
    {
        CircularBufferManager::FakeMetaInfo meta = meta_pushed_[nTriggersChecked_];
        window_iterator = buffer_manager_->getTriggerDataWindow(device_data_buffer_->extractEventName(meta.fesaConcreteEventName));

//        std::cout <<  "data_start_: " << window_iterator->data_start_->index() << std::endl;
//        std::cout <<  "data_end_  : " << window_iterator->data_end_->index() << std::endl;
        std::size_t result_size = 100000, n_data_written = 0;
        float result1[result_size], result2[result_size];
        buffer_manager_->copyDataWindow(window_iterator, &result1[0], &result2[0], result_size, n_data_written);

        ASSERT_EQ(n_data_written, preTriggerSamples_ + postTriggerSamples_);
        ASSERT_EQ(window_iterator->windowSize(), preTriggerSamples_ + postTriggerSamples_);
        ASSERT_EQ(window_iterator->getWindowStatus(), (unsigned int)0);
        ref_meta = window_iterator->getRefMeta();
        ASSERT_FALSE(!ref_meta);

        // Triggerers should be alligned to the correct trigger position (First post trigger sample)
        ASSERT_STREQ( device_data_buffer_->extractEventName(meta.fesaConcreteEventName).c_str(), device_data_buffer_->eventID2eventName(ref_meta->getTimingContext()->getEventNumber()).c_str()) << " nTriggersChecked_: " << nTriggersChecked_;
        ASSERT_EQ( meta.context->getTimeStamp(), ref_meta->getTimingContext()->getTimeStamp())<< "TestError: Wrong WR stamp at nTriggersChecked_: " << nTriggersChecked_;
        ASSERT_EQ( meta.context->getTimeStamp(), ref_meta->getRelatedSampleStamp())<< "TestError: Wrong relatedSample stamp at nTriggersChecked_: " << nTriggersChecked_;

        timeSinceRefMeta = window_iterator->getTimeSinceRefMetaForEachSample();
        ASSERT_EQ (window_iterator->timeSinceRefMetaSanityCheck(timeSinceRefMeta, 0, true),0);
        ASSERT_EQ(timeSinceRefMeta.size(), n_data_written);

        for (std::size_t i=0,j = fakeSamplesChecked;i< timeSinceRefMeta.size();i++,j++ )
        {
            if(DEBUG_ENABLED)
            {
                std::cout << "fakeSampleStamps_[j] : " << fakeSampleStamps_[j] << std::endl;
                std::cout << "meta.trigger_stamp : " << meta.trigger_stamp << std::endl;
                std::cout << "measured timeSinceRefMeta["<<i<<"]      : " << timeSinceRefMeta[i] << std::endl;
                std::cout << "expected timeSinceRefMeta["<<j<<"]      : " << (fakeSampleStamps_[j] - meta.trigger_stamp)/1000000000. << std::endl;
            }
            ASSERT_NEAR((fakeSampleStamps_[j] - meta.trigger_stamp)/1000000000., timeSinceRefMeta[i], FLOAT_COMPARISON_TOLERANCE ) << "Wrong timeSinceRefMeta at index: " << i;
            ASSERT_EQ(result1[i], j);
        }
        fakeSamplesChecked += n_data_written;
    }
}

TEST_F(CircularBufferManagerBaseTestFixture, digitizer_streaming)
{
    DataPushMode dataPushMode = DataPushMode::CONTINOUS_MULTI_SAMPLES;
    std::size_t size_buffer = 1000;
    createCircularBufferManager(float(100), size_buffer,100, 200, ClientNotificationType::STREAMING, dataPushMode);
    MaxClientUpdateFrequencyType update_frequency_hz = 1;

    // 10 sequences should generate a buffer-rollover
    for (uint32_t seq_index = 0; seq_index < 10; seq_index ++)
        checkSequenceStreaming(4712, update_frequency_hz, dataPushMode);
    ASSERT_TRUE (circular_buffer_->n_data_pushed_total() > size_buffer); // make sure we did at least one roundtrip on the circular buffer
}

// FIXME: Stopped timing seems to work fine in roduction, but for some reason this test fails .. to be checked
//TEST_F(CircularBufferManagerBaseTestFixture, digitizer_streaming_timing_stops)
//{
//    DataPushMode dataPushMode = DataPushMode::CONTINOUS_MULTI_SAMPLES;
//    std::size_t size_buffer = 1000;
//    createCircularBufferManager(float(100), size_buffer,100, 200, ClientNotificationType::STREAMING, dataPushMode);
//    MaxClientUpdateFrequencyType update_frequency_hz = 1;
//
//    // 10 sequences should generate a buffer-rollover ... with timing
//    for (uint32_t seq_index = 0; seq_index < 20; seq_index ++)
//        checkSequenceStreaming(seq_index, update_frequency_hz, dataPushMode);
//
//    // Another 9 sequences (900samples) without timing .. we expect the latest ref-trigger to be used
//    // We disable the timeSinceRefMetaSanityCheck during the transition, since it is expected that both timings are not in sync.
//    for (uint32_t seq_index = 0; seq_index < 9; seq_index ++)
//        checkSequenceStreaming_noTiming(update_frequency_hz, true, false);
//
//    // Another sequence (100samples) without timing .. now all samples with timing got overwritten --> no ref trigger an more
//    checkSequenceStreaming_noTiming(update_frequency_hz, false, false);
//
//    // timing resumes
//    for (uint32_t seq_index = 0; seq_index < 10; seq_index ++)
//        checkSequenceStreaming(seq_index, update_frequency_hz, dataPushMode);
//}

TEST_F(CircularBufferManagerBaseTestFixture, digitizer_streaming_no_timing_at_all)
{
    DataPushMode dataPushMode = DataPushMode::CONTINOUS_MULTI_SAMPLES;
    std::size_t size_buffer = 1000;
    createCircularBufferManager(float(100), size_buffer,100, 200, ClientNotificationType::STREAMING, dataPushMode);
    MaxClientUpdateFrequencyType update_frequency_hz = 1;

    // Another 20 sequences without timing
    for (uint32_t seq_index = 0; seq_index < 12; seq_index ++)
        checkSequenceStreaming_noTiming(update_frequency_hz, false, true);
}

// FIXME: FULL_SEQ tests to be modified according to latest changes:
// - Use pattern with GAP_START (see simulation)
// - Tolerate 'same sample twice' --> The last sample of the current seq. can be the first sample of the next sequence
//TEST_F(CircularBufferManagerBaseTestFixture, digitizer_full_sequence)
//{
//    DataPushMode dataPushMode = DataPushMode::CONTINOUS_MULTI_SAMPLES;
//    std::size_t size_buffer = 1000;
//    createCircularBufferManager(float(100), size_buffer,100, 200, ClientNotificationType::FULL_SEQUENCE, dataPushMode);
//
//    injectFakeSequence1_streaming(4711, dataPushMode);
//    injectFakeSequence1_streaming(4712, dataPushMode);
//
//    checkSequenceFullSeq(4711, dataPushMode);
//    //checkSequenceFullSeq(4712, dataPushMode);
//    //checkSequenceFullSeq(4712, dataPushMode);
//}

TEST_F(CircularBufferManagerBaseTestFixture, digitizer_triggered)
{
    DataPushMode dataPushMode = DataPushMode::DISJUNCT_MULTI_SAMPLES;
    preTriggerSamples_ = 10;
    postTriggerSamples_ = 100;
    std::size_t size_buffer = 1000;
    createCircularBufferManager(float(10000), size_buffer,100, 200, ClientNotificationType::TRIGGERED, dataPushMode);
    buffer_manager_->setTriggerSamples(preTriggerSamples_, postTriggerSamples_);

    checkSequenceTriggered(4712);
    checkSequenceTriggered(4711);
    ASSERT_TRUE (circular_buffer_->n_data_pushed_total() > size_buffer); // make sure we did at least one roundtrip on the circular buffer
}

TEST_F(CircularBufferManagerBaseTestFixture, digitizer_triggered_no_pre_samples)
{
    DataPushMode dataPushMode = DataPushMode::DISJUNCT_MULTI_SAMPLES;
    preTriggerSamples_ = 0;
    postTriggerSamples_ = 100;
    std::size_t size_buffer = 1000;
    createCircularBufferManager(float(10000), size_buffer,100, 200, ClientNotificationType::TRIGGERED, dataPushMode);
    buffer_manager_->setTriggerSamples(preTriggerSamples_, postTriggerSamples_);

    checkSequenceTriggered(4712);
    checkSequenceTriggered(4711);
    ASSERT_TRUE (circular_buffer_->n_data_pushed_total() > size_buffer); // make sure we did at least one roundtrip on the circular buffer
}

// Skipped for now .. we rely on getting the same number of trigger events and trigger tags .. to be fixed on the Digitizer Side
//TEST_F(CircularBufferManagerBaseTestFixture, digitizer_triggered_no_context_match)
//{
//    DataPushMode dataPushMode = DataPushMode::DISJUNCT_MULTI_SAMPLES;
//    preTriggerSamples_ = 1000;
//    postTriggerSamples_ = 10000;
//    std::size_t size_buffer = 100000;
//
//    createCircularBufferManager(float(samp_rate_1kHz), size_buffer,100, 200, ClientNotificationType::TRIGGERED, dataPushMode);
//
//    buffer_manager_->setTriggerSamples(preTriggerSamples_, postTriggerSamples_);
//    buffer_manager_->setTriggerMatchingTolerance(1000);
//    random_measurement_jitter_min_ns_ = 1001;
//    random_measurement_jitter_max_ns_ = 1002;
//    int64_t sampleTosampleDistance_ns = buffer_manager_->getSampleToSampleDistance() *  1000000000;
//    int64_t preTriggerTime_ns = preTriggerSamples_ * sampleTosampleDistance_ns;
//    int64_t postTriggerTime_ns = postTriggerSamples_ * sampleTosampleDistance_ns;
//    injectFakeEventWithTriggerData("CMD_SEQ_START#257", 4711, preTriggerTime_ns, postTriggerTime_ns);
//    CircularWindowIterator* window_iterator = buffer_manager_->getTriggerDataWindow("CMD_SEQ_START");
//    ASSERT_EQ(window_iterator->getWindowStatus(), AcquisitionStatus::TRIGGER_CONTEXT_MATCHING_NO_MATCH);
//}

// Skipped for now .. we rely on getting the same number of trigger events and trigger tags .. to be fixed on the Digitizer Side
//TEST_F(CircularBufferManagerBaseTestFixture, digitizer_triggered_multi_context_match)
//{
//    DataPushMode dataPushMode = DataPushMode::DISJUNCT_MULTI_SAMPLES;
//    preTriggerSamples_ = 1000;
//    postTriggerSamples_ = 10000;
//    std::size_t size_buffer = 100000;
//
//    createCircularBufferManager(float(samp_rate_10kHz), size_buffer,100, 200, ClientNotificationType::TRIGGERED, dataPushMode);
//
//    buffer_manager_->setTriggerSamples(preTriggerSamples_, postTriggerSamples_);
//    buffer_manager_->setTriggerMatchingTolerance(1000000);
//    random_measurement_jitter_min_ns_ = 0;
//    random_measurement_jitter_max_ns_ = 100;
//
//    std::size_t samples = preTriggerSamples_ + postTriggerSamples_;
//    int64_t sampleTosampleDistance_ns = buffer_manager_->getSampleToSampleDistance() *  1000000000;
//
//    for( std::size_t samp_count = 0; samp_count < samples; samp_count++)
//    {
//        if( samp_count == 999  )
//            injectFakeEvent(fakeClock, "CMD_SEQ_START#257", 4711);
//
//        if( samp_count == 1001  )
//            injectFakeEvent(fakeClock, "CMD_SEQ_START#257", 4711);
//
//        if( samp_count == preTriggerSamples_  )//trigger tag on first post trigger sample
//            injectFakeEvent(fakeClock, "CMD_SEQ_START#257", 4711);
//
//        fakeSampleStamps_.push_back(fakeClock);
//        //std::cout << "fakeSampleStamps_["<<fakeSampleOffset_<<"]: " << fakeSampleStamps_[fakeSampleOffset_] <<  " timestamp: " << fakeClock << std::endl;
//        fakeSampleOffset_++;
//        fakeClock += sampleTosampleDistance_ns;
//    }
//    pushData();
//
//
//    CircularWindowIterator* window_iterator = buffer_manager_->getTriggerDataWindow("CMD_SEQ_START");
//    ASSERT_EQ(window_iterator->getWindowStatus(), AcquisitionStatus::TRIGGER_CONTEXT_MATCHING_MULTIPLE_MATCHES);
//}

TEST_F(CircularBufferManagerBaseTestFixture, power_supply_MIL_Streaming)
{
    std::size_t size_buffer = 100;
    DataPushMode dataPushMode = DataPushMode::DISJUNCT_SINGLE_SAMPLES;
    createCircularBufferManager(float(100), size_buffer,100, 200, ClientNotificationType::STREAMING, dataPushMode);
    MaxClientUpdateFrequencyType update_frequency_hz = 1;

    for (uint32_t seq_index = 0; seq_index < 10; seq_index ++)
        checkSequenceStreaming(4712, update_frequency_hz, dataPushMode);
    ASSERT_TRUE (circular_buffer_->n_data_pushed_total() > size_buffer); // make sure we did at least one roundtrip on the circular buffer
}

TEST_F(CircularBufferManagerBaseTestFixture, power_supply_MIL_Streaming_no_timing_at_all)
{
    std::size_t size_buffer = 1000;
    DataPushMode dataPushMode = DataPushMode::DISJUNCT_SINGLE_SAMPLES;
    createCircularBufferManager(float(100), size_buffer,100, 200, ClientNotificationType::STREAMING, dataPushMode);
    MaxClientUpdateFrequencyType update_frequency_hz = 1;

    // Another 20 sequences without timing
    for (uint32_t seq_index = 0; seq_index < 12; seq_index ++)
        checkSequenceStreaming_noTiming(update_frequency_hz, false, true);
}

// FIXME: FULL_SEQ tests to be modified according to latest changes:
// - Use pattern with GAP_START (see simulation)
// - Tolerate 'same sample twice' --> The last sample of the current seq. can be the first sample of the next sequence
//TEST_F(CircularBufferManagerBaseTestFixture, power_supply_full_sequence)
//{
//    std::size_t size_buffer = 40;
//    DataPushMode dataPushMode = DataPushMode::DISJUNCT_SINGLE_SAMPLES;
//    createCircularBufferManager(float(100), size_buffer,100, 200, ClientNotificationType::FULL_SEQUENCE, dataPushMode);
//
//    //buffer_manager_->setLogLevelFullSequence(LogLevel::TRACE);
//
//    // TODO: Not clear why we first need to run some default sequences to have the test pass
//    injectFakeSequence1_streaming(4711, dataPushMode);
//    injectFakeSequence1_streaming(4711, dataPushMode);
//    injectFakeSequence1_streaming(4711, dataPushMode);
//
//    checkSequenceFullSeq(4711, dataPushMode);
//    checkSequenceFullSeq(4712, dataPushMode);
//    checkSequenceFullSeq(4712, dataPushMode);
//
//    ASSERT_TRUE (circular_buffer_->n_data_pushed_total() > size_buffer); // make sure we did at least one roundtrip on the circular buffer
//
//    // Now lets just add two new meta items without adding new samples
//    // That should not lead to a new sequence window (this scenario actually happens during operation, e.g. in the gap)
//    wait(530);
//    injectFakeEvent(fakeClock, "CMD_SEQ_START#257", 1);
//    wait(1);
//    injectFakeEvent(fakeClock, "CMD_SEQ_START#257", 2);
//    ASSERT_THROW(
//            buffer_manager_->getFullSequenceDataWindow (prevSeqStartEvent_);
//            ,std::exception);
//    ASSERT_FALSE(data_ready_flagged_);
//}

// FIXME: FULL_SEQ tests to be modified according to latest changes:
// - Use pattern with GAP_START (see simulation)
// - Tolerate 'same sample twice' --> The last sample of the current seq. can be the first sample of the next sequence
//TEST_F(CircularBufferManagerBaseTestFixture, power_supply_full_sequence_aborted)
//{
//    std::size_t size_buffer = 40;
//    DataPushMode dataPushMode = DataPushMode::DISJUNCT_SINGLE_SAMPLES;
//    createCircularBufferManager(float(100), size_buffer,100, 200, ClientNotificationType::FULL_SEQUENCE, dataPushMode);
//
//    injectFakeSequence1_streaming(4711, dataPushMode);
//    injectFakeSequence1_streaming(4711, dataPushMode);
//
//    checkSequenceFullSeq(4711, dataPushMode);
//    checkSequenceFullSeq(4712, dataPushMode);
//
//    // After 10 command events, the sequence should be registered as "aborted" and
//    for(int i= 0; i<15;i++)
//    {
//        wait(5);
//        pushSingleSampleWithMeta();
//        wait(5);
//        injectFakeEvent(fakeClock, "EVT_GAP_START#258", 1);
//    }
//    ASSERT_TRUE(data_ready_flagged_);
//    ASSERT_NO_THROW(
//            buffer_manager_->getFullSequenceDataWindow (prevSeqStartEvent_);
//            );
//}

// TODO: Different client update rates, different sample rates, etc. etc.

// TODO: Test as well smaller methods instead of testing "EVERYTHING"

} // end namespace
