/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <test/CircularBufferBaseTestFixture.h>

#include <cabad/CircularBuffer.h>

namespace cabad
{

TEST_F(CircularBufferBaseTestFixture, BeginEnd)
{
    CircularBuffer<int> buffer(10);
    ASSERT_NO_THROW(
            ASSERT_EQ((long unsigned)10, buffer.pEnd()->index()); // needed for container.end() (points outside of container )
            ASSERT_EQ((long unsigned)0, buffer.pBegin()->index());
    );
}

TEST_F(CircularBufferBaseTestFixture, reset)
{
    CircularBuffer<int> buffer(10);
    for(int i=0;i<10;i++)
        buffer.push(i);
    std::shared_ptr<CircularBufferBase::iterator> iter = buffer.getReadIterator(8);
    iter->reset_to(4);
    ASSERT_EQ(iter->index(), (long unsigned)4);
    iter->reset_to(12);
    ASSERT_EQ(iter->index(), (long unsigned)2);
    iter->reset_to(0);
    ASSERT_EQ(iter->index(), (long unsigned)0);
    iter->reset_to(-1);
    ASSERT_EQ(iter->index(), (long unsigned)9);
    iter->reset_to(-2);
    ASSERT_EQ(iter->index(), (long unsigned)8);
    iter->reset_to(-9);
    ASSERT_EQ(iter->index(), (long unsigned)1);
    iter->reset_to(-10);
    ASSERT_EQ(iter->index(), (long unsigned)0);
    iter->reset_to(-11);
    ASSERT_EQ(iter->index(), (long unsigned)9);
}

TEST_F(CircularBufferBaseTestFixture, OperatorIncrement)
{
    CircularBuffer<int> buffer(10);
    for(int i=0;i<10;i++)
        buffer.push(i);
    CircularBuffer<int>::concrete_iterator iter = buffer.begin();
    for(int i=0;i<10;i++)
    {
        ASSERT_EQ(iter.index(), (long unsigned)i);
        ASSERT_EQ(*iter, i);
        ++iter;
    }
    ASSERT_EQ(iter.index(), (long unsigned)0);
    ASSERT_EQ(*iter, 0);
}

TEST_F(CircularBufferBaseTestFixture, OperatorDecrement)
{
    CircularBuffer<int> buffer(10);
    for(int i=0;i<10;i++)
        buffer.push(i);
    CircularBuffer<int>::concrete_iterator iter = buffer.begin();

    ASSERT_EQ(iter.index(), (long unsigned)0);
    ASSERT_EQ(*iter, 0);
    --iter;
    for(int i=9;i>0;i--)
    {
        ASSERT_EQ(iter.index(), (long unsigned)i);
        ASSERT_EQ(*iter, i);
        --iter;
    }
}

TEST_F(CircularBufferBaseTestFixture, OperatorIncrementEqual)
{
    CircularBuffer<int> buffer(10);
    std::shared_ptr<CircularBufferBase::iterator> iter = buffer.getReadIterator(8);
    (*iter)+=8;
    ASSERT_EQ(iter->index(), (long unsigned)6);
    (*iter)+=16;
    ASSERT_EQ(iter->index(), (long unsigned)2);
    (*iter)+=8;
    ASSERT_EQ(iter->index(), (long unsigned)0);
}

TEST_F(CircularBufferBaseTestFixture, OperatorDecrementEqual)
{
    CircularBuffer<int> buffer(10);
    std::shared_ptr<CircularBufferBase::iterator> iter = buffer.getReadIterator(2);
    (*iter)-=6;
    ASSERT_EQ(iter->index(), (long unsigned)6);
    (*iter)-=16;
    ASSERT_EQ(iter->index(), (long unsigned)0);
    (*iter)-=8;
    ASSERT_EQ(iter->index(), (long unsigned)2);
}

TEST_F(CircularBufferBaseTestFixture, assigment)
{
    CircularBuffer<int> buffer(10);
    std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(8);
    std::shared_ptr<CircularBufferBase::iterator> iter2 = buffer.getReadIterator(5);
    *iter2 = *iter1;

    ASSERT_TRUE(iter1->index() == iter2->index());
    ASSERT_TRUE(iter1->getCircularSize() == iter2->getCircularSize());
}

TEST_F(CircularBufferBaseTestFixture, comparison)
{
    CircularBuffer<int> buffer1(10);
    std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer1.getReadIterator(8);
    std::shared_ptr<CircularBufferBase::iterator> iter2 = buffer1.getReadIterator(8);
    std::shared_ptr<CircularBufferBase::iterator> iter3 = buffer1.getReadIterator(7);
    CircularBuffer<int> buffer2(15);
    std::shared_ptr<CircularBufferBase::iterator> iter4 = buffer2.getReadIterator(8);

    ASSERT_TRUE(*iter1 == *iter2);
    ASSERT_FALSE(*iter1 == *iter3);
    ASSERT_FALSE(*iter1 == *iter4);

    ASSERT_FALSE(*iter1 != *iter2);
    ASSERT_TRUE(*iter1 != *iter3);
    ASSERT_TRUE(*iter1 != *iter4);

    ASSERT_TRUE(*iter1 == *iter2);
}

TEST_F(CircularBufferBaseTestFixture, distanceTillEnd)
{
    CircularBuffer<int> buffer(10);
    std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(8);
    std::shared_ptr<CircularBufferBase::iterator> iter2 = buffer.getReadIterator(9);
    std::shared_ptr<CircularBufferBase::iterator> iter3 = buffer.getReadIterator(0);
    ASSERT_EQ(iter1->distanceTillEnd(),(long unsigned)1);
    ASSERT_EQ(iter2->distanceTillEnd(),(long unsigned)0);
    ASSERT_EQ(iter3->distanceTillEnd(),(long unsigned)9);
}

TEST_F(CircularBufferBaseTestFixture, isBetween)
{
    CircularBuffer<int> bufferSmall(10);
    std::shared_ptr<CircularBufferBase::iterator> iterSmall = bufferSmall.getReadIterator(5);
    {
		std::shared_ptr<CircularBufferBase::iterator> iter0 = bufferSmall.getReadIterator(0);
		std::shared_ptr<CircularBufferBase::iterator> iter5 = bufferSmall.getReadIterator(5);
		std::shared_ptr<CircularBufferBase::iterator> iter8 = bufferSmall.getReadIterator(8);
		ASSERT_TRUE(iter8->isBetween(*iter5,*iter0));
    }

    CircularBuffer<int> buffer(100);
    std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(40);
    std::shared_ptr<CircularBufferBase::iterator> iter2 = buffer.getReadIterator(39);
    std::shared_ptr<CircularBufferBase::iterator> iter3 = buffer.getReadIterator(41);
    std::shared_ptr<CircularBufferBase::iterator> iter4 = buffer.getReadIterator(42);
    std::shared_ptr<CircularBufferBase::iterator> iter5 = buffer.getReadIterator(42);

    ASSERT_FALSE(iter1->isBetween(*iterSmall,*iter2)); // 40 is not between  5 and 39
    ASSERT_TRUE(iter1->isBetween(*iter2,*iter3));      // 40 is between     39 and 41
    ASSERT_TRUE(iter1->isBetween(*iter2,*iter1));      // 40 is between     39 and 40
    ASSERT_FALSE(iter1->isBetween(*iter3,*iter2));     // 40 is not between 41 and 39 (rollover)
    ASSERT_TRUE(iter4->isBetween(*iter3,*iter2));      // 42 is between     41 and 39 (rollover)
    ASSERT_FALSE(iter4->isBetween(*iter2,*iter3));     // 42 is not between 39 and 41
    ASSERT_TRUE(iter1->isBetween(*iter1,*iter1));      // 40 is between     40 and 40
    ASSERT_TRUE(iter4->isBetween(*iter1,*iter1));      // 42 is between     40 and 40 (rollover)
}

TEST_F(CircularBufferBaseTestFixture, distance_to)
{
    {
        CircularBuffer<int> buffer(10);
        std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(1);
        std::shared_ptr<CircularBufferBase::iterator> iter2 = buffer.getReadIterator(1);
        ASSERT_EQ(iter1->distance_to(*iter2), (long unsigned)0);
    }
    {
        CircularBuffer<int> buffer(10);
        std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(1);
        std::shared_ptr<CircularBufferBase::iterator> iter2 = buffer.getReadIterator(3);
        ASSERT_EQ(iter1->distance_to(*iter2), (long unsigned)2);
    }
    {
        CircularBuffer<int> buffer(10);
        std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(1);
        std::shared_ptr<CircularBufferBase::iterator> iter2 = buffer.getReadIterator(0);
        ASSERT_EQ(iter1->distance_to(*iter2), (long unsigned)9);
    }
}

TEST_F(CircularBufferBaseTestFixture, prev_index)
{
    CircularBuffer<int> buffer(10);
    std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(0);
    std::shared_ptr<CircularBufferBase::iterator> iter2 = buffer.getReadIterator(5);
    std::shared_ptr<CircularBufferBase::iterator> iter3 = buffer.getReadIterator(9);
    ASSERT_EQ(iter1->prev_index(), (long unsigned)9);
    ASSERT_EQ(iter2->prev_index(), (long unsigned)4);
    ASSERT_EQ(iter3->prev_index(), (long unsigned)8);
}

TEST_F(CircularBufferBaseTestFixture, next)
{
    CircularBuffer<int> buffer(10);
    std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(0);
    std::shared_ptr<CircularBufferBase::iterator> iter2 = buffer.getReadIterator(5);
    std::shared_ptr<CircularBufferBase::iterator> iter3 = buffer.getReadIterator(9);
    iter1 = buffer.next(*iter1);
    iter2 = buffer.next(*iter2);
    iter3 = buffer.next(*iter3);
    ASSERT_EQ(iter1->index(), (long unsigned)1);
    ASSERT_EQ(iter2->index(), (long unsigned)6);
    ASSERT_EQ(iter3->index(), (long unsigned)0);
}

TEST_F(CircularBufferBaseTestFixture, prev)
{
    CircularBuffer<int> buffer(10);
    std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(0);
    std::shared_ptr<CircularBufferBase::iterator> iter2 = buffer.getReadIterator(5);
    std::shared_ptr<CircularBufferBase::iterator> iter3 = buffer.getReadIterator(9);
    iter1 = buffer.prev(*iter1);
    iter2 = buffer.prev(*iter2);
    iter3 = buffer.prev(*iter3);
    ASSERT_EQ(iter1->index(), (long unsigned)9);
    ASSERT_EQ(iter2->index(), (long unsigned)4);
    ASSERT_EQ(iter3->index(), (long unsigned)8);
}

TEST_F(CircularBufferBaseTestFixture, getReadIterator)
{
    CircularBuffer<int> buffer(100);
    std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(0);
}

TEST_F(CircularBufferBaseTestFixture, getCopyOfWriteIterator)
{
    int someValue = 42;
    CircularBuffer<int> buffer(10);
    buffer.push(someValue);
    buffer.push(someValue);
    std::shared_ptr<CircularBufferBase::iterator> writeIter1 = buffer.getCopyOfWriteIterator();
    std::shared_ptr<CircularBufferBase::iterator> writeIter2 = buffer.getCopyOfWriteIterator();
    ASSERT_EQ(writeIter1->index(), (long unsigned)2);
    ASSERT_TRUE(&(*writeIter1) != &(*writeIter2));
}

TEST_F(CircularBufferBaseTestFixture, valid_distance_between)
{
    int someValue = 42;
    CircularBuffer<int> buffer(10);
    buffer.push(someValue);
    buffer.push(someValue);

    std::shared_ptr<CircularBufferBase::iterator> iter0 = buffer.getReadIterator(0);
    std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(1);

    std::shared_ptr<CircularBufferBase::iterator> iter2 = buffer.getReadIterator(2); // here is the write iterator (lets validate that)
    ASSERT_EQ( buffer.getCopyOfWriteIterator()->index(), (unsigned long)2);

    std::shared_ptr<CircularBufferBase::iterator> iter3 = buffer.getReadIterator(3);
    std::shared_ptr<CircularBufferBase::iterator> iter8 = buffer.getReadIterator(8);

    ASSERT_EQ(0,  buffer.valid_distance_between(*iter0,*iter0));
    ASSERT_EQ(1,  buffer.valid_distance_between(*iter0,*iter1));
    ASSERT_EQ(-8, buffer.valid_distance_between(*iter1,*iter3));
    ASSERT_EQ(5,  buffer.valid_distance_between(*iter3,*iter8));
    ASSERT_EQ(-5, buffer.valid_distance_between(*iter8,*iter3));
    ASSERT_EQ(3,  buffer.valid_distance_between(*iter8,*iter1));
    ASSERT_EQ(-3, buffer.valid_distance_between(*iter1,*iter8));
}

TEST_F(CircularBufferBaseTestFixture, getLatest)
{
    int someValue = 42;
    CircularBuffer<int> buffer(3);

    {
        std::shared_ptr<CircularBufferBase::iterator> iter = buffer.getLatest();
        ASSERT_EQ(iter->index(), (long unsigned)2);
        buffer.push(someValue);
    }
    {
        std::shared_ptr<CircularBufferBase::iterator> iter = buffer.getLatest();
        ASSERT_EQ(iter->index(), (long unsigned)0);
        buffer.push(someValue);
    }
    {
        std::shared_ptr<CircularBufferBase::iterator> iter = buffer.getLatest();
        ASSERT_EQ(iter->index(), (long unsigned)1);
        buffer.push(someValue);
    }
    {
        std::shared_ptr<CircularBufferBase::iterator> iter = buffer.getLatest();
        ASSERT_EQ(iter->index(), (long unsigned)2);
        buffer.push(someValue);
    }
}

} // end namespace
