/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/DataReadyQueue.h>
#include <gtest/gtest.h>

#include <test/Util.h>

#include <string>
#include <iostream>

namespace cabad
{

class DataReadyQueueFixture : public ::testing::Test
{

public:

    DataReadyQueueFixture()
    {
        dummy_notificationType_ = ClientNotificationType::FULL_SEQUENCE;
        dummy_sinkNames_.push_back("Sink1");
        dummy_sinkNames_.push_back("Sink2");
        dummy_updateFrequency_ = 10;
    }

    ClientNotificationType dummy_notificationType_;
    std::vector<std::string> dummy_sinkNames_;
    std::shared_ptr<const TimingContext> dummy_context_;
    MaxClientUpdateFrequencyType dummy_updateFrequency_;

    // Is called once for all tests of this fixture
    static void SetUpTestCase();

    //called on each test
    void SetUp();
    void TearDown();

};


void DataReadyQueueFixture::SetUpTestCase()
{
    setLogFunction(fake_log_function);
}
void DataReadyQueueFixture::SetUp()
{

}

void DataReadyQueueFixture::TearDown()
{

}
} // end namespace
