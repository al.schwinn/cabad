/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <test/CircularBufferTestFixture.h>

#include <cabad/CircularBuffer.h>

namespace cabad
{

std::size_t CircularBufferTestFixture::bufferSize_ = 10;
CircularBuffer<int> CircularBufferTestFixture::buffer_(bufferSize_);

TEST_F(CircularBufferTestFixture, Single_Iterator_BeginEnd)
{
    CircularBuffer<int> buffer(10);
    ASSERT_NO_THROW(
            ASSERT_EQ((long unsigned)10, buffer.end().index()); // needed for container.end() (points outside of container )
            ASSERT_EQ((long unsigned)0, buffer.begin().index());
    );
}

TEST_F(CircularBufferTestFixture, Single_Iterator_Pointers)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);

    CircularBuffer<int>::concrete_iterator iter = buffer.begin();
    for (std::size_t i= 0; i<circular_size;i++)
    {
        *iter = int(i);
        ++iter;
    }
    iter = buffer.begin();
    for (int i= 0; i<(int)circular_size;i++)
    {
        ASSERT_EQ(i, *iter);
        ++iter;
    }
}

TEST_F(CircularBufferTestFixture, Single_Iterator_assigment)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);

    CircularBuffer<int>::concrete_iterator iter1 = buffer.begin();
    CircularBuffer<int>::concrete_iterator iter2 = iter1;

    ++iter1;
    ++iter2;
    *iter1 = int(4);
    ASSERT_EQ(4, *iter2);
    ++iter1;
    ASSERT_EQ(4, *iter2);
}

TEST_F(CircularBufferTestFixture, Single_braces_operator)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    for (std::size_t i= 0; i<circular_size;i++)
        buffer[i] = i;
    for (std::size_t i= 0; i<circular_size;i++)
        ASSERT_EQ((int)i, buffer[i]);

}

TEST_F(CircularBufferTestFixture, moveIterToLatest)
{
    int someValue = 23;
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    buffer.push(someValue);
    CircularBuffer<int>::concrete_iterator iter;
    buffer.moveIterToLatest(iter);

    ASSERT_EQ(someValue, *iter);
}

TEST_F(CircularBufferTestFixture, Single_push_scalar)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(0);
    std::shared_ptr<CircularBufferBase::iterator> iter2 = buffer.getReadIterator(1);
    std::shared_ptr<CircularBufferBase::iterator> iter3 = buffer.getReadIterator(9);
    for (int i= 0; i<30;i++)
        buffer.push(i);
    for (std::size_t i= 0; i<circular_size;i++)
        ASSERT_EQ(int(i+20), buffer[i]);

    buffer.moveIterToLatest(*iter1);
    buffer.moveIterToLatest(*iter2);
    buffer.moveIterToLatest(*iter3);

    ++(*iter2);
    ++(*iter3);
    ++(*iter3);

    int someValue = 123;
    buffer.push(someValue);
}

TEST_F(CircularBufferTestFixture, Single_push_array)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);

    // Not possible to push arrays bigger than the buffer itself
    ASSERT_THROW(
            int data[circular_size + 1];
            buffer.push(data, circular_size + 1);
            ,std::exception);

    { // regular copy

        std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(0);
        std::shared_ptr<CircularBufferBase::iterator> iter2 = buffer.getReadIterator(4);
        std::shared_ptr<CircularBufferBase::iterator> iter3 = buffer.getReadIterator(5);
        std::shared_ptr<CircularBufferBase::iterator> iter4 = buffer.getReadIterator(9);
        int data[5];
        for (int i= 0; i<5;i++)
            data[i] = i;
        buffer.push(data, 5);
        for (std::size_t i= 0; i<5;i++)
            ASSERT_EQ((int)i, buffer[i]);
    }

    { // buffer end reached. Will copy twice
        std::size_t circular_size = 12;
        CircularBuffer<int> buffer(circular_size);
        int data[8];
        for (int i= 0; i<8;i++)
            data[i] = i;
        buffer.push(data, 8);// 0-7 populated now
        std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(7);
        std::shared_ptr<CircularBufferBase::iterator> iter2 = buffer.getReadIterator(8);
        std::shared_ptr<CircularBufferBase::iterator> iter3 = buffer.getReadIterator(4);
        std::shared_ptr<CircularBufferBase::iterator> iter4 = buffer.getReadIterator(5);
        buffer.push(data, 8);// 8-11 populated now as well, 0-3 overwritten, write iter now at 4
        for (std::size_t i= 0; i<4;i++) // check the overwritten elements
            ASSERT_EQ((int)i+4, buffer[i]);
    }
}

TEST_F(CircularBufferTestFixture, copyDataWindow)
{
    int data[bufferSize_];
    for (std::size_t i= 0; i<bufferSize_;i++)
        data[i] = (int)i;
    std::shared_ptr<CircularBufferBase::iterator> start;
    std::shared_ptr<CircularBufferBase::iterator> end;
    buffer_.push(data, bufferSize_);

    { // window of single element
        std::size_t result_size = 1, expected_n_data_written = 0;
        int result[result_size];
        start = buffer_.getReadIterator(2);
        end = buffer_.getReadIterator(2);
        buffer_.copyDataWindow(*start, *end, &result[0], result_size, expected_n_data_written);
        ASSERT_EQ(result_size, expected_n_data_written);
        ASSERT_EQ(2, result[0]);
    }

    { // window does not cross buffer_.end()
        std::size_t result_size = 5, expected_n_data_written = 0;
        int result[result_size];
        start = buffer_.getReadIterator(0);
        end = buffer_.getReadIterator(4);
		buffer_.copyDataWindow(*start, *end, &result[0], result_size, expected_n_data_written);
        ASSERT_EQ(result_size, expected_n_data_written);
        for (std::size_t i= 0; i<result_size;i++)
            ASSERT_EQ((int)i, result[i]);
    }

    { // window size == buffer size
        std::size_t result_size = bufferSize_, expected_n_data_written = 0;
        int result[result_size];
        start = buffer_.getReadIterator(0);
        end = buffer_.getReadIterator(9);
        buffer_.copyDataWindow(*start, *end, &result[0], result_size, expected_n_data_written);
        ASSERT_EQ(result_size, expected_n_data_written);
        for (std::size_t i= 0; i<result_size;i++)
            ASSERT_EQ((int)i, result[i]);
    }

    { // window crosses buffer_.end()
        std::size_t result_size = 7, expected_n_data_written = 0;
        int result[result_size];
        start = buffer_.getReadIterator(5);
        end = buffer_.getReadIterator(1);
        buffer_.copyDataWindow(*start, *end, &result[0], result_size, expected_n_data_written);
        ASSERT_EQ(result_size, expected_n_data_written);
        ASSERT_EQ(5, result[0]);
        ASSERT_EQ(6, result[1]);
        ASSERT_EQ(7, result[2]);
        ASSERT_EQ(8, result[3]);
        ASSERT_EQ(9, result[4]);
        ASSERT_EQ(0, result[5]);
        ASSERT_EQ(1, result[6]);
    }

    { // window crosses buffer_.end() + window size == buffer size
        std::size_t result_size = bufferSize_, expected_n_data_written = 0;
        int result[result_size];
        start = buffer_.getReadIterator(5);
        end = buffer_.getReadIterator(4);
        buffer_.copyDataWindow(*start, *end, &result[0], result_size, expected_n_data_written);
        ASSERT_EQ(result_size, expected_n_data_written);
        ASSERT_EQ(5, result[0]);
        ASSERT_EQ(6, result[1]);
        ASSERT_EQ(7, result[2]);
        ASSERT_EQ(8, result[3]);
        ASSERT_EQ(9, result[4]);
        ASSERT_EQ(0, result[5]);
        ASSERT_EQ(1, result[6]);
        ASSERT_EQ(2, result[7]);
        ASSERT_EQ(3, result[8]);
        ASSERT_EQ(4, result[9]);
    }

    { // reserved buffer to small
        std::size_t result_size = 1, expected_n_data_written = 0;
        int result[result_size];
        start = buffer_.getReadIterator(2);
        end = buffer_.getReadIterator(3);
        ASSERT_THROW(
            buffer_.copyDataWindow(*start, *end, &result[0], result_size, expected_n_data_written);
        ,std::exception);
        ASSERT_EQ((long unsigned)0, expected_n_data_written);
    }
}

} // end namespace
