/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/CircularBuffer.h>
#include <gtest/gtest.h>

#include <test/Util.h>

#include <string>
#include <iostream>

namespace cabad
{

class CircularBufferTestFixture : public ::testing::Test
{

public:

    static std::size_t bufferSize_;
    static CircularBuffer<int> buffer_;

    CircularBufferTestFixture()
    {

    }

    static void getDataCallback(int* data, std::size_t data_size);

    // Is called once for all tests of this fixture
    static void SetUpTestCase();
};


void CircularBufferTestFixture::SetUpTestCase()
{
    setLogFunction(fake_log_function);
}

} // end namespace
