/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <test/CircularWindowIteratorTestFixture.h>
#include <test/TimingContextMock.h>

#include <cabad/CircularWindowIterator.h>
#include <cabad/CircularBuffer.h>
#include <cabad/SampleMetaData.h>

#include <math.h>

namespace cabad
{

TEST_F(CircularWindowIteratorTestFixture, windowSize)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);

    MetaDataBuffer metabuffer(10, &(*device_data_buffer_));
    CircularWindowIterator window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              double(0.01));

    window.data_start_ = buffer.getReadIterator(0);
    window.data_end_ = buffer.getReadIterator(4);

    ASSERT_EQ(window.windowSize(), (std::size_t)5);
}

TEST_F(CircularWindowIteratorTestFixture, distanceRefMetaToWindow)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    std::shared_ptr<CircularBufferBase::iterator> refMetaSample = buffer.getReadIterator(0);

    MetaDataBuffer metabuffer(10, &(*device_data_buffer_));
    std::shared_ptr<SampleMetadata> new_meta( new SampleMetadata(refMetaSample, 0, 1234, 0));
    metabuffer.push(new_meta);

    CircularWindowIterator window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              double(0.01));

    window.data_start_ = buffer.getReadIterator(0);
    window.data_end_ = buffer.getReadIterator(4);

    ASSERT_THROW(
            window.distanceRefMetaToWindow(); // no ref trigger defined yet
            ,std::exception);
    window.setRefMeta(new_meta);

    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(0));
    ++(*refMetaSample);
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(-1));
    ++(*refMetaSample);
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(-2));
    ++(*refMetaSample);
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(-3));
    ++(*refMetaSample);
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(-4));
    ++(*refMetaSample);
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(5)); // ref trigger left window
    ++(*refMetaSample);
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(4));
    ++(*refMetaSample);
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(3));
    ++(*refMetaSample);
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(2));
    ++(*refMetaSample);
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(1));
    ++(*refMetaSample);
    ASSERT_EQ(window.distanceRefMetaToWindow(), int64_t(0));
}

TEST_F(CircularWindowIteratorTestFixture, updateMetaWindowAccordingToSampleWindow)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    std::shared_ptr<const TimingContext> inject(new TimingContextMock(2, 283));
    std::shared_ptr<CircularBufferBase::iterator> meta1_sample = buffer.getReadIterator(0);
    std::shared_ptr<CircularBufferBase::iterator> meta2_sample = buffer.getReadIterator(5);
    std::shared_ptr<CircularBufferBase::iterator> meta3_sample = buffer.getReadIterator(8);
    std::shared_ptr<CircularBufferBase::iterator> meta4_sample = buffer.getReadIterator(9);

    MetaDataBuffer metabuffer(10, &(*device_data_buffer_));
    std::shared_ptr<SampleMetadata> meta1( new SampleMetadata(meta1_sample, 0, 1, 0));
    std::shared_ptr<SampleMetadata> meta2_inject_event( new SampleMetadata(meta2_sample, 0, inject, 2, 0));
    std::shared_ptr<SampleMetadata> meta3( new SampleMetadata(meta3_sample, 0, 3, 0));
    std::shared_ptr<SampleMetadata> meta4( new SampleMetadata(meta4_sample, 0, 4, 0));
    metabuffer.push(meta1);
    metabuffer.push(meta2_inject_event);
    metabuffer.push(meta3);
    metabuffer.push(meta4);

    {   // Meta before/after data-window are not used
        CircularWindowIterator window("test",
                  &buffer,
                  &metabuffer,
                  false,
                  DataPushMode::CONTINOUS_MULTI_SAMPLES,
                  double(0.01));

        window.data_start_ = buffer.getReadIterator(2);
        window.data_end_ = buffer.getReadIterator(8);

        ASSERT_NO_THROW(
                window.updateMetaWindowAccordingToSampleWindow(true);
        );

        //window.meta_start_->print();
        ASSERT_EQ(meta2_inject_event, window.meta_start_);
        ASSERT_EQ(meta3, window.meta_end_);
        ASSERT_EQ(meta2_inject_event, window.getRefMeta());
    }

    {   // meta_start_ and meta_end_ are unset if no meta was found in the window
        CircularWindowIterator window("test",
                  &buffer,
                  &metabuffer,
                  false,
                  DataPushMode::CONTINOUS_MULTI_SAMPLES,
                  double(0.01));

        window.data_start_ = buffer.getReadIterator(6);
        window.data_end_ = buffer.getReadIterator(7);

        // Previous end should be used to find a reference trigger
        window.meta_end_ = meta2_inject_event;

        ASSERT_NO_THROW(
                window.updateMetaWindowAccordingToSampleWindow(true);
        );

        ASSERT_FALSE(window.meta_start_);
        ASSERT_FALSE(window.meta_end_);
        ASSERT_EQ(meta2_inject_event, window.getRefMeta());

        // If no previous end was defined, just the previous reference-trigger should be reused
        window.meta_start_.reset();
        window.meta_end_.reset();

        ASSERT_NO_THROW(
                window.updateMetaWindowAccordingToSampleWindow(true);
        );

        ASSERT_FALSE(window.meta_start_);
        ASSERT_FALSE(window.meta_end_);
        ASSERT_EQ(meta2_inject_event, window.getRefMeta());
    }

}

TEST_F(CircularWindowIteratorTestFixture, moveDataWindowTo)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);

    MetaDataBuffer metabuffer(10, &(*device_data_buffer_));
    CircularWindowIterator window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              double(0.01));

    std::shared_ptr<CircularBufferBase::iterator> iter4 = buffer.getReadIterator(4);
    window.moveDataWindowTo(5, iter4);
    ASSERT_EQ((unsigned long)0, window.data_start_->index());
    ASSERT_EQ((unsigned long)4, window.data_end_->index());

    std::shared_ptr<CircularBufferBase::iterator> iter9 = buffer.getReadIterator(9);
    window.moveDataWindowTo(5, iter9);
    ASSERT_EQ((unsigned long)5, window.data_start_->index());
    ASSERT_EQ((unsigned long)9, window.data_end_->index());

    std::shared_ptr<CircularBufferBase::iterator> iter0 = buffer.getReadIterator(0);
    window.moveDataWindowTo(1, iter0);
    ASSERT_EQ((unsigned long)0, window.data_start_->index());
    ASSERT_EQ((unsigned long)0, window.data_end_->index());

    window.moveDataWindowTo(4, iter4);
    ASSERT_EQ((unsigned long)1, window.data_start_->index());
    ASSERT_EQ((unsigned long)4, window.data_end_->index());

    window.moveDataWindowTo(0, iter4);
    ASSERT_EQ((unsigned long)1, window.data_start_->index());
    ASSERT_EQ((unsigned long)4, window.data_end_->index());
}

TEST_F(CircularWindowIteratorTestFixture, copyConstructor)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    std::shared_ptr<CircularBufferBase::iterator> iter0 = buffer.getReadIterator(0);
    std::shared_ptr<CircularBufferBase::iterator> iter4 = buffer.getReadIterator(4);
    std::shared_ptr<CircularBufferBase::iterator> iter5 = buffer.getReadIterator(5);
    std::shared_ptr<CircularBufferBase::iterator> iter9 = buffer.getReadIterator(9);

    MetaDataBuffer metabuffer1(10, &(*device_data_buffer_));
    CircularWindowIterator window1("test",
              &buffer,
              &metabuffer1,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              double(0.01));

    window1.moveDataWindowTo(5,iter4); // window1: [0..4]
    MetaDataBuffer metabuffer2(10, &(*device_data_buffer_));
    CircularWindowIterator window2(&metabuffer2);
    window2 = window1; // window1: [0..4], window2: [0..4]
    ASSERT_EQ((unsigned long)0, window2.data_start_->index());
    ASSERT_EQ((unsigned long)4, window2.data_end_->index());

    window1.moveDataWindowTo(5,iter9); // window1: [5..9], window2: [0..4]
    ASSERT_EQ((unsigned long)5, window1.data_start_->index());
    ASSERT_EQ((unsigned long)9, window1.data_end_->index());
    ASSERT_EQ((unsigned long)0, window2.data_start_->index());
    ASSERT_EQ((unsigned long)4, window2.data_end_->index());
}

TEST_F(CircularWindowIteratorTestFixture, validation)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    std::shared_ptr<CircularBufferBase::iterator> iter4 = buffer.getReadIterator(4);
    std::shared_ptr<CircularBufferBase::iterator> iter8 = buffer.getReadIterator(8);

    MetaDataBuffer metabuffer(10, &(*device_data_buffer_));
    CircularWindowIterator window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              double(0.01));

    int some_data = 123;

    // Initially window is valid (points to buffer.end() with both iterators
    ASSERT_TRUE(window.valid());

    // First write, to make previous write iter differ from current write iter (otherwise the whole buffer will be invalid)
    buffer.push(some_data);

    window.moveDataWindowTo(3,iter4); // window has 3 samples: #2, #3 and #4
    window.validate();
    ASSERT_TRUE(window.valid());

    // write iter entered window
    buffer.push(some_data); // write iter now at #2
    window.validate();
    ASSERT_FALSE(window.valid());

    // write iter left window
    int data_array1[3] = { 3,4,5 };
    buffer.push(data_array1,3); // write iter now at #5
    window.validate();
    ASSERT_FALSE(window.valid());

    window.moveDataWindowTo(3,iter8); // window has 3 samples: #6 #7 #8
    window.validate();
    ASSERT_TRUE(window.valid()); // Movement makes window valid again

    // write itrator writes cross complete window
    int data_array2[5]  = { 6,7,8,9,0 };
    buffer.push(data_array2,5); // write iter now at #0

    window.validate();
    ASSERT_FALSE(window.valid());
}

TEST_F(CircularWindowIteratorTestFixture, isInWindow)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(1);
    std::shared_ptr<CircularBufferBase::iterator> iter2 = buffer.getReadIterator(2);
    std::shared_ptr<CircularBufferBase::iterator> iter3 = buffer.getReadIterator(3);
    std::shared_ptr<CircularBufferBase::iterator> iter4 = buffer.getReadIterator(4);
    std::shared_ptr<CircularBufferBase::iterator> iter5 = buffer.getReadIterator(5);

    MetaDataBuffer metabuffer(10, &(*device_data_buffer_));
    CircularWindowIterator window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              double(0.01));

    std::shared_ptr<CircularBufferBase::iterator> uninitialized;
    ASSERT_FALSE(window.isInWindow(uninitialized));

    window.moveDataWindowTo(3,iter4); // window has 3 samples: #2, #3 and #4
    ASSERT_FALSE(window.isInWindow(iter1));
    ASSERT_TRUE (window.isInWindow(iter2));
    ASSERT_TRUE (window.isInWindow(iter3));
    ASSERT_TRUE (window.isInWindow(iter4));
    ASSERT_FALSE(window.isInWindow(iter5));

    window.moveDataWindowTo(1,iter4); // window has 1 samples: #4
    ASSERT_FALSE(window.isInWindow(iter3));
    ASSERT_TRUE (window.isInWindow(iter4));
    ASSERT_FALSE(window.isInWindow(iter5));
}

TEST_F(CircularWindowIteratorTestFixture, not_used_yet)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(1);

    MetaDataBuffer metabuffer(10, &(*device_data_buffer_));
    CircularWindowIterator window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              double(0.01));

    ASSERT_TRUE(window.not_used_yet());
    window.moveDataWindowTo(1,iter1);
    ASSERT_FALSE(window.not_used_yet());
}

TEST_F(CircularWindowIteratorTestFixture, timeSinceRefMetaSanityCheck)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    std::shared_ptr<CircularBufferBase::iterator> refMetaSample = buffer.getReadIterator(0);
    std::vector<float> timeSinceRefMetaForEachSample;

    MetaDataBuffer metabuffer(10, &(*device_data_buffer_));
    int64_t refMetaStamp = 1234;
    std::shared_ptr<const TimingContext> timing_context(new TimingContextMock(refMetaStamp, 42));

    std::shared_ptr<SampleMetadata> new_meta(new SampleMetadata(refMetaSample, 0, timing_context, 0));
    metabuffer.push(new_meta);
    std::shared_ptr<SampleMetadata> refMeta = metabuffer.back();

    CircularWindowIterator window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              double(0.01));

    window.data_start_ = buffer.getReadIterator(0);
    window.data_end_ = buffer.getReadIterator(4);
    window.setRefMeta(refMeta);

    float *channeltimeSinceRefMetas = new float[window.windowSize()];
    window.setRefMeta(new_meta);

    ASSERT_THROW( // Throw if passed array is too short (instead of crash)
            window.fillTimeSinceRefMetaForEachSample(channeltimeSinceRefMetas, 2);
            ,std::exception);

    window.fillTimeSinceRefMetaForEachSample(channeltimeSinceRefMetas, window.windowSize());
    ASSERT_EQ (0, window.timeSinceRefMetaSanityCheck(timeSinceRefMetaForEachSample));

    delete []channeltimeSinceRefMetas;
}

TEST_F(CircularWindowIteratorTestFixture, timeSinceRefMetaSanityCheck_injected_ref_meta)
{
    // TODO
}

TEST_F(CircularWindowIteratorTestFixture, getTimeStampOfLastSample_before_window)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    std::shared_ptr<CircularBufferBase::iterator> refMetaSample = buffer.getReadIterator(2);
    std::vector<float> timeSinceRefMetaForEachSample;

    MetaDataBuffer metabuffer(10, &(*device_data_buffer_));
    int64_t refMetaStamp = 12340000000;
    double sample_sample_distance_sec = 0.01; // == 10000000ns
    std::shared_ptr<const TimingContext> timing_context(new TimingContextMock(refMetaStamp, 42));

    std::shared_ptr<SampleMetadata> new_meta(new SampleMetadata(refMetaSample, 0, timing_context, refMetaStamp));
    metabuffer.push(new_meta);
    std::shared_ptr<SampleMetadata> refMeta = metabuffer.back();

    CircularWindowIterator window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              sample_sample_distance_sec);

    window.data_start_ = buffer.getReadIterator(4);
    window.data_end_ = buffer.getReadIterator(7);
    window.setRefMeta(refMeta);

    float *channeltimeSinceRefMetas = new float[window.windowSize()];
    window.fillTimeSinceRefMetaForEachSample(channeltimeSinceRefMetas, window.windowSize());
//    for(std::size_t i= 0; i< window.windowSize();i++)
//    {
//        std::cout << "channeltimeSinceRefMetas["<< i <<"]:                       " << channeltimeSinceRefMetas[i] << std::endl;
//    }
    int64_t lastSampleStampNano = refMetaStamp + int64_t(round(channeltimeSinceRefMetas[window.windowSize()-1] * 1000000000));
    //std::cout << "lastSampleStampNano: " << lastSampleStampNano << std::endl;
    ASSERT_EQ (lastSampleStampNano, window.getTimeStampOfLastSample());

    delete []channeltimeSinceRefMetas;
}

TEST_F(CircularWindowIteratorTestFixture, getTimeStampOfLastSample_on_first_sample)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    std::shared_ptr<CircularBufferBase::iterator> refMetaSample = buffer.getReadIterator(0);
    std::vector<float> timeSinceRefMetaForEachSample;

    MetaDataBuffer metabuffer(10, &(*device_data_buffer_));
    int64_t refMetaStamp = 12340000000;
    double sample_sample_distance_sec = 0.01; // == 10000000ns
    std::shared_ptr<const TimingContext> timing_context(new TimingContextMock(refMetaStamp, 42));

    std::shared_ptr<SampleMetadata> new_meta(new SampleMetadata(refMetaSample, 0, timing_context, refMetaStamp));
    metabuffer.push(new_meta);
    std::shared_ptr<SampleMetadata> refMeta = metabuffer.back();

    CircularWindowIterator window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              sample_sample_distance_sec);

    window.data_start_ = buffer.getReadIterator(0);
    window.data_end_ = buffer.getReadIterator(4);
    window.setRefMeta(refMeta);

    float *channeltimeSinceRefMetas = new float[window.windowSize()];
    window.fillTimeSinceRefMetaForEachSample(channeltimeSinceRefMetas, window.windowSize());
//    for(std::size_t i= 0; i< window.windowSize();i++)
//    {
//        std::cout << "channeltimeSinceRefMetas["<< i <<"]:                       " << channeltimeSinceRefMetas[i] << std::endl;
//    }
    int64_t lastSampleStampNano = refMetaStamp + int64_t(round(channeltimeSinceRefMetas[window.windowSize()-1] * 1000000000));
    //std::cout << "lastSampleStampNano: " << lastSampleStampNano << std::endl;
    ASSERT_EQ (lastSampleStampNano, window.getTimeStampOfLastSample());

    delete []channeltimeSinceRefMetas;
}

TEST_F(CircularWindowIteratorTestFixture, getTimeStampOfLastSample_in_window)
{
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);
    std::shared_ptr<CircularBufferBase::iterator> refMetaSample = buffer.getReadIterator(3);
    std::vector<float> timeSinceRefMetaForEachSample;

    MetaDataBuffer metabuffer(10, &(*device_data_buffer_));
    int64_t refMetaStamp = 12340000000;
    double sample_sample_distance_sec = 0.01; // == 10000000ns
    std::shared_ptr<const TimingContext> timing_context(new TimingContextMock(refMetaStamp, 42));

    std::shared_ptr<SampleMetadata> new_meta(new SampleMetadata(refMetaSample, 0, timing_context, refMetaStamp));
    metabuffer.push(new_meta);
    std::shared_ptr<SampleMetadata> refMeta = metabuffer.back();

    CircularWindowIterator window("test",
              &buffer,
              &metabuffer,
              false,
              DataPushMode::CONTINOUS_MULTI_SAMPLES,
              sample_sample_distance_sec);

    window.data_start_ = buffer.getReadIterator(0);
    window.data_end_ = buffer.getReadIterator(4);
    window.setRefMeta(refMeta);

    float *channeltimeSinceRefMetas = new float[window.windowSize()];
    window.fillTimeSinceRefMetaForEachSample(channeltimeSinceRefMetas, window.windowSize());
//    for(std::size_t i= 0; i< window.windowSize();i++)
//    {
//        std::cout << "channeltimeSinceRefMetas["<< i <<"]:                       " << channeltimeSinceRefMetas[i] << std::endl;
//    }
    int64_t lastSampleStampNano = refMetaStamp + int64_t(round(channeltimeSinceRefMetas[window.windowSize()-1] * 1000000000));
    //std::cout << "lastSampleStampNano: " << lastSampleStampNano << std::endl;
    ASSERT_EQ (lastSampleStampNano, window.getTimeStampOfLastSample());

    delete []channeltimeSinceRefMetas;
}
} // end namespace
