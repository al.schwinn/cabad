/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/DataReadyManager.h>
#include <gtest/gtest.h>

#include <test/Util.h>

#include <string>
#include <iostream>

namespace cabad
{

class DataReadyManagerFixture : public ::testing::Test
{

public:

    DataReadyManagerFixture()
    {
        dummy_group_ = "dummy_notification_group";
    }

    static uint32_t n_allDataReadyCalled_;
    static void callbackAllDataReady (uint32_t data_id)
    {
        n_allDataReadyCalled_++;
    }

    // Is called once for all tests of this fixture
    static void SetUpTestCase();

    //called on each test
    void SetUp();
    void TearDown();

    std::string dummy_group_;

};

void DataReadyManagerFixture::SetUpTestCase()
{
    setLogFunction(fake_log_function);
    setTimeStampFunction(fake_clock_get_utc_now);
    fakeClock = START_TIME;
    n_allDataReadyCalled_ = 0;
}
void DataReadyManagerFixture::SetUp()
{

}

void DataReadyManagerFixture::TearDown()
{

}

} // end namespace
