/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/DeviceDataBufferBase.h>
#include <gtest/gtest.h>

#include <test/Util.h>

#include <string>
#include <iostream>

namespace cabad
{

class DeviceDataBufferBaseFixture : public ::testing::Test
{

public:

    std::vector<std::string> fesaConcreteEventNames_;
    std::string fesaDeviceName_;

    std::string fesaInternalPrefix_;
    std::string inject_event_;
    std::string seq_start_event_;
    std::string command_event_;

    DeviceDataBufferBaseFixture()
    {

    }

    // Is called once for all tests of this fixture
    static void SetUpTestCase();

    //called on each test
    void SetUp();
    void TearDown();

};

void DeviceDataBufferBaseFixture::SetUpTestCase()
{
    setLogFunction(fake_log_function);
}
void DeviceDataBufferBaseFixture::SetUp()
{
    fesaInternalPrefix_ = "MyLayer::";

    inject_event_ ="CMD_BEAM_INJECTION#283";
    seq_start_event_ ="CMD_SEQ_START#257";
    command_event_ = "EVT_COMMAND#255";

    fesaConcreteEventNames_.push_back(fesaInternalPrefix_ + inject_event_);
    fesaConcreteEventNames_.push_back(fesaInternalPrefix_ + seq_start_event_);
    fesaConcreteEventNames_.push_back(fesaInternalPrefix_ + command_event_);
    fesaDeviceName_ = "TestDevice";
}

void DeviceDataBufferBaseFixture::TearDown()
{

}
} // end namespace
