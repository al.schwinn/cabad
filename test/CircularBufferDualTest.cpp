/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <test/CircularBufferDualTestFixture.h>

#include <cabad/CircularBuffer.h>

namespace cabad
{

std::size_t CircularBufferDualTestFixture::bufferSize_ = 10;
CircularBufferDual<int,int> CircularBufferDualTestFixture::buffer_(bufferSize_);

TEST_F(CircularBufferDualTestFixture, Iterator_BeginEnd)
{
    CircularBufferDual<int,int> buffer(10);
    ASSERT_NO_THROW(
            ASSERT_EQ((long unsigned)10, buffer.end().index()); // needed for container.end() (points outside of container )
            ASSERT_EQ((long unsigned)0, buffer.begin().index());
    );
}

TEST_F(CircularBufferDualTestFixture, Iterator_Pointers)
{
    std::size_t circular_size = 10;
    CircularBufferDual<int,int> buffer(circular_size);

    CircularBufferDual<int,int>::concrete_iterator iter = buffer.begin();
    for (std::size_t i= 0; i<circular_size;i++)
    {
        iter.ref_type1() = (int)i;
        iter.ref_type2() = (int)i+10;
        ++iter;
    }
    iter = buffer.begin();
    for (std::size_t i= 0; i<circular_size;i++)
    {
        ASSERT_EQ((int)i, iter.ref_type1());
        ASSERT_EQ((int)i+10, iter.ref_type2());
        ++iter;
    }
}

TEST_F(CircularBufferDualTestFixture, Iterator_assigment)
{
    std::size_t circular_size = 10;
    CircularBufferDual<int,int> buffer(circular_size);

    CircularBufferDual<int,int>::concrete_iterator iter1 = buffer.begin();
    CircularBufferDual<int,int>::concrete_iterator iter2 = iter1;

    ++iter1;
    ++iter2;
    iter1.ref_type1() = int(4);
    iter1.ref_type2() = int(5);
    ASSERT_EQ(4, iter2.ref_type1());
    ASSERT_EQ(5, iter2.ref_type2());

    CircularBufferDual<int,int>::concrete_iterator iter3 = buffer.begin();
    CircularBufferDual<int,int>::concrete_iterator iter4 = iter3;

    ++iter3;

    ASSERT_NE(iter3.index(), iter4.index());
}

TEST_F(CircularBufferDualTestFixture, get_type)
{
    std::size_t circular_size = 10;
    CircularBufferDual<int,int> buffer(circular_size);
    for (std::size_t i= 0; i<circular_size;i++)
    {
        buffer.get_type1_ref(i) = i;
        buffer.get_type2_ref(i) = i+10;
    }
    for (std::size_t i= 0; i<circular_size;i++)
    {
        ASSERT_EQ((int)i, buffer.get_type1_ref(i));
        ASSERT_EQ((int)i+10, buffer.get_type2_ref(i));
    }

}


TEST_F(CircularBufferDualTestFixture, push_scalar)
{
    std::size_t circular_size = 10;
    CircularBufferDual<int,int> buffer(circular_size);
    std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(0);
    std::shared_ptr<CircularBufferBase::iterator> iter2 = buffer.getReadIterator(1);
    std::shared_ptr<CircularBufferBase::iterator> iter3 = buffer.getReadIterator(9);
    for (int i=0; i<30;i++)
    {
        int j = i+10;
        buffer.push(i,j);
    }
    for (std::size_t i= 0; i<circular_size;i++)
    {
        ASSERT_EQ(int(i+20), buffer.get_type1_ref(i));
        ASSERT_EQ(int(i+30), buffer.get_type2_ref(i));
    }
    buffer.moveIterToLatest(*iter1);
    buffer.moveIterToLatest(*iter2);
    buffer.moveIterToLatest(*iter3);

    ++(*iter2);
    ++(*iter3);
    ++(*iter3);

    int someValue = 123;
    buffer.push(someValue, someValue);
}

TEST_F(CircularBufferDualTestFixture, push_array)
{
        std::size_t circular_size = 10;
        CircularBufferDual<int,int> buffer(circular_size);

        // Not possible to push arrays bigger than the buffer itself
        ASSERT_THROW(
                int data1[circular_size + 1];
                int data2[circular_size + 1];
                buffer.push(data1, data2, circular_size + 1);
                ,std::exception);

    { // regular copy
        std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(0);
        std::shared_ptr<CircularBufferBase::iterator> iter2 = buffer.getReadIterator(4);
        std::shared_ptr<CircularBufferBase::iterator> iter3 = buffer.getReadIterator(5);
        std::shared_ptr<CircularBufferBase::iterator> iter4 = buffer.getReadIterator(9);
        int data1[5];
        int data2[5];
        for (int i= 0; i<5;i++)
        {
            data1[i] = i;
            data2[i] = i+10;
        }

        buffer.push(data1, data2, 5);
        for (std::size_t i= 0; i<5;i++)
        {
            ASSERT_EQ((int)i, buffer.get_type1_ref(i));
            ASSERT_EQ((int)i+10, buffer.get_type2_ref(i));
        }
    }

    { // buffer end reached. Will copy twice
        std::size_t circular_size = 12;
        CircularBufferDual<int,int> buffer(circular_size);
        int data1[8];
        int data2[8];
        for (int i= 0; i<8;i++)
        {
            data1[i] = i;
            data2[i] = i + 10;
        }
        buffer.push(data1, data2, 8);// 0-7 populated now
        std::shared_ptr<CircularBufferBase::iterator> iter1 = buffer.getReadIterator(7);
        std::shared_ptr<CircularBufferBase::iterator> iter2 = buffer.getReadIterator(8);
        std::shared_ptr<CircularBufferBase::iterator> iter3 = buffer.getReadIterator(4);
        std::shared_ptr<CircularBufferBase::iterator> iter4 = buffer.getReadIterator(5);
        buffer.push(data1, data2, 8);// 8-11 populated now as well, 0-3 overwritten, write iter now at 4
        for (std::size_t i= 0; i<4;i++) // check the overwritten elements
        {
            ASSERT_EQ((int)i + 4, buffer.get_type1_ref(i));
            ASSERT_EQ((int)i + 4 + 10, buffer.get_type2_ref(i));
        }
    }
}

TEST_F(CircularBufferDualTestFixture, copyDataWindow)
{
    int data1[bufferSize_];
    int data2[bufferSize_];
    for (std::size_t i= 0; i<bufferSize_;i++)
    {
        data1[i] = i;
        data2[i] = i + 10;
    }
    std::shared_ptr<CircularBufferBase::iterator> start;
    std::shared_ptr<CircularBufferBase::iterator> end;
    buffer_.push(data1, data2, bufferSize_);

    { // window of single element
        std::size_t result_size = 1, expected_n_data_written = 0;
        int result1[result_size], result2[result_size];
        start = buffer_.getReadIterator(2);
        end = buffer_.getReadIterator(2);
        buffer_.copyDataWindow(*start, *end, &result1[0], &result2[0], result_size, expected_n_data_written);
        ASSERT_EQ(result_size, expected_n_data_written);
        ASSERT_EQ(2, result1[0]);
        ASSERT_EQ(12, result2[0]);
    }

    { // window does not cross buffer_.end()
        std::size_t result_size = 5, expected_n_data_written = 0;
        int result1[result_size], result2[result_size];
        start = buffer_.getReadIterator(0);
        end = buffer_.getReadIterator(4);
        buffer_.copyDataWindow(*start, *end, &result1[0], &result2[0], result_size, expected_n_data_written);
        ASSERT_EQ(result_size, expected_n_data_written);
        for (std::size_t i= 0; i<5;i++)
        {
            ASSERT_EQ((int)i,    result1[i]);
            ASSERT_EQ((int)i+10, result2[i]);
        }
    }

    { // window size == buffer size
        std::size_t result_size = bufferSize_, expected_n_data_written = 0;
        int result1[result_size], result2[result_size];
        start = buffer_.getReadIterator(0);
        end = buffer_.getReadIterator(9);
        buffer_.copyDataWindow(*start, *end, &result1[0], &result2[0], result_size, expected_n_data_written);
        ASSERT_EQ(result_size, expected_n_data_written);
        for (std::size_t i= 0; i<10;i++)
        {
            ASSERT_EQ((int)i,    result1[i]);
            ASSERT_EQ((int)i+10, result2[i]);
        }
    }

    { // window crosses buffer_.end()
        std::size_t result_size = 7, expected_n_data_written = 0;
        int result1[result_size], result2[result_size];
        start = buffer_.getReadIterator(5);
        end = buffer_.getReadIterator(1);
        buffer_.copyDataWindow(*start, *end, &result1[0], &result2[0], result_size, expected_n_data_written);
        ASSERT_EQ(result_size, expected_n_data_written);
        for (std::size_t i= 0; i<5;i++)
        {
            ASSERT_EQ((int)i+ 5, result1[i]);
            ASSERT_EQ((int)i+15, result2[i]);
        }
        ASSERT_EQ((int)0, result1[5]);
        ASSERT_EQ((int)1, result1[6]);
        ASSERT_EQ((int)10, result2[5]);
        ASSERT_EQ((int)11, result2[6]);
    }

    { // window crosses buffer_.end() + window size == buffer size
        std::size_t result_size = bufferSize_, expected_n_data_written = 0;
        int result1[result_size], result2[result_size];
        start = buffer_.getReadIterator(5);
        end = buffer_.getReadIterator(4);
        buffer_.copyDataWindow(*start, *end, &result1[0], &result2[0], result_size, expected_n_data_written);
        ASSERT_EQ(result_size, expected_n_data_written);
        for (std::size_t i= 0; i<5;i++)
        {
            ASSERT_EQ((int)i+ 5, result1[i]);
            ASSERT_EQ((int)i+15, result2[i]);
        }
        ASSERT_EQ( (int)0, result1[5]);
        ASSERT_EQ( (int)1, result1[6]);
        ASSERT_EQ( (int)2, result1[7]);
        ASSERT_EQ( (int)3, result1[8]);
        ASSERT_EQ( (int)4, result1[9]);
        ASSERT_EQ((int)10, result2[5]);
        ASSERT_EQ((int)11, result2[6]);
        ASSERT_EQ((int)12, result2[7]);
        ASSERT_EQ((int)13, result2[8]);
        ASSERT_EQ((int)14, result2[9]);
    }
}

TEST_F(CircularBufferDualTestFixture, distance_to_write_iter)
{
    // Initially write iterator should be at 0
    ASSERT_EQ((long unsigned)0,buffer_.getWriteIterIndex());

    std::shared_ptr<CircularBufferBase::iterator> iter0 = buffer_.getReadIterator(0);
    std::shared_ptr<CircularBufferBase::iterator> iter4 = buffer_.getReadIterator(4);
    std::shared_ptr<CircularBufferBase::iterator> iter9 = buffer_.getReadIterator(9);

    ASSERT_EQ((long unsigned)0,buffer_.distance_to_write_iter(*iter0));
    ASSERT_EQ((long unsigned)6,buffer_.distance_to_write_iter(*iter4));
    ASSERT_EQ((long unsigned)1,buffer_.distance_to_write_iter(*iter9));


    // Now lets move it to the last element
    std::size_t dataSize = bufferSize_-1;
    int data1[dataSize];
    int data2[dataSize];
    for (std::size_t i= 0; i<dataSize;i++)
    {
        data1[i] = i;
        data2[i] = i + 10;
    }

    // Pushing on each element, so the write-iter should be located at 9 (last element)
    buffer_.push(data1, data2, dataSize);
    ASSERT_EQ((long unsigned)9,buffer_.getWriteIterIndex());

    ASSERT_EQ((long unsigned)9,buffer_.distance_to_write_iter(*iter0));
    ASSERT_EQ((long unsigned)5,buffer_.distance_to_write_iter(*iter4));
    ASSERT_EQ((long unsigned)0,buffer_.distance_to_write_iter(*iter9));
}


} // end namespace
