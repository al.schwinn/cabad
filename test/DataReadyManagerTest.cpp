/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <test/DataReadyManagerFixture.h>

#include <test/CircularBufferManagerMock.h>

namespace cabad
{

uint32_t DataReadyManagerFixture::n_allDataReadyCalled_ = 0;

TEST_F(DataReadyManagerFixture, getDataReadyHandler_no_forced_grouping)
{
    DataReadyManager notif_manager(callbackAllDataReady, 10);
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);

    CircularBufferManagerMock manager_A1("myBufferA1");
    CircularBufferManagerMock manager_A2("myBufferA2");
    CircularBufferManagerMock manager_B1("myBufferB1");
    CircularBufferManagerMock manager_B2("myBufferB2");
    CircularBufferManagerMock manager_C1("myBufferC1");
    CircularBufferManagerMock manager_C2("myBufferC2");
    CircularBufferManagerMock manager_C3("myBufferC3");

    uint32_t trigger_event_id_A = 123;
    uint32_t trigger_event_id_B = 55;
    uint32_t trigger_event_id_C = 47;

    std::vector<std::pair <DataReadyHandler*, CircularBufferManagerMock*> > handlers_and_managers;

    ASSERT_NO_THROW(
        // Group1
        handlers_and_managers.push_back(std::make_pair(notif_manager.getStreamingDataReadyHandler(dummy_group_, ClientNotificationType::STREAMING,10,100,&manager_A1), &manager_A1));
        handlers_and_managers.push_back(std::make_pair(notif_manager.getStreamingDataReadyHandler(dummy_group_,ClientNotificationType::STREAMING,10,100,&manager_A2),&manager_A2));

        // Group2
        handlers_and_managers.push_back(std::make_pair(notif_manager.getTriggeredDataReadyHandler(dummy_group_,ClientNotificationType::FULL_SEQUENCE,trigger_event_id_A,100,&manager_A1),&manager_A1)); // same buffer might provide streaming and full seq data
        handlers_and_managers.push_back(std::make_pair(notif_manager.getTriggeredDataReadyHandler(dummy_group_,ClientNotificationType::FULL_SEQUENCE,trigger_event_id_A,100,&manager_A2),&manager_A2));

        // Group3
        handlers_and_managers.push_back(std::make_pair(notif_manager.getTriggeredDataReadyHandler(dummy_group_,ClientNotificationType::TRIGGERED,trigger_event_id_B,10,&manager_B1),&manager_B1));
        handlers_and_managers.push_back(std::make_pair(notif_manager.getTriggeredDataReadyHandler(dummy_group_,ClientNotificationType::TRIGGERED,trigger_event_id_B,10,&manager_B2),&manager_B2));

        // Group4
        handlers_and_managers.push_back(std::make_pair(notif_manager.getTriggeredDataReadyHandler(dummy_group_,ClientNotificationType::TRIGGERED,trigger_event_id_C,1000,&manager_C1),&manager_C1));
        handlers_and_managers.push_back(std::make_pair(notif_manager.getTriggeredDataReadyHandler(dummy_group_,ClientNotificationType::TRIGGERED,trigger_event_id_C,1000,&manager_C2),&manager_C2));
        handlers_and_managers.push_back(std::make_pair(notif_manager.getTriggeredDataReadyHandler(dummy_group_,ClientNotificationType::TRIGGERED,trigger_event_id_C,1000,&manager_C3),&manager_C3));
    ); // ASSERT_NO_THROW

    ASSERT_NO_THROW(
        {   // two buffers
            std::vector<std::string> circularBufferNames({"myBufferA1","myBufferA2"});
            ASSERT_TRUE(notif_manager.doTheseCircularBuffersShareTheSameDataReadyHandler(circularBufferNames, ClientNotificationType::STREAMING, 10));
        }
        {   // same buffers, different update-type
            std::vector<std::string> circularBufferNames({"myBufferA1","myBufferA2"});
            ASSERT_TRUE(notif_manager.doTheseCircularBuffersShareTheSameDataReadyHandler(circularBufferNames, ClientNotificationType::FULL_SEQUENCE, trigger_event_id_A));
        }
        {   // single buffer
            std::vector<std::string> circularBufferNames({"myBufferB1"});
            ASSERT_TRUE(notif_manager.doTheseCircularBuffersShareTheSameDataReadyHandler(circularBufferNames, ClientNotificationType::TRIGGERED, trigger_event_id_B));
        }
        {   // two out of tree
            std::vector<std::string> circularBufferNames({"myBufferC1","myBufferC3"});
            ASSERT_TRUE(notif_manager.doTheseCircularBuffersShareTheSameDataReadyHandler(circularBufferNames, ClientNotificationType::TRIGGERED, trigger_event_id_C));
        }
        {   // unknown bufferr
            std::vector<std::string> circularBufferNames({"unkbown buffer1", "unkbown buffer2"});
            ASSERT_FALSE(notif_manager.doTheseCircularBuffersShareTheSameDataReadyHandler(circularBufferNames, ClientNotificationType::TRIGGERED, 100));
        }
        {   // no buffer
            std::vector<std::string> circularBufferNames;
            ASSERT_FALSE(notif_manager.doTheseCircularBuffersShareTheSameDataReadyHandler(circularBufferNames, ClientNotificationType::STREAMING, 10));
        }
    ); // ASSERT_NO_THROW
}


TEST_F(DataReadyManagerFixture, getDataReadyHandler_forced_grouping)
{
    DataReadyManager notif_manager(callbackAllDataReady, 10);
    std::size_t circular_size = 10;
    CircularBuffer<int> buffer(circular_size);

    CircularBufferManagerMock buffer_manager_A1("myBufferA1");
    CircularBufferManagerMock buffer_manager_A2("myBufferA2");
    CircularBufferManagerMock buffer_manager_B1("myBufferB1");
    CircularBufferManagerMock buffer_manager_B2("myBufferB2");

    std::vector<std::pair <DataReadyHandler*, CircularBufferManagerMock*> > handlers_and_managers;

    ASSERT_NO_THROW(
        // Group A
        handlers_and_managers.push_back(std::make_pair(notif_manager.getStreamingDataReadyHandler("groupA", ClientNotificationType::STREAMING,10,10,&buffer_manager_A1),&buffer_manager_A1));
        handlers_and_managers.push_back(std::make_pair(notif_manager.getStreamingDataReadyHandler("groupA", ClientNotificationType::STREAMING,10,100,&buffer_manager_A2),&buffer_manager_A2));

        // Group B
        handlers_and_managers.push_back(std::make_pair(notif_manager.getStreamingDataReadyHandler("groupB", ClientNotificationType::STREAMING,10,10,&buffer_manager_B1),&buffer_manager_B1));
        handlers_and_managers.push_back(std::make_pair(notif_manager.getStreamingDataReadyHandler("groupB", ClientNotificationType::STREAMING,10,100,&buffer_manager_B2),&buffer_manager_B2));

    ); // ASSERT_NO_THROW

    ASSERT_NO_THROW(
        {
            std::vector<std::string> circularBufferNames({"myBufferA1","myBufferB1"});
            ASSERT_FALSE(notif_manager.doTheseCircularBuffersShareTheSameDataReadyHandler(circularBufferNames, ClientNotificationType::STREAMING, 10));
        }
        {
            std::vector<std::string> circularBufferNames({"myBufferA2","myBufferB2"});
            ASSERT_FALSE(notif_manager.doTheseCircularBuffersShareTheSameDataReadyHandler(circularBufferNames, ClientNotificationType::STREAMING, 10));
        }

    ); // ASSERT_NO_THROW
}

} // end namespace
