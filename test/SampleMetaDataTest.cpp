/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <test/SampleMetaDataFixture.h>
#include <test/TimingContextMock.h>

namespace cabad
{

TEST_F(SampleMetaDataFixture, push_front)
{
    std::size_t size = 3;
    ASSERT_NO_THROW(
        MetaDataBuffer buffer(size, &(*device_data_buffer_));

        std::shared_ptr<SampleMetadata> e1(new SampleMetadata());
        std::shared_ptr<SampleMetadata> e2(new SampleMetadata());
        std::shared_ptr<SampleMetadata> e3(new SampleMetadata());
        std::shared_ptr<SampleMetadata> e4(new SampleMetadata());

        buffer.push(e1);
        ASSERT_EQ(e1,buffer.front());
        ASSERT_EQ(e1,buffer.back());
        ASSERT_FALSE(e1->next());
        ASSERT_FALSE(e1->prev());

        buffer.push(e2);
        ASSERT_EQ(e2,buffer.front());
        ASSERT_EQ(e1,buffer.back());
        ASSERT_EQ(e1->next(), e2);
        ASSERT_FALSE(e1->prev());
        ASSERT_FALSE(e2->next());
        ASSERT_EQ(e2->prev(), e1);

        buffer.push(e3);
        ASSERT_EQ(e3,buffer.front());
        ASSERT_EQ(e1,buffer.back());
        ASSERT_EQ(e2->next(), e3);
        ASSERT_FALSE(e1->prev());
        ASSERT_FALSE(e3->next());
        ASSERT_EQ(e3->prev(), e2);

        buffer.push(e4); // now the oldest should cycle out
        ASSERT_EQ(e4,buffer.front());
        ASSERT_EQ(e2,buffer.back());
        ASSERT_FALSE(e2->prev());

        ASSERT_EQ(e3->next(), e4);
        ASSERT_FALSE(e4->next());
        ASSERT_EQ(e4->prev(), e3);
    );
}

TEST_F(SampleMetaDataFixture, push_strictly_increasing)
{
    std::size_t size = 3;
    ASSERT_NO_THROW( // no insert into the middle
        MetaDataBuffer buffer(size, &(*device_data_buffer_));

        std::shared_ptr<const TimingContext> ctxt1(new TimingContextMock(1, 257));
        std::shared_ptr<const TimingContext> ctxt2(new TimingContextMock(2, 283));
        std::shared_ptr<const TimingContext> ctxt3(new TimingContextMock(3, 255));
        std::shared_ptr<const TimingContext> ctxt4(new TimingContextMock(4, 255));
        std::shared_ptr<SampleMetadata> e1(new SampleMetadata(ctxt1));//bigger number = newer
        std::shared_ptr<SampleMetadata> e2(new SampleMetadata(ctxt2));
        std::shared_ptr<SampleMetadata> e3(new SampleMetadata(ctxt3));
        std::shared_ptr<SampleMetadata> e4(new SampleMetadata(ctxt4));

        buffer.push(e1);
        buffer.push(e2);
        buffer.push(e4);
        buffer.push(e3);

        //Expected: 1,2,4  (3 is skipped, since 4 is more recent)
        std::cout << buffer << std::endl;
        ASSERT_EQ(e1,buffer.back());
        ASSERT_EQ(e2,buffer.back()->next());
        ASSERT_EQ(e4,buffer.back()->next()->next());
        ASSERT_EQ(e4,buffer.front());
        ASSERT_EQ(e2,buffer.front()->prev());
        ASSERT_EQ(e1,buffer.front()->prev()->prev());
        ASSERT_FALSE(e1->prev());
        ASSERT_FALSE(e4->next());
    );
}

TEST_F(SampleMetaDataFixture, push_back)
{
    std::size_t size = 3;
    ASSERT_NO_THROW(
        MetaDataBuffer buffer(size, &(*device_data_buffer_));

        std::shared_ptr<SampleMetadata> e1(new SampleMetadata());
        std::shared_ptr<SampleMetadata> e2(new SampleMetadata());
        std::shared_ptr<SampleMetadata> e3(new SampleMetadata());
        std::shared_ptr<SampleMetadata> e4(new SampleMetadata());

        buffer.push(e1);
        ASSERT_EQ(e1,buffer.front());
        ASSERT_EQ(e1,buffer.back());
        ASSERT_FALSE(e1->next());
        ASSERT_FALSE(e1->prev());

        buffer.push(e2);
        ASSERT_EQ(e2,buffer.front());
        ASSERT_EQ(e1,buffer.back());
        ASSERT_EQ(e1->next(), e2);
        ASSERT_FALSE(e1->prev());
        ASSERT_FALSE(e2->next());
        ASSERT_EQ(e2->prev(), e1);

        buffer.push(e3);
        ASSERT_EQ(e3,buffer.front());
        ASSERT_EQ(e1,buffer.back());
        ASSERT_EQ(e2->next(), e3);
        ASSERT_FALSE(e1->prev());
        ASSERT_FALSE(e3->next());
        ASSERT_EQ(e3->prev(), e2);

        buffer.push(e4); // now the oldest should cycle out
        ASSERT_EQ(e4,buffer.front());
        ASSERT_EQ(e2,buffer.back());
        ASSERT_FALSE(e2->prev());

        ASSERT_EQ(e3->next(), e4);
        ASSERT_FALSE(e4->next());
        ASSERT_EQ(e4->prev(), e3);
    );
}

TEST_F(SampleMetaDataFixture, push_same_timing_context_twice)
{
    std::size_t size = 3;
    ASSERT_NO_THROW(
        MetaDataBuffer buffer(size, &(*device_data_buffer_));

        std::shared_ptr<const TimingContext> seq_start(new TimingContextMock(1, 257));
        std::shared_ptr<const TimingContext> inject(new TimingContextMock(2, 283));
        std::shared_ptr<const TimingContext> command(new TimingContextMock(3, 255));
        std::shared_ptr<SampleMetadata> e_seq_start(new SampleMetadata(seq_start));
        std::shared_ptr<SampleMetadata> e_inject(new SampleMetadata(inject));
        std::shared_ptr<SampleMetadata> e_command(new SampleMetadata(command));

        buffer.push(e_seq_start);
        buffer.push(e_inject);
        buffer.push(e_command);
        ASSERT_EQ(e_seq_start,buffer.back());
        ASSERT_EQ(e_command,buffer.front());

        ASSERT_EQ(-1,buffer.push(e_inject)); // should be ignored, is already in buffer

        // nothing cycled out
        ASSERT_EQ(e_seq_start,buffer.back());
        ASSERT_EQ(e_command,buffer.front());
    );
}

TEST_F(SampleMetaDataFixture, getNextTriggerMeta)
{
    MetaDataBuffer buffer(5, &(*device_data_buffer_));

    std::shared_ptr<const TimingContext> seq_start(new TimingContextMock(1, 257));
    std::shared_ptr<const TimingContext> inject(new TimingContextMock(2, 283));
    std::shared_ptr<const TimingContext> command(new TimingContextMock(3, 255));

    std::shared_ptr<SampleMetadata> e1(new SampleMetadata());
    std::shared_ptr<SampleMetadata> e2(new SampleMetadata(seq_start));
    std::shared_ptr<SampleMetadata> e3(new SampleMetadata(inject));
    std::shared_ptr<SampleMetadata> e4(new SampleMetadata(command));
    std::shared_ptr<SampleMetadata> empty;

    // throw if empty
    ASSERT_THROW(
            buffer.getNextTriggerMeta(std::shared_ptr<SampleMetadata>(nullptr));
            ,std::exception);

    buffer.push(e1);

    // nothing / no Trigger meta added so far 1
    ASSERT_NO_THROW(
        std::shared_ptr<SampleMetadata> result = buffer.getNextTriggerMeta (e2);
        ASSERT_FALSE(result);
    );

    // nothing / no Trigger meta added so far 2
    ASSERT_NO_THROW(
        std::shared_ptr<SampleMetadata> result = buffer.getNextTriggerMeta (empty);
        ASSERT_FALSE(result);
    );

    buffer.push(e2);
    buffer.push(e3);
    buffer.push(e4);

    // Start search by back
    ASSERT_NO_THROW(
        std::shared_ptr<SampleMetadata> result = buffer.getNextTriggerMeta (empty);
        ASSERT_EQ(result, e2);
    );

    // Start search by e2
    ASSERT_NO_THROW(
        std::shared_ptr<SampleMetadata> result = buffer.getNextTriggerMeta (e2);
        ASSERT_EQ(result, e3);
    );

    // Start search by e3
    ASSERT_NO_THROW(
        std::shared_ptr<SampleMetadata> result = buffer.getNextTriggerMeta (e3);
        ASSERT_EQ(result, e4);
    );

    // Start search by e4 (nothing found)
    ASSERT_NO_THROW(
        std::shared_ptr<SampleMetadata> result = buffer.getNextTriggerMeta (e4);
        ASSERT_FALSE(result);
    );
}

TEST_F(SampleMetaDataFixture, findTriggerMeta)
{
    MetaDataBuffer buffer(5, &(*device_data_buffer_));

    std::shared_ptr<const TimingContext> seq_start(new TimingContextMock(2, 257));
    std::shared_ptr<const TimingContext> inject(new TimingContextMock(3, 283));
    std::shared_ptr<const TimingContext> command(new TimingContextMock(4, 255));

    std::shared_ptr<SampleMetadata> e1(new SampleMetadata());
    std::shared_ptr<SampleMetadata> e2(new SampleMetadata(seq_start));
    std::shared_ptr<SampleMetadata> e3(new SampleMetadata(inject));
    std::shared_ptr<SampleMetadata> e4(new SampleMetadata(command));

    // throw if empty
    ASSERT_THROW(
            buffer.getNextTriggerMeta(std::shared_ptr<SampleMetadata>(nullptr));
            ,std::exception);

    buffer.push(e1);

    // nothing / no Trigger meta added so far 1
    ASSERT_NO_THROW(
        std::shared_ptr<SampleMetadata> result = buffer.findTriggerMeta (2);
        ASSERT_FALSE(result);
    );

    buffer.push(e2);
    buffer.push(e3);
    buffer.push(e4);

    ASSERT_NO_THROW(
        std::shared_ptr<SampleMetadata> result = buffer.findTriggerMeta (2);
        ASSERT_EQ(result, e2);
    );
    ASSERT_NO_THROW(
        std::shared_ptr<SampleMetadata> result = buffer.findTriggerMeta (3);
        ASSERT_EQ(result, e3);
    );
    ASSERT_NO_THROW(
        std::shared_ptr<SampleMetadata> result = buffer.findTriggerMeta (4);
        ASSERT_EQ(result, e4);
    );

    // Unknown stamp
    ASSERT_NO_THROW(
        std::shared_ptr<SampleMetadata> result = buffer.findTriggerMeta (5);
        ASSERT_FALSE(result);
    );
}

TEST_F(SampleMetaDataFixture, findRefMeta)
{
    CircularBuffer<float> circular_buffer(100);
    std::shared_ptr<CircularBufferBase::iterator> valid_sample = circular_buffer.getReadIterator(1);

    std::shared_ptr<const TimingContext> ref_meta1(new TimingContextMock(11, 283));
    std::shared_ptr<const TimingContext> ref_meta2(new TimingContextMock(22, 283));

    //Invalid start/end passed
    ASSERT_THROW(
            MetaDataBuffer buffer(5, &(*device_data_buffer_));
            std::shared_ptr<SampleMetadata> e1(new SampleMetadata(valid_sample));
            buffer.push(e1);
            buffer.findRefMeta(std::shared_ptr<SampleMetadata>(nullptr),e1);
            ,std::exception);
    ASSERT_THROW(
            MetaDataBuffer buffer(5, &(*device_data_buffer_));
            std::shared_ptr<SampleMetadata> e1(new SampleMetadata(valid_sample));
            buffer.push(e1);
            buffer.findRefMeta(e1,std::shared_ptr<SampleMetadata>(nullptr));
            ,std::exception);

    // ref trigger before window start
    ASSERT_NO_THROW(
        MetaDataBuffer buffer(5, &(*device_data_buffer_));
        std::shared_ptr<SampleMetadata> e1(new SampleMetadata(valid_sample, ref_meta1));
        std::shared_ptr<SampleMetadata> e2(new SampleMetadata(valid_sample, 22));
        std::shared_ptr<SampleMetadata> e3(new SampleMetadata(valid_sample, 33));
        buffer.push(e1);
        buffer.push(e2);
        buffer.push(e3);
        std::shared_ptr<const SampleMetadata> result = buffer.findRefMeta(e2,e3);
        ASSERT_EQ(result, e1);
    );

    // ref trigger on window_start
    ASSERT_NO_THROW(
        MetaDataBuffer buffer(5, &(*device_data_buffer_));
        std::shared_ptr<SampleMetadata> e1(new SampleMetadata(valid_sample, 1));
        std::shared_ptr<SampleMetadata> e2(new SampleMetadata(valid_sample, ref_meta1));
        std::shared_ptr<SampleMetadata> e3(new SampleMetadata(valid_sample, 33));
        buffer.push(e1);
        buffer.push(e2);
        buffer.push(e3);
        std::shared_ptr<const SampleMetadata> result = buffer.findRefMeta(e2,e3);
        ASSERT_EQ(result, e2);
    );

    // ref between start and end
    ASSERT_NO_THROW(
        MetaDataBuffer buffer(5, &(*device_data_buffer_));
        std::shared_ptr<SampleMetadata> e1(new SampleMetadata(valid_sample, 0));
        std::shared_ptr<SampleMetadata> e2(new SampleMetadata(valid_sample, 1));
        std::shared_ptr<SampleMetadata> e3(new SampleMetadata(valid_sample, ref_meta1));
        std::shared_ptr<SampleMetadata> e4(new SampleMetadata(valid_sample, 44));
        buffer.push(e1);
        buffer.push(e2);
        buffer.push(e3);
        buffer.push(e4);
        std::shared_ptr<const SampleMetadata> result = buffer.findRefMeta(e2,e4);
        ASSERT_EQ(result, e3);
    );

    //ref trigger on window end
    ASSERT_NO_THROW(
        MetaDataBuffer buffer(5, &(*device_data_buffer_));
        std::shared_ptr<SampleMetadata> e1(new SampleMetadata(valid_sample, 0));
        std::shared_ptr<SampleMetadata> e2(new SampleMetadata(valid_sample, 0));
        std::shared_ptr<SampleMetadata> e3(new SampleMetadata(valid_sample, ref_meta1));
        buffer.push(e1);
        buffer.push(e2);
        buffer.push(e3);
        std::shared_ptr<const SampleMetadata> result = buffer.findRefMeta(e2,e3);
        ASSERT_EQ(result, e3);
    );

    // ref trigger before window start and inside window (inside window should be picked)
    ASSERT_NO_THROW(
        MetaDataBuffer buffer(5, &(*device_data_buffer_));
        std::shared_ptr<SampleMetadata> e1(new SampleMetadata(valid_sample, ref_meta1));
        std::shared_ptr<SampleMetadata> e2(new SampleMetadata(valid_sample, 12));
        std::shared_ptr<SampleMetadata> e3(new SampleMetadata(valid_sample, ref_meta2));
        std::shared_ptr<SampleMetadata> e4(new SampleMetadata(valid_sample, 44));
        buffer.push(e1);
        buffer.push(e2);
        buffer.push(e3);
        buffer.push(e4);
        std::shared_ptr<const SampleMetadata> result = buffer.findRefMeta(e2,e4);
        ASSERT_EQ(result, e3);
    );

    // Two ref triggers inside window (oldest should be picked)
    ASSERT_NO_THROW(
        MetaDataBuffer buffer(5, &(*device_data_buffer_));
        std::shared_ptr<SampleMetadata> e1(new SampleMetadata(valid_sample, 0));
        std::shared_ptr<SampleMetadata> e2(new SampleMetadata(valid_sample, ref_meta1));
        std::shared_ptr<SampleMetadata> e3(new SampleMetadata(valid_sample, ref_meta2));
        std::shared_ptr<SampleMetadata> e4(new SampleMetadata(valid_sample, 44));
        buffer.push(e1);
        buffer.push(e2);
        buffer.push(e3);
        buffer.push(e4);
        std::shared_ptr<const SampleMetadata> result = buffer.findRefMeta(e1,e4);
        ASSERT_EQ(result, e2);
    );
}

TEST_F(SampleMetaDataFixture, getNextSequenceMarker)
{
    std::shared_ptr<const TimingContext> old_seq_marker_start(new TimingContextMock(1, 257));
    std::shared_ptr<const TimingContext> new_seq_marker_start(new TimingContextMock(2, 258));
    std::shared_ptr<const TimingContext> new_seq_marker_gap(new TimingContextMock(3, 257));

    // No sequence markers
    ASSERT_NO_THROW(
            MetaDataBuffer buffer(5, &(*device_data_buffer_));
            std::shared_ptr<SampleMetadata> e1(new SampleMetadata());
            buffer.push(e1);
            auto result = buffer.getNextSequenceMarker(std::shared_ptr<SampleMetadata>(nullptr));
            ASSERT_TRUE(!result);
    );

    // No elements happened after old_seq_marker_start
    ASSERT_NO_THROW(
            MetaDataBuffer buffer(5, &(*device_data_buffer_));
            std::shared_ptr<SampleMetadata> e1 = std::make_shared<SampleMetadata>(old_seq_marker_start);
            buffer.push(e1);
            ASSERT_FALSE(buffer.getNextSequenceMarker(e1));
    );

    // seq-start on next
    ASSERT_NO_THROW(
        MetaDataBuffer buffer(5, &(*device_data_buffer_));
        std::shared_ptr<SampleMetadata> e1(new SampleMetadata());
        std::shared_ptr<SampleMetadata> e2 = std::make_shared<SampleMetadata>(old_seq_marker_start);
        std::shared_ptr<SampleMetadata> e3 = std::make_shared<SampleMetadata>(new_seq_marker_start);
        std::shared_ptr<SampleMetadata> e4(new SampleMetadata());
        buffer.push(e1);
        buffer.push(e2);
        buffer.push(e3);
        buffer.push(e4);
        std::shared_ptr<SampleMetadata> result = buffer.getNextSequenceMarker(e2);
        ASSERT_EQ(result, e3);
    );

    // gap-start on next
    ASSERT_NO_THROW(
        MetaDataBuffer buffer(5, &(*device_data_buffer_));
        std::shared_ptr<SampleMetadata> e1(new SampleMetadata());
        std::shared_ptr<SampleMetadata> e2 = std::make_shared<SampleMetadata>(old_seq_marker_start);
        std::shared_ptr<SampleMetadata> e3 = std::make_shared<SampleMetadata>(new_seq_marker_gap);
        std::shared_ptr<SampleMetadata> e4(new SampleMetadata());
        buffer.push(e1);
        buffer.push(e2);
        buffer.push(e3);
        buffer.push(e4);
        std::shared_ptr<SampleMetadata> result = buffer.getNextSequenceMarker(e2);
        ASSERT_EQ(result, e3);
    );

    // seq-start on buffer.begin
    ASSERT_NO_THROW(
        MetaDataBuffer buffer(5, &(*device_data_buffer_));
        std::shared_ptr<SampleMetadata> e1(new SampleMetadata());
        std::shared_ptr<SampleMetadata> e2 = std::make_shared<SampleMetadata>(old_seq_marker_start);
        std::shared_ptr<SampleMetadata> e3(new SampleMetadata());
        std::shared_ptr<SampleMetadata> e4 = std::make_shared<SampleMetadata>(new_seq_marker_start);
        buffer.push(e1);
        buffer.push(e2);
        buffer.push(e3);
        buffer.push(e4);
        std::shared_ptr<SampleMetadata> result = buffer.getNextSequenceMarker(e2);
        ASSERT_EQ(result, e4);
    );

    // no seq-start found
    ASSERT_NO_THROW(
        MetaDataBuffer buffer(5, &(*device_data_buffer_));
        std::shared_ptr<SampleMetadata> e1(new SampleMetadata());
        std::shared_ptr<SampleMetadata> e2 = std::make_shared<SampleMetadata>(old_seq_marker_start);
        std::shared_ptr<SampleMetadata> e3(new SampleMetadata());
        std::shared_ptr<SampleMetadata> e4(new SampleMetadata());
        buffer.push(e1);
        buffer.push(e2);
        buffer.push(e3);
        buffer.push(e4);
        std::shared_ptr<SampleMetadata> result = buffer.getNextSequenceMarker(e2);
        ASSERT_FALSE(result);
    );

    // search meta should not be used as 'found meta'
    ASSERT_NO_THROW(
        MetaDataBuffer buffer(5, &(*device_data_buffer_));
        std::shared_ptr<SampleMetadata> e1(new SampleMetadata());
        std::shared_ptr<SampleMetadata> e2 = std::make_shared<SampleMetadata>(old_seq_marker_start);
        std::shared_ptr<SampleMetadata> e3(new SampleMetadata());
        buffer.push(e1);
        buffer.push(e2);
        buffer.push(e3);
        std::shared_ptr<SampleMetadata> result = buffer.getNextSequenceMarker(e2);
        ASSERT_FALSE(result);
    );

    // marker is not found in buffer / already cycled out -->start serach by buffer-begin
    ASSERT_NO_THROW(
        MetaDataBuffer buffer(2, &(*device_data_buffer_));
        std::shared_ptr<SampleMetadata> e1 = std::make_shared<SampleMetadata>(old_seq_marker_start);
        std::shared_ptr<SampleMetadata> e2 = std::make_shared<SampleMetadata>(new_seq_marker_start);
        std::shared_ptr<SampleMetadata> e3 = std::make_shared<SampleMetadata>(new_seq_marker_gap);
        buffer.push(e1);
        buffer.push(e2);
        buffer.push(e3);

        // Make sure the old triger cycled out
        std::shared_ptr<SampleMetadata> result1 = buffer.findTriggerMeta(e1->getTimingContextStamp());
        ASSERT_FALSE(result1);

        std::shared_ptr<SampleMetadata> result2 = buffer.getNextSequenceMarker(e1);
        ASSERT_EQ(e2,result2);
    );
}

} // end namespace
