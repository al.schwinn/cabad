# **ATTENTION !! THIS PROJECT IS WIP !!!**

## cabad

**c**ircular **a**cquisition **b**uffer for **a**ccelerator **d**evices

The main target of this library is to package incoming data in a way so that outgoing packages are time-synchronius across all channels. Incoming data can be pushed asynchroniusly for each channel.

In order to synchronize, the incoming data needs to have timestamps.

The packaging is tailored to the use-case "accelerator devices". E.g. there is a packaging mode called "FULL_SEQUENCE" which shows a spefic time-window relevant for accelerator operation.

The buffer is lock-free in the way, that readers will not block writers. In case of multiple writers, a lock will be used to synchronize them.

- Meta tags are used to store related timing metainfo and to allign the data in time
- Supported acquisition modes are STREAMING, FULL_SEQUENCE and TRIGGERED.
- Clients can listen to a queue in order to obtain specific data-packages, based on several parameters

### Some notes on the bare circular buffer:
- A statical allocated buffer is used
- The buffer supports multiple-writer and multiple-reader threads
- Readers dont need to mutex lock the buffer for reading. They will not block the write iterator.
- Instead, writers will flag registered readers as "invalid", if they write on the same data slot. Like that read iterators know on potential corrupted data and can drop it. (Though this only will happen on border cases and under extreme conditions)
- A mutex is used to snychronize multiple writers.

### Requirements which led to the current design
- Possibility to use memcopy on the data (to be as fast as possibly)
- Use data types which can be directly copied into the client's data container (currently the cleint is FESA --> basic types, no structures)
- Fixed buffer size (no memory allocation during runtime)
- read iterators should not block write iterators
- A simple way to use the buffer in FESA classes

Why would you want to use this buffer over some other public implementations (e.g. boost circular buffer) ?
- There was no public implementation which fulfilled our requirements. E.g. check https://stackoverflow.com/a/890269/1599887
- When writing this, there was no circular buffer arround, which solved the "non-blocking write iter" aspect for a fixed buffer size

### Build from source
- make && make test && make run-test && make install

### Test
Check the basic example inside the "example" folder for a simple test:
- build cabad itself
- enter the "example" folder
- make && ./cabad_example
