/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <cabad/CircularBufferManagerBase.h>

namespace CabadExample
{

#define CIRCULAR_BUFFER_TYPE cabad::CircularBufferDual<float, float>

/* Manages all Iterators and Meta-Data of a single CircularSampleBuffer */ 
class CircularBufferManager: public cabad::CircularBufferManagerBase
{

public:

    CircularBufferManager(std::string signal_name,
                                                                   float sample_rate_hz,
                                                                   std::vector<cabad::MaxClientUpdateFrequencyType> maxClientUpdateFrequencies,
                                                                   cabad::ContextTracker* contextTracker,
                                                                   cabad::DeviceDataBufferBase* deviceDataBuffer,
                                                                   cabad::CircularBufferBase* circularBuffer,
                                                                   cabad::MetaDataBuffer* metadataBuffer,
                                                                   cabad::ClientNotificationType clientNotificationType,
                                                                   cabad::DataPushMode dataPushMode,
                                                                   cabad::DataReadyManager* dataReadyManager,
                                                                   const std::string& notification_group):
                 cabad::CircularBufferManagerBase(            signal_name,
                                                              sample_rate_hz,
                                                              maxClientUpdateFrequencies,
                                                              contextTracker,
                                                              deviceDataBuffer,
                                                              circularBuffer,
                                                              metadataBuffer,
                                                              clientNotificationType,
                                                              dataPushMode,
                                                              dataReadyManager,
                                                              notification_group)
    {

    }

    void push(float            *values,
                                     float            *errors,
                                     std::size_t             size)
    {
        try
        {
            //    std::cout << "CircularBufferManager::pushing " << values_size << " samples - Number of tags found: " << tags.size() << std::endl;
            CIRCULAR_BUFFER_TYPE *buffer = static_cast<CIRCULAR_BUFFER_TYPE*>(circularBuffer_);
            buffer->push(values, errors, size);

            // TODO: Example on how to push trigger tags or SampleStamps

            pushBackDataFinished();
        }
        catch(std::exception& ex)
        {
            std::cout << "Something went wrong: " << ex.what() << std::endl;
        }
    }

    void copyDataWindow(cabad::CircularWindowIterator* window_iter, float* values, float* errors, const std::size_t& buffer_size, std::size_t& n_data_written)
    {
        if(window_iter->disabled())
            return;

		CIRCULAR_BUFFER_TYPE *buffer = static_cast<CIRCULAR_BUFFER_TYPE*>(circularBuffer_);
		buffer->copyDataWindow(*(window_iter->data_start_), *(window_iter->data_end_), values, errors, buffer_size, n_data_written);
		std::cout << "copyDataWindow - copied " << n_data_written << "samples" << std::endl;
    }

};

} //namespace

