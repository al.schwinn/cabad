
#include <cabad/ContextTracker.h>
#include <cabad/DataReadyManager.h>
#include <cabad/DataReadyQueue.h>
#include <cabad/DeviceDataBufferBase.h>

#include "SignalSource.h"
#include "CircularBufferManager.h"

#include <chrono>
#include <thread>//sleep

#include <memory>//smart pointers

// global, in order to keep things simple (needed in the callback)
cabad::DataReadyManager* daqDataReadyManager;
CabadExample::SignalSource* source1;
CabadExample::SignalSource* source2;

void circular_buffer_log_function(cabad::LogLevel severity, const std::string& file, int line, const std::string& message, const std::string& topic)
{
    std::cout << file  << ":" << line << " Message: " << message << std::endl;
}

void cb_data_ready(uint32_t data_id)
{
    cabad::ClientNotificationData data;
    daqDataReadyManager->getDataByID(data_id, data);

    for(const auto& signal_name : data.sinkNames_)
    {
        std::cout << signal_name << ", ";

        // TODO: Simplify API by directly sending a reference to the related window
        // Find the according source,etc.
        CabadExample::SignalSource* source;
        if(signal_name == source1->signal_name_)
            source = source1;
        else
            source = source2;
        cabad::CircularWindowIterator* window_iter = source->circular_buffer_manager_->getStreamingDataWindow(data.updateFrequency_);

        std::size_t buffer_size = 10000;
        std::size_t n_samples_received = 0;
        float values[buffer_size];
        float errors[buffer_size];
        source->circular_buffer_manager_->copyDataWindow(window_iter, &values[0], &errors[0], buffer_size, n_samples_received);
        std::cout << n_samples_received << " samples received for the signal: " << signal_name <<  "(update freq " << data.updateFrequency_ << "Hz): ";
        for(std::size_t i = 0; i < n_samples_received; i++)
        {
            std::cout << values[i] << ",";
            // print only first 5 samples to dont pollute the screen
            if(i==4)
            {
                std::cout << "..." << std::endl;
                break;
            }
        }
    }
}

int main (void)
{
    // set a log function for the circular buffer
    cabad::setLogFunction(circular_buffer_log_function);

    // Buffer specific to each (FESA) device, specifying on which events trigger data is expected
    const std::vector<std::string> triggerEvents;
    cabad::DeviceDataBufferBase deviceData(triggerEvents, "FesaDeviceNomen");

    // Used to track e.g. WhiteRabbit context information. (Which is not stored long enough by FESA)
    cabad::ContextTracker contextTracker(100);

    daqDataReadyManager = new cabad::DataReadyManager(cb_data_ready, // Called when data of some circular buffers is ready to be shipped to clients
                                                      1000);                                                         // Maximum number of Data-Ready Notifications to queue up (e.g. relevant for slow clients)

    float sample_rate_hz = 1000; // we simulate a 1kHz data source
    float buffer_time_seconds = 10;
    source1 = new CabadExample::SignalSource(deviceData,"Signal1",sample_rate_hz,buffer_time_seconds,contextTracker,daqDataReadyManager);
    source2 = new CabadExample::SignalSource(deviceData,"Signal2",sample_rate_hz,buffer_time_seconds,contextTracker,daqDataReadyManager);

    while(true)
    {
        // Simplified ... each source as well could push new data in its own thread
        source1->simulate_data(100);
        source2->simulate_data(100);
        std::this_thread::sleep_for(std::chrono::milliseconds(100)); //since we have client updaterate 10Hz
    }

    return 0;
}


