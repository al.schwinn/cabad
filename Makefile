## Help on how to modify Makefiles you can get e.g. here: http://www.ijon.de/comp/tutorials/makefile.html

CPU ?= x86_64
CABAD_VERSION = master

G_TEST_VERSION ?= 1.6.0
G_TEST_HOME ?= /common/usr/cscofe/opt/google/gtest/$(G_TEST_VERSION)
G_TEST_INCL ?=-I$(G_TEST_HOME)/include
G_TEST_LIBS ?=-lpthread -L$(G_TEST_HOME)/lib/$(CPU) -lgtest

G_MOCK_VERSION ?= 1.6.0
G_MOCK_HOME ?= /common/usr/cscofe/opt/google/gmock/$(G_MOCK_VERSION)
G_MOCK_INCL ?=-I$(G_MOCK_HOME)/include
G_MOCK_LIBS ?=-L$(G_MOCK_HOME)/lib/$(CPU) -lgmock

mkfile_path := $(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))
mkfile_dir:=$(shell cd $(shell dirname $(mkfile_path)); pwd)
current_dir:=$(notdir $(mkfile_dir))

#$(info mkfile_dir is $(mkfile_dir))

#headers to include
INCLUDES += -I.
INCLUDES_TEST += $(INCLUDES) $(G_TEST_INCL) $(G_MOCK_INCL)

#libraries to link
LIBS_TEST = $(LIBS) $(G_TEST_LIBS) $(G_MOCK_LIBS)


ifdef DEBUG
# No optimization and include debugging symbols. Usage: make DEBUG=TRUE
COMPILER_FLAGS += -O0 -g
else
#Optimisation flag ( 00 till 03 )
COMPILER_FLAGS += -O3
endif

#Enable C++11 Support
COMPILER_FLAGS += -std=c++11

# Warn all - always use this to make the compiler really picky (and thus more helpful) 
COMPILER_FLAGS += -Wall

# Hold on first error (less console scrolling)
COMPILER_FLAGS += -Wfatal-errors

#Possibility to compile with colormake via less: colormake  2>&1 | less -R
COMPILER_FLAGS += -fdiagnostics-color=always

COMPILER_FLAGS += -DCABAD_VERSION=$(CABAD_VERSION)

OBJECT_DIR  := build/objects
LIB_DIR     := build/lib
BIN_DIR     := build/test
INCLUDE_DIR := build/include

PROJECT_NAME=cabad

# .o files have same name as .cpp files
SOURCES_FOLDER=${PROJECT_NAME}
SOURCES	     := $(wildcard $(SOURCES_FOLDER)/*.cpp)
SOURCES_TEST := $(wildcard test/*.cpp)
OBJ          := $(patsubst $(SOURCES_FOLDER)/%.cpp,$(OBJECT_DIR)/%.o,$(SOURCES))
OBJ_TEST     := $(SOURCES_TEST:%.cpp=%.o)

INCLUDES_SRC := $(wildcard $(SOURCES_FOLDER)/*.h)
INCLUDES_DST := $(patsubst %.h,$(INCLUDE_DIR)/%.h,$(INCLUDES_SRC))

TARGET = $(LIB_DIR)/lib${PROJECT_NAME}.a
TARGET_TEST = $(BIN_DIR)/test
#$(info OBJ is: $(OBJ))
#$(info SOURCES is: $(SOURCES))

all: $(OBJ) $(TARGET) $(INCLUDES_DST)
	@echo "-- build of target: '$(TARGET)' finished --"

test: all $(TARGET_TEST) 

# starting a specific test:
# make test -j4;./build/test/test --gtest_filter=CircularBufferManagerBaseTestFixture*

# starting a specific test via gdb:
# make test -j4;gdb --args ./build/test/test --gtest_filter=CircularBufferManagerBaseTestFixture*
run-test: test
	./build/test/test

clean:
	rm -f $(OBJ) $(TARGET) $(INCLUDES_DST) $(OBJ_TEST) $(TARGET_TEST)
	
$(INCLUDE_DIR)/$(SOURCES_FOLDER)/%.h: $(SOURCES_FOLDER)/%.h
	@mkdir -p $(@D)
	cp $^ $@

# Compilation command:
# Generic Way to create a .o file for each .cpp file (for many cpp files) $< is the "first dependency"
$(OBJECT_DIR)/%.o: $(SOURCES_FOLDER)/%.cpp
	@echo "==== Making '$@' ===="
	@mkdir -p $(@D)
	g++ $(INCLUDES) $(COMPILER_FLAGS) -c $< -o $@

test/%.o: test/%.cpp
	g++ $(INCLUDES_TEST) $(COMPILER_FLAGS) -c $< -o $@
	
$(TARGET_TEST): $(OBJ_TEST)
	@echo "==== Making testprog '$@' ===="
	@mkdir -p $(@D)
	g++ $(OBJ_TEST) $(LINKER_FLAGS) -o $(TARGET_TEST) $(TARGET) $(LIBS_TEST) $(LIBS_TEST)  # adding the libs twice solves some strange linker problem

$(TARGET): $(OBJ)
	@echo "==== Building the library: $@ ===="
	@echo 
	@echo 
	@mkdir -p $(@D)
	ar rcs $@ $(OBJ)

INSTALL_DIR_BASE ?= /common/usr/cscofe/opt/${PROJECT_NAME}
INSTALL_DIR = ${INSTALL_DIR_BASE}/${CABAD_VERSION}
#$(info OBJ is: $(OBJ))

install : $(TARGET)
	@echo "==== Installing ${PROJECT_NAME} into: '${INSTALL_DIR}' ===="
	@mkdir -p ${INSTALL_DIR}
	@cp -r ${mkfile_dir}/build/include ${INSTALL_DIR}
	@cp -r ${mkfile_dir}/build/lib ${INSTALL_DIR}
ifneq ($(CABAD_VERSION),master)
		@chmod a-w -R ${INSTALL_DIR}
endif
	@echo "==== Install of ${PROJECT_NAME} finished ===="
	